import libs.data_parser as dp
import libs.structure as stc

import pymongo as pm
import yaml
import os


with open('confs/conf.yml', 'r') as yamlfile:
    config = yaml.load(yamlfile)

# Данные по присоединению к Clickhouse. Подгружаются либо из env-переменных либо из файла conf.yml
try:
    env = 'dev'
except:
    raise(KeyError('Задайте переменную окружения (export ENV_NAME=prod)'))

# Данные Clickhouse
clickhouse_data = {
    'url': config[env]['clickhouse_url'],
    'user': config[env]['clickhouse_user'],
    'password': config[env]['clickhouse_password']
}

# Словарь для выгрузки параметров кандидатов
# Подробно расписал для одного датасета, для остальных по аналогии
candidates_parameters_dict = {
    # Область выгрузки. Для megafon и sberbank используются разные движки таблиц
    # MergeTree и ReplacingMergeTree соответственно.
    'scope': 'megafon',
    # Инстанс коннекшна к MongoDB
    'conn': pm.MongoClient('mongodb://' + config[env]['conn'], readPreference='secondary'),
    # Схема данных выгрузки. Нужна для генерации запроса на создание таблицы в
    # в КХ и дальнейшей обработки данных соответсвенно их типам.
    'coldict': stc.colsnames_candidates,
    # Путь до поля в Clickhouse, по дате последней записи которого определяется
    # с какого момента выгружать данных из MongoDB
    'status_set_at_field': stc.colsnames_candidates['status_set_at']['name'],
    # Название базы данных и табицы в которую производится выгрузка
    'full_tbl_name': config[env]['megafon']['db_name'] + '.' + config[env]['megafon']['tbl_candidates_name'],
    # Путь до поля с датой в MongoDB, по которому выгружаются данные
    'path_to_audit_updated_at': 'AuditInfo.UpdatedAt',
    # Используется для выгрузки статусов. По этому полю определяется
    # какие статусы уже присутсвуют в Clickhouse.
    'last_state_update_field': '_LastStateUpdate',
    # Поле ключа партиционирования данных в Clickhouse
    'date_field': stc.colsnames_candidates['date_for_clickhouse']['name'],
    # Строка коннекшна к Clickhouse
    'clickhouse_url': 'http://' + clickhouse_data['url'] + '/?user=' + clickhouse_data['user'] + '&password=' + clickhouse_data['password'],
    # Лимит при выгрузке. Не используется по причине введения поля batch_size
    'limit': int(config['limit']),
    # Отображение отправляемых запросов. В некоторых местах может не работать.
    'show_queries': config['show_queries'],
    # Путь до поля _id в Clickhouse как первичного ключа
    'id_field': stc.colsnames_candidates['id_field']['mongo_name'],
    # Путь до целевой коллекции в MongoDB
    'path': dp.Path.pars_path('/candidates/Candidates//'),
    # Путь до коллекции заявок в MongoDB
    'jobr_path': dp.Path.pars_path('/workspace/JobRequisitions//'),
    # Компании которые выгружаются в Clickhouse. Нужны для генерации запроса в MongoDB
    'companies': config[env]['megafon']['companies'],
    # Размер батча выгружаемых данных
    'batch_size': config['batch_size']
}

OrgUnits_parameters_dict = {
    # Область выгрузки. Для megafon и sberbank используются разные движки таблиц
    # MergeTree и ReplacingMergeTree соответственно.
    'scope': 'megafon',
    # Инстанс коннекшна к MongoDB
    'conn': pm.MongoClient('mongodb://' + config[env]['conn'], readPreference='secondary'),
    # Схема данных выгрузки. Нужна для генерации запроса на создание таблицы в
    # в КХ и дальнейшей обработки данных соответсвенно их типам.
    'coldict': stc.colsnames_candidates_OrgUnits,
    # Путь до поля в Clickhouse, по дате последней записи которого определяется
    # с какого момента выгружать данных из MongoDB
    'status_set_at_field': stc.colsnames_candidates_OrgUnits['status_set_at']['name'],
    # Название базы данных и табицы в которую производится выгрузка
    'full_tbl_name': config[env]['megafon']['db_name'] + '.' + config[env]['megafon']['tbl_candidates_name_new'],
    # Путь до поля с датой в MongoDB, по которому выгружаются данные
    'path_to_audit_updated_at': 'AuditInfo.UpdatedAt',
    # Используется для выгрузки статусов. По этому полю определяется
    # какие статусы уже присутсвуют в Clickhouse.
    'last_state_update_field': '_LastStateUpdate',
    # Поле ключа партиционирования данных в Clickhouse
    'date_field': stc.colsnames_candidates_OrgUnits['date_for_clickhouse']['name'],
    # Строка коннекшна к Clickhouse
    'clickhouse_url': 'http://' + clickhouse_data['url'] + '/?user=' + clickhouse_data['user'] + '&password='
                      + clickhouse_data['password'],
    # Лимит при выгрузке. Не используется по причине введения поля batch_size
    'limit': int(config['limit']),
    # Отображение отправляемых запросов. В некоторых местах может не работать.
    'show_queries': config['show_queries'],
    # Путь до поля _id в Clickhouse как первичного ключа
    'id_field': stc.colsnames_candidates_OrgUnits['id_field']['mongo_name'],
    # Путь до целевой коллекции в MongoDB
    'path': dp.Path.pars_path('/workspace/OrgUnits//'),
    # Путь до коллекции заявок в MongoDB
    'jobr_path': dp.Path.pars_path('/workspace/JobRequisitions//'),
    # Компании которые выгружаются в Clickhouse. Нужны для генерации запроса в MongoDB
    'companies': config[env]['megafon']['companies'],
    # Размер батча выгружаемых данных
    'batch_size': config['batch_size']

}

videointerview_parameters_dict = {
    'scope': 'sberbank',
    'conn': pm.MongoClient('mongodb://' + config[env]['conn'], readPreference='secondary'),
    'coldict': stc.colsnames_videointerview,
    'status_set_at_field': stc.colsnames_videointerview['updated_at']['name'],
    'full_tbl_name': (config[env]['sberbank']['db_name']) + '.' + (config[env]['sberbank']['tbl_candidates_name_videointerview']),
    'path_to_audit_updated_at': 'AuditInfo.UpdatedAt',
    'primary_keys': '_id, _question_order, _competence_name, _value, _by_id, _by_name',
    'last_state_update_field': '_LastStateUpdate',
    'date_field': stc.colsnames_videointerview['date_for_clickhouse']['name'],
    'clickhouse_url': 'http://' + clickhouse_data['url'] + '/?user=' + clickhouse_data['user'] + '&password=' +
                      clickhouse_data['password'],
    'limit': int(config['limit']),
    'show_queries': config['show_queries'],
    'id_field': stc.colsnames_videointerview['id_field']['mongo_name'],
    'path': dp.Path.pars_path('/Assessments/VideoInterview//'),
    'companies': config[env]['sberbank']['companies'],
    'batch_size': config['batch_size']

}


sber_educations_parameters_dict = {
    'scope': 'sberbank',
    'conn' : pm.MongoClient('mongodb://' + config[env]['conn'], readPreference='secondary'),
    'coldict' : stc.colsnames_educations_sber,
    'status_set_at_field' : stc.colsnames_educations_sber['updated_at']['name'],
    'full_tbl_name' : (config[env]['sberbank']['db_name']) + '.' + (config[env]['sberbank']['tbl_educations_name']),
    'path_to_audit_updated_at' : 'AuditInfo.UpdatedAt',
    'primary_keys': '_id, _education_organization, _education_specialization, _education_start_year, _education_end_year',
    'last_state_update_field' : '_LastStateUpdate',
    'date_field' : stc.colsnames_educations_sber['date_for_clickhouse']['name'],
    'clickhouse_url' : 'http://' + clickhouse_data['url'] + '/?user=' + clickhouse_data['user'] + '&password=' + clickhouse_data['password'],
    'limit' : int(config['limit']),
    'show_queries' : config['show_queries'],
    'id_field' : stc.colsnames_educations_sber['id_field']['mongo_name'],
    'path' : dp.Path.pars_path('/candidates/Candidates//'),
    'companies' : config[env]['sberbank']['companies'],
    'batch_size': config['batch_size']
}