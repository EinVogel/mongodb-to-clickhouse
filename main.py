import pymongo as pm
import pandas as pd
import time
from datetime import datetime, date, timedelta
import yaml
import pdb
import os

import libs.data_parser as dp
import libs.structure as stc
import libs.mongo_clickhouse as mc
import confs.utils as util
from confs.utils import OrgUnits_parameters_dict, \
    candidates_parameters_dict, \
    videointerview_parameters_dict,\
    sber_educations_parameters_dict
from testing.delete_table import drop_table

with open('confs/conf.yml', 'r') as yamlfile:
    config = yaml.load(yamlfile)

env = 'dev'


# Наиболее отлаженный и полноценный датасет – кандитаты pikta. Остальные делаются по аналогии
# Принимает на вход объект params из config/utils.py, который содержит все данные о датасете
def process_candidates(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по кандидатам')
# Проверяем соединение с КХ
    connection_check = mc.check_for_connection(params)
    if connection_check:
        # Создаем базу в КХ
        create_db = mc.create_db(params)
        # Засекаем время
        start_time_total = time.time()
        # Создание таблицы. Требует сгенерированного объекта в structure.py
        cand_table = mc.create_table(params)
        skip = 0
        while True:
            # Генерация запроса к Mongo
            mongo_query, query_text = mc.create_mongodb_query(params, min_date)
            # Получение данных из Mongo
            candidates_to_cursor = mc.select_mongodb(params, mongo_query, query_text).skip(skip)
            # Преобразование Mongo данных в DataFrame
            candidates_df = mc.cursor_to_df(candidates_to_cursor)
            if candidates_df.shape[0] != 0:
                # Обработка данных из Mongo. Получение нужных полей
                candidates_prep = mc.cand_df_preparation(params, candidates_df)
                # Присоединеие заявок к датасету
                jobr_prep = mc.cand_jobr_df_join(params, candidates_prep)
                # Исключение статусов присутвующих в КХ
                updated_df = mc.update_df(params, jobr_prep, min_date)
                # Приведение к плоской структуре
                flat_dataset =mc.df_to_flat(params, updated_df)
                if flat_dataset.shape[0] == 0:
                    skip += int(params['batch_size']/2)
                elif flat_dataset.shape[0] > 0:
                    skip = 0
                # Добавление столбца _date_for_clickhouse
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                # Подготовка к отправке данных в КХ в качестве строки
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                # Отправка данных в КХ
                candidates_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if candidates_to_clickhouse:
                    print('Кандидаты выгружены в Clickhouse')
                else:
                    print('Ошибка - Кандидаты не выгружены')
            else:
                print('Нет кандидатов за выбранный период')
                return True
            if candidates_df.shape[0] < params['batch_size']:
                return True
        print('Работа окончена')


# Наиболее отлаженный и полноценный датасет – кандитаты pikta. Остальные делаются по аналогии
# Принимает на вход объект params из config/utils.py, который содержит все данные о датасете
def process_orgunits(params, min_date):
    print()
    # лимит -1 в файле значит - выводим все данные
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по кандидатам')
    # Проверяем соединение с КХ
    # Функция check_for_connection вызывается из файла
    connection_check = mc.check_for_connection(params)
    if connection_check:
        # Создаем базу в КХ
        create_db = mc.create_db(params)
        # Засекаем время
        start_time_total = time.time()
        # Создание таблицы. Требует сгенерированного объекта в structure.py
        cand_table = mc.create_table(params)
        skip = 0
        while True:
            # Генерация запроса к Mongo
            mongo_query, query_text = mc.create_mongodb_query(params, min_date)
            # Получение данных из Mongo
            candidates_to_cursor = mc.select_mongodb(params, mongo_query, query_text).skip(skip)
            # Преобразование Mongo данных в DataFrame
            candidates_df = mc.cursor_to_df(candidates_to_cursor)
            if candidates_df.shape[0] != 0:
                # Обработка данных из Mongo. Получение нужных полей
                candidates_prep = mc.org_df_preparation(params, candidates_df)
                # Присоединеие заявок к датасету
                # jobr_prep = mc.cand_jobr_df_join(params, candidates_prep)
                # # Исключение статусов присутвующих в КХ
                # updated_df = mc.update_df(params, jobr_prep, min_date)
                # # Приведение к плоской структуре
                # flat_dataset = mc.df_to_flat(params, updated_df)
                # if flat_dataset.shape[0] == 0:
                #     skip += int(params['batch_size']/2)
                # elif flat_dataset.shape[0] > 0:
                #     skip = 0
                # Добавление столбца _date_for_clickhouse
                #
                preprocessed_dataset = mc.data_preprocess(params, candidates_prep)
                # Подготовка к отправке данных в КХ в качестве строки
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                # # Отправка данных в КХ
                candidates_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)
                #
                # if candidates_to_clickhouse:
                #     print('Кандидаты выгружены в Clickhouse')
                # else:
                #     print('Ошибка - Кандидаты не выгружены')
            else:
                print('Нет кандидатов за выбранный период')
                return True
            if candidates_df.shape[0] < params['batch_size']:
                return True
        print('Работа окончена')


def process_bids(params, min_date):
    print()
    print('Начали работу по заявкам')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        jobr_table = mc.create_table(params)
        while True:
            jobr_mongodb_query, query_text = mc.create_jobr_mongodb_query(params, min_date)
            jobr_to_cursor = mc.select_jobr_data(params, jobr_mongodb_query, query_text)
            jobr_df = mc.cursor_to_df(jobr_to_cursor)
            if jobr_df.shape[0] != 0:
                jobr_prep = mc.jobr_data_preparation(params, jobr_df)
                jobr_data_updated = mc.update_df(params, jobr_prep, min_date)
                jobr_data_flat = mc.jobr_data_to_flat(params, jobr_data_updated)
                jobr_data_preprocessed = mc.data_preprocess(params, jobr_data_flat)
                jobr_query_data = mc.df_to_str(params, jobr_data_preprocessed)
                jobr_load_to_clickhouse = mc.data_to_clickhouse(params, jobr_query_data, start_time_total)

                if jobr_load_to_clickhouse:
                    print('Заявки выгружены в clickhouse')
                else:
                    print('Ошибка - Заявки не выгружены')
            else:
                print('Нет заявок за выбранный период')
                return True
            if jobr_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения с Clickhouse')
    print('Работа окончена')


def process_communications(params, min_date):
    print()
    print()
    print('Начали работу по коммуникациям (смс+мейлы+звонки)')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        comm_table = mc.create_table(params)

        sms_query, restrictions, sms_text = mc.create_comm_query(params, min_date, 'SMS')
        sms_cursor = mc.select_sms_mongodb(params, sms_query, restrictions, sms_text)
        sms_df = mc.cursor_to_df(sms_cursor)
        comm_df = pd.DataFrame()
        if sms_df.shape[0] != 0:
            sms_prep = mc.comm_df_preparation(params, sms_df, 'SMS')
            comm_df = comm_df.append(sms_prep)
        else:
            print('Нет SMS за данный период')
        email_query, restrictions, email_text = mc.create_comm_query(params, min_date, 'Email')
        email_cursor = mc.select_email_mongodb(params, email_query, restrictions, email_text)
        email_df = mc.cursor_to_df(email_cursor)
        if email_df.shape[0] != 0:
            email_prep = mc.comm_df_preparation(params, email_df, 'Email')
            comm_df = comm_df.append(email_prep)
        else:
            print('Нет email\'ов за данный период')
        call_query, restrictions, call_text = mc.create_comm_query(params, min_date, 'Call')
        call_cursor = mc.select_call_mongodb(params, call_query, restrictions, call_text)
        call_df = mc.cursor_to_df(call_cursor)
        if call_df.shape[0] != 0:
            call_prep = mc.comm_df_preparation(params, call_df, 'Call')
            comm_df = comm_df.append(call_prep)
        else:
            print('Нет звонков за данный период')
        comm_df = comm_df.reset_index(drop=True)
        try:
            df_to_clh_query = mc.df_to_str(params, comm_df)
            comm_to_clickhouse = mc.data_to_clickhouse(params, df_to_clh_query, start_time_total)
        except:
            print('Нет данных для выгрузки')
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_appointment(params, min_date):
    print()
    print()
    print('Начали работу по мероприятиям')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        appoint_table = mc.create_table(params)
        while True:
            appointment_query, query_text = mc.create_appointment_query(params, min_date)
            appointment_cursor = mc.select_mongodb(params, appointment_query, query_text)
            appointment_df = mc.cursor_to_df(appointment_cursor)
            if appointment_df.shape[0] != 0:
                appointment_prep = mc.appointment_df_preparation(params, appointment_df)
                app_df_to_clh_query = mc.df_to_str(params, appointment_prep)
                appointment_to_clickhouse = mc.data_to_clickhouse(params, app_df_to_clh_query, start_time_total)
                if appointment_to_clickhouse:
                    print('Мероприятия выгружены в clickhouse')
                else:
                    print('Ошибка - Мероприятия не выгружены')
            else:
                print('Нет мероприятий за выбранный период')
                return True
            if appointment_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_users(params, min_date):
    print()
    print()
    print('Начали работу по Users')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        users_drop = mc.drop_table(params)
        users_table = mc.create_table(params)
        users_query, query_text = mc.create_query(params, min_date)
        users_cursor = mc.select_mongodb(params, users_query, query_text)
        users_df = mc.cursor_to_df(users_cursor)
        if users_df.shape[0] != 0:
            users_prep = mc.users_df_preparation(params, users_df)
            users_df_to_clh_query = mc.df_to_str(params, users_prep)
            users_to_clickhouse = mc.data_to_clickhouse(params, users_df_to_clh_query, start_time_total)
            if users_to_clickhouse:
                print('User\'ы успешно выгружены')
            else:
                print('Ошибка - User\'ы не выгружены')
        else:
            print('Нет User\'ов  за выбранный период')
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_scenarios_sber(params, min_date):
    print()
    print()
    print('Начали работу по Scenarios')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        scenarios_drop = mc.drop_table(params)
        scenarios_table = mc.create_table(params)

        scenarios_query, query_text = mc.create_query(params, min_date)
        scenarios_cursor = mc.select_mongodb(params, scenarios_query, query_text)
        scenarios_df = mc.cursor_to_df(scenarios_cursor)
        if scenarios_df.shape[0] != 0:
            scenarios_prep = mc.scenarios_df_preparation(params, scenarios_df)
            scenarios_df_to_clh_query = mc.df_to_str(params, scenarios_prep)
            scenarios_to_clickhouse = mc.data_to_clickhouse(params, scenarios_df_to_clh_query, start_time_total)
            if scenarios_to_clickhouse:
                print('Scenarios успешно выгружены')
            else:
                print('Ошибка - Scenarios не выгружены')
        else:
            print('Нет Scenarios за выбранный период')
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_user_teams(params, min_date):
    print()
    print('Начали работу по User Team')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        user_team_drop = mc.drop_table(params)
        user_team_table = mc.create_table(params)

        user_team_query, query_text = mc.create_query(params, min_date)
        user_team_cursor = mc.select_mongodb(params, user_team_query, query_text)
        user_team_df = mc.cursor_to_df(user_team_cursor)
        if user_team_df.shape[0] != 0:
            user_team_prep = mc.user_team_df_preparation(params, user_team_df)
            user_team_df_to_clh_query = mc.df_to_str(params, user_team_prep)
            user_team_to_clickhouse = mc.data_to_clickhouse(params, user_team_df_to_clh_query, start_time_total)
            if user_team_to_clickhouse:
                print('User Team успешно выгружены')
            else:
                print('Ошибка - User Team не выгружены')
        else:
            print('Нет User Teams за выбранный период')
        print('Работа окончена')
    else:
        print('Нет соединения c Clickhouse')


def process_client(params, min_date):
    print()
    print()
    print('Начали работу по клиентам')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        client_drop = mc.drop_table(params)
        client_table = mc.create_table(params)

        client_query, query_text = mc.create_query(params, min_date)
        client_cursor = mc.select_mongodb(params, client_query, query_text)
        client_df = mc.cursor_to_df(client_cursor)
        if client_df.shape[0] != 0:
            client_prep = mc.client_df_preparation(params, client_df)
            client_df_to_clh_query = mc.df_to_str(params, client_prep)
            client_to_clickhouse = mc.data_to_clickhouse(params, client_df_to_clh_query, start_time_total)
            if client_to_clickhouse:
                print('Клиенты успешно выгружены')
            else:
                print('Ошибка - Клиенты не выгружены')
        else:
            print('Нет клиентов за выбранный период')
        print('Работа окончена')
    else:
        print('Нет соединения c Clickhouse')


def process_vacancies(params, min_date):
    print()
    print()
    print('Начали выгрузку вакансий')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        vacancies_drop = mc.drop_table(params)
        vacancies_table = mc.create_table(params)
        while True:
            vacancies_query, query_text = mc.create_query(params, min_date)
            vacancies_cursor = mc.select_mongodb(params, vacancies_query, query_text)
            vacancies_df = mc.cursor_to_df(vacancies_cursor)
            if vacancies_df.shape[0] != 0:
                vacancies_prep = mc.vacancies_df_preparation(params, vacancies_df)
                vacancies_df_to_clh_query = mc.df_to_str(params, vacancies_prep)
                vacanices_to_clickhouse = mc.data_to_clickhouse(params, vacancies_df_to_clh_query, start_time_total)
            else:
                print('Нет вакансий за выбранный период')
                return True
            if vacancies_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_candidates_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по кандидатам')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        cand_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_mongodb_query(params, min_date)
            candidates_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            candidates_df = mc.cursor_to_df(candidates_to_cursor)
            if candidates_df.shape[0] != 0:
                candidates_prep = mc.cand_df_preparation(params, candidates_df)
                data_for_query = mc.df_to_str(params, candidates_prep)
                candidates_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)
                if candidates_to_clickhouse:
                    print('Кандидаты выгружены в clickhouse')
                else:
                    print('Ошибка - Кандидаты не выгружены')
            else:
                print('Нет кандидатов за выбранный период')
                return True
            if candidates_df.shape[0] < params['batch_size']:
                return True


def process_statuses_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по статусам')

    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        status_table = mc.create_table(params)
        skip = 0
        while True:
            mongo_query, query_text = mc.create_statuses_mongodb_query(params, min_date, 'batch')
            candidates_to_cursor = mc.select_mongodb(params, mongo_query, query_text).skip(skip)
            statuses_df = mc.cursor_to_df(candidates_to_cursor)
            if statuses_df.shape[0] != 0:
                statuses_prep = mc.stat_df_preparation(params, statuses_df)
                updated_df = mc.update_df(params, statuses_prep, min_date)
                flat_dataset =mc.statuses_to_flat(params, updated_df)
                if flat_dataset.shape[0] == 0:
                    skip += int(params['batch_size']/2)
                elif flat_dataset.shape[0] > 0:
                    skip = 0
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                candidates_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if candidates_to_clickhouse:
                    print('Статусы выгружены в clickhouse')
                else:
                    print('Ошибка - Статусы не выгружены')
            else:
                print('Нет статусов за выбранный период')
                return True
            if statuses_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_applications_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Applications')

    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        app_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_app_query(params, min_date)
            apps_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            apps_df = mc.cursor_to_df(apps_to_cursor)
            if apps_df.shape[0] != 0:
                apps_prep = mc.apps_df_preparation(params, apps_df)
                data_for_query = mc.df_to_str(params, apps_prep)
                apps_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if apps_to_clickhouse:
                    print('Applications выгружены в clickhouse')
                else:
                    print('Ошибка - Applications не выгружены')
            else:
                print('Нет Applications за выбранный период')
                return True
            if apps_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_notifications_sber(params, min_date):
    print()
    print('Лимит ' + str(params ['limit']) + ' записей')
    print('Начали работу по нотификациям')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        notifications_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_notif_query(params, min_date)
            notifications_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            notifications_df = mc.cursor_to_df(notifications_to_cursor)
            if notifications_df.shape[0] != 0:
                notifications_prep = mc.notif_df_preparation(params, notifications_df)
                flat_dataset =mc.notif_to_flat(params, notifications_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                notifications_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if notifications_to_clickhouse:
                    print('Нотификации выгружены в clickhouse')
                else:
                    print('Ошибка - Нотификации не выгружены')
            else:
                print('Нет нотификаций за выбранный период')
                return True
            if notifications_df.shape[0] < params['batch_size']:
                return True
    print('Работа окончена')


def process_tags(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по тегам')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        tags_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_tags_query(params, min_date)
            tags_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            tags_df = mc.cursor_to_df(tags_to_cursor)
            if tags_df.shape[0] != 0:
                tags_prep = mc.tags_df_preparation(params, tags_df)
                flat_dataset = mc.tags_to_flat(params, tags_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                tags_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if tags_to_clickhouse:
                    print('Теги выгружены в clickhouse')
                else:
                    print('Ошибка - Теги не выгружены')
            else:
                print('Нет тегов за выбранный период')
                return True
            if tags_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_tags_other(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по тегам')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        tags_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_other_tags_query(params, min_date)
            tags_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            tags_df = mc.cursor_to_df(tags_to_cursor)
            if tags_df.shape[0] != 0:
                tags_prep = mc.other_tags_df_preparation(params, tags_df)
                flat_dataset = mc.other_tags_to_flat(params, tags_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                tags_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if tags_to_clickhouse:
                    print('Теги выгружены в clickhouse')
                else:
                    print('Ошибка - Теги не выгружены')
            else:
                print('Нет тегов за выбранный период')
                return True
            if tags_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_educations_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Educations')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        educations_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_educations_query(params, min_date)
            eds_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            eds_df = mc.cursor_to_df(eds_to_cursor)
            if eds_df.shape[0] != 0:
                eds_prep = mc.eds_df_preparation(params, eds_df)
                flat_dataset = mc.eds_to_flat(params, eds_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                eds_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if eds_to_clickhouse:
                    print('Educations выгружены в clickhouse')
                else:
                    print('Ошибка - Educations не выгружены')
            else:
                print('Нет Educations за выбранный период')
                return True
            if eds_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_videointerview(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по videointerview')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        videointerview_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_videointerview_query(params, min_date)
            eds_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            eds_df = mc.cursor_to_df(eds_to_cursor)
            if eds_df.shape[0] != 0:
                eds_prep = mc.video_preparation(params, eds_df)
                flat_dataset = mc.videointerview_to_flat(params, eds_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                eds_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if eds_to_clickhouse:
                    print('Educations выгружены в clickhouse')
                else:
                    print('Ошибка - videointerviews не выгружены')
            else:
                print('Нет videointerviews за выбранный период')
                return True
            if eds_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_works_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Works')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        works_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_works_query(params, min_date)
            works_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            works_df = mc.cursor_to_df(works_to_cursor)
            if works_df.shape[0] != 0:
                works_prep = mc.works_df_preparation(params, works_df)
                flat_dataset = mc.works_to_flat(params, works_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                works_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if works_to_clickhouse:
                    print('Works выгружены в clickhouse')
                else:
                    print('Ошибка - Works не выгружены')
            else:
                print('Нет Works за выбранный период')
                return True
            if works_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_comments_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Comments')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        comments_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_comments_query(params, min_date)
            comments_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            comments_df = mc.cursor_to_df(comments_to_cursor)
            if comments_df.shape[0] != 0:
                comments_prep = mc.comments_df_preparation(params, comments_df)
                flat_dataset = mc.comments_to_flat(params, comments_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                comments_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if comments_to_clickhouse:
                    print('Comments выгружены в clickhouse')
                else:
                    print('Ошибка - Comments не выгружены')
            else:
                print('Нет Comments за выбранный период')
                return True
            if comments_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_vacancies_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по вакансиям')

    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        vacancies_drop = mc.drop_table(params)
        vacancies_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_vacancy_query(params, min_date)
            vacancies_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            vacancies_df = mc.cursor_to_df(vacancies_to_cursor)
            if vacancies_df.shape[0] != 0:
                vacancies_prep = mc.vacancies_df_preparation(params, vacancies_df)
                data_for_query = mc.df_to_str(params, vacancies_prep)
                vacancies_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)
                if vacancies_to_clickhouse:
                    print('Вакансии выгружены в clickhouse')
                else:
                    print('Ошибка - Вакансии не выгружены')
            else:
                print('Нет вакансий за выбранный период')
                return True
            if vacancies_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_campaign_call_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Campaign Call List Item')

    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        campaign_call_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_campaign_call_query(params, min_date)
            campaign_call_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            campaign_call_df = mc.cursor_to_df(campaign_call_to_cursor)
            if campaign_call_df.shape[0] != 0:
                campaign_call_prep = mc.campaign_call_df_preparation(params, campaign_call_df)
                data_for_query = mc.df_to_str(params, campaign_call_prep)
                campaign_call_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)
                if campaign_call_to_clickhouse:
                    print('Campaign Call List Item выгружены в clickhouse')
                else:
                    print('Ошибка - Campaign Call List Item не выгружены')
            else:
                print('Нет Campaign Call List Item за выбранный период')
                return True
            if campaign_call_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_sources(params, min_date):
    print()
    print()
    print('Начали выгрузку дополнительных источников')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        sources_drop = mc.drop_table(params)
        sources_table = mc.create_table(params)

        sources_query, query_text = mc.create_query(params, min_date)
        sources_cursor = mc.select_mongodb(params, sources_query, query_text)
        sources_df = mc.cursor_to_df(sources_cursor)
        if sources_df.shape[0] != 0:
            sources_prep = mc.sources_df_preparation(params, sources_df)
            sources_df_to_clh_query = mc.df_to_str(params, sources_prep)
            sources_to_clickhouse = mc.data_to_clickhouse(params, sources_df_to_clh_query, start_time_total)
            if sources_to_clickhouse:
                print('Дополнительные источники выгружены в clickhouse')
            else:
                print('Ошибка - Дополнительные источники не выгружены')
        else:
            print('Нет дополнительных источников за выбранный период')
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_vacancy_sources_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по привязанным источникам вакансий')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        sources_drop = mc.drop_table(params)
        sources_table = mc.create_table(params)

        mongo_query, query_text = mc.create_vacancy_sources_query(params, min_date)
        vacancies_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
        vacancies_df = mc.cursor_to_df(vacancies_to_cursor)
        if vacancies_df.shape[0] != 0:
            sources_prep = mc.vacancy_sources_df_preparation(params, vacancies_df)
            flat_dataset = mc.vacancy_sources_to_flat(params, sources_prep)
            preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
            data_for_query = mc.df_to_str(params, preprocessed_dataset)
            sources_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

            if sources_to_clickhouse:
                print('Источники выгружены в clickhouse')
            else:
                print('Ошибка - Источники не выгружены')
        else:
            print('Нет источников за выбранный период')
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_quiz_results_sber(params, min_date):
    print()
    print('Лимит ' + str(params ['limit']) + ' записей')
    print('Начали работу по Quiz Interview')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        quiz_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_quiz_query(params, min_date)
            quiz_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            quiz_df = mc.cursor_to_df(quiz_to_cursor)
            if quiz_df.shape[0] != 0:
                quiz_prep = mc.quiz_df_preparation(params, quiz_df)
                flat_dataset = mc.quiz_to_flat(params, quiz_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                quiz_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if quiz_to_clickhouse:
                    print('Quiz Interview выгружены в clickhouse')
                else:
                    print('Ошибка - Quiz Interview не выгружены')
            else:
                print('Нет Quiz Interview за выбранный период')
                return True
            if quiz_df.shape[0] < params['batch_size']:
                return True
    print('Работа окончена')


def process_simulation_interview_sber(params, min_date):
    print()
    print('Лимит ' + str(params ['limit']) + ' записей')
    print('Начали работу по Simulation Interview')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        quiz_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_query(params, min_date)
            quiz_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            quiz_df = mc.cursor_to_df(quiz_to_cursor)
            if quiz_df.shape[0] != 0:
                quiz_prep = mc.simulation_df_preparation(params, quiz_df)
                preprocessed_dataset = mc.data_preprocess(params, quiz_prep)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                quiz_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if quiz_to_clickhouse:
                    print('Simulation Interview выгружены в clickhouse')
                else:
                    print('Ошибка - Simulation Interview не выгружены')
            else:
                print('Нет Simulation Interview за выбранный период')
                return True
            if quiz_df.shape[0] < params['batch_size']:
                return True
    print('Работа окончена')


def process_video_results_sber(params, min_date):
    print()
    print('Лимит ' + str(params ['limit']) + ' записей')
    print('Начали работу по Video Interview')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        quiz_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_video_query(params, min_date)
            quiz_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            quiz_df = mc.cursor_to_df(quiz_to_cursor)
            if quiz_df.shape[0] != 0:
                quiz_prep = mc.video_df_preparation(params, quiz_df)
                flat_dataset = mc.video_to_flat(params, quiz_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                quiz_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if quiz_to_clickhouse:
                    print('Video Interview выгружены в clickhouse')
                else:
                    print('Ошибка - Video Interview не выгружены')
            else:
                print('Нет Video Interview за выбранный период')
                return True
            if quiz_df.shape[0] < params['batch_size']:
                return True
    print('Работа окончена')


def process_audio_results_sber(params, min_date):
    print()
    print('Лимит ' + str(params ['limit']) + ' записей')
    print('Начали работу по Audio Interview')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        quiz_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_audio_query(params, min_date)
            quiz_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            quiz_df = mc.cursor_to_df(quiz_to_cursor)
            if quiz_df.shape[0] != 0:
                quiz_prep = mc.audio_df_preparation(params, quiz_df)
                flat_dataset = mc.video_to_flat(params, quiz_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                quiz_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if quiz_to_clickhouse:
                    print('Audio Interview выгружены в clickhouse')
                else:
                    print('Ошибка - Audio Interview не выгружены')
            else:
                print('Нет Audio Interview за выбранный период')
                return True
            if quiz_df.shape[0] < params['batch_size']:
                return True
    print('Работа окончена')


def process_candidates_request(params, min_date):
    print()
    print()
    print('Начали выгрузку Candidates Request')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        sources_table = mc.create_table(params)
        while True:
            sources_query, query_text = mc.create_query(params, min_date)
            sources_cursor = mc.select_mongodb(params, sources_query, query_text)
            sources_df = mc.cursor_to_df(sources_cursor)
            if sources_df.shape[0] != 0:
                sources_prep = mc.candidates_request_df_preparation(params, sources_df)
                sources_df_to_clh_query = mc.df_to_str(params, sources_prep)
                sources_to_clickhouse = mc.data_to_clickhouse(params, sources_df_to_clh_query, start_time_total)
                if sources_to_clickhouse:
                    print('Candidates Request выгружены в clickhouse')
                else:
                    print('Ошибка - Candidates Request не выгружены')
            else:
                print('Нет Candidates Request за выбранный период')
                return True
            if sources_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_questionnaire(params, min_date):
    print()
    print()
    print('Начали работу по Анкетам')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        questionnaire_table = mc.create_table(params)
        while True:
            users_query, query_text = mc.create_quest_query(params, min_date)
            users_cursor = mc.select_mongodb(params, users_query, query_text)
            users_df = mc.cursor_to_df(users_cursor)
            if users_df.shape[0] != 0:
                users_prep = mc.questionnaire_df_preparation(params, users_df)
                users_df_to_clh_query = mc.df_to_str(params, users_prep)
                users_to_clickhouse = mc.data_to_clickhouse(params, users_df_to_clh_query, start_time_total)
                if users_to_clickhouse:
                    print('Анкеты успешно выгружены')
                else:
                    print('Ошибка - Анкеты не выгружены')
            else:
                print('Нет Анкет за выбранный период')
                return True
            if users_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_jobr_sber(params, min_date):
    print()
    print()
    print('Начали работу по Заявкам')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        questionnaire_table = mc.create_table(params)
        while True:
            users_query, query_text = mc.create_jobr_sber_query(params, min_date)
            users_cursor = mc.select_mongodb(params, users_query, query_text)
            users_df = mc.cursor_to_df(users_cursor)
            if users_df.shape[0] != 0:
                users_prep = mc.jobr_sber_df_preparation(params, users_df)
                users_df_to_clh_query = mc.df_to_str(params, users_prep)
                users_to_clickhouse = mc.data_to_clickhouse(params, users_df_to_clh_query, start_time_total)
                if users_to_clickhouse:
                    print('Заявки успешно выгружены')
                else:
                    print('Ошибка - Заявки не выгружены')
            else:
                print('Нет Заявок за выбранный период')
                return True
            if users_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_ird_stat(params, min_date):
    print()
    print()
    print('Начали работу по Ird Stat')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        stat_table = mc.create_table(params)
        while True:
            stat_query, query_text = mc.create_query(params, min_date)
            del stat_query["$and"][1]
            stat_cursor = mc.select_mongodb(params, stat_query, query_text)
            stat_df = mc.cursor_to_df(stat_cursor)
            if stat_df.shape[0] != 0:
                stat_prep = mc.ird_stat_df_preparation(params, stat_df)
                stat_df_to_clh_query = mc.df_to_str(params, stat_prep)
                stat_to_clickhouse = mc.data_to_clickhouse(params, stat_df_to_clh_query, start_time_total)
                if stat_to_clickhouse:
                    print('Ird Stat успешно выгружены')
                else:
                    print('Ошибка - Ird Stat не выгружены')
            else:
                print('Нет Ird Stat за выбранный период')
                return True
            if stat_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_ird_results(params, min_date):
    print()
    print()
    print('Начали работу по Ird Results')
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        create_db = mc.create_db(params)
        stat_table = mc.create_table(params)
        while True:
            stat_query, query_text = mc.create_query(params, min_date)
            del stat_query["$and"][1]
            stat_cursor = mc.select_mongodb(params, stat_query, query_text)
            stat_df = mc.cursor_to_df(stat_cursor)
            if stat_df.shape[0] != 0:
                stat_prep = mc.ird_results_df_preparation(params, stat_df)
                stat_df_to_clh_query = mc.df_to_str(params, stat_prep)
                stat_to_clickhouse = mc.data_to_clickhouse(params, stat_df_to_clh_query, start_time_total)
                if stat_to_clickhouse:
                    print('Ird Results успешно выгружены')
                else:
                    print('Ошибка - Ird Results не выгружены')
            else:
                print('Нет Ird Results за выбранный период')
                return True
            if stat_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


def process_task_sber(params, min_date):
    print()
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по Task')

    pd.options.mode.chained_assignment = None
    start_time_total = time.time()
    connection_check = mc.check_for_connection(params)
    if connection_check:
        works_table = mc.create_table(params)
        while True:
            mongo_query, query_text = mc.create_query(params, min_date)
            works_to_cursor = mc.select_mongodb(params, mongo_query, query_text)
            works_df = mc.cursor_to_df(works_to_cursor)
            if works_df.shape[0] != 0:
                works_prep = mc.task_df_preparation(params, works_df)
                flat_dataset = mc.task_to_flat(params, works_prep)
                preprocessed_dataset = mc.data_preprocess(params, flat_dataset)
                data_for_query = mc.df_to_str(params, preprocessed_dataset)
                works_to_clickhouse = mc.data_to_clickhouse(params, data_for_query, start_time_total)

                if works_to_clickhouse:
                    print('Task выгружены в clickhouse')
                else:
                    print('Ошибка - Task не выгружены')
            else:
                print('Нет Task за выбранный период')
                return True
            if works_df.shape[0] < params['batch_size']:
                return True
    else:
        print('Нет соединения c Clickhouse')
    print('Работа окончена')


# Блок выгрузки данных
print('Сервис запущен')

drop_table(OrgUnits_parameters_dict)
print('- Удаление таблицы ', OrgUnits_parameters_dict['full_tbl_name'], ' прошло успешно')
drop_table(videointerview_parameters_dict)
print('- Удаление таблицы ', videointerview_parameters_dict['full_tbl_name'], ' прошло успешно')

try:
    try:
        min_date = datetime.strptime(config['date_loadout'], ('%Y, %m, %d, %H, %M'))  # start date

        counter = 0
        while True:
            counter += 1
            # Реализация выгрузки для prod
            # Выгрузка дял остальных окружений
            process_orgunits(util.OrgUnits_parameters_dict, min_date)
            process_videointerview(util.videointerview_parameters_dict, min_date)
            time.sleep(60 * config['time_to_sleep_success_min'])
    except KeyboardInterrupt:
        print()
        print('Сервис остановлен')
        print()
except KeyboardInterrupt:
    pass
