import numpy as np
import pandas as pd
import datetime
import time
import pdb

import libs.data_parser as dp
import libs.structure as stc


# Функции для работы с данными 'мероприятия' сбербанк
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}

    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['sf_candidate_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['skillaz_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['candidate_email']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['sf_job_requisition_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['is_evergreen']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['is_standard']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['transferred_from']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['salary']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_probation']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_start_date']
    dict_data[t_col['name']] = list(map(dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_trainee_status']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_work_schedule']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['security_add_info_request']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['security_check_result']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['created_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['created_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['updated_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['deleted_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['current_status_sf_label']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['current_status_sf_set_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['current_status_value']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['current_status_set_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['interview_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['interview_is_finished']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))

    res = pd.DataFrame(dict_data)
    return res
