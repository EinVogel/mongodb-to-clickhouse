import numpy as np
import pandas as pd
import time
import datetime
import pdb
import libs.data_parser as dp

# Функции для работы с данными 'Анкетам'
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['updated_at_mil']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_miliseconds, list(map(dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))
    ))

    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['sf_status']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['import_status']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['last_modified_datetime']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    t_col = coldict['division']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['internal_title']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['external_title']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['recruiter_email']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['title']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['origination_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['initiator']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['address']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['openings_filled_count']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['candidates_in_progress_count']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['rated_applicant_count']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['recruiter_groups']
    field = dp.force_take_series(data, t_col['mongo_name'])
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['recruiter_contacts']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['salary_min']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['salary_mid']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['salary_max']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['recruiter_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['recruiting_groups']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['department']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['is_evergreen']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['is_standard']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['app_template_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['hiring_manager']
    field = dp.force_take_series(data, t_col['mongo_name'])
    # дополнительная обработка, в случае, когда тип вложенной структуры - dict
    field_new = [[a] for a in field]
    dict_data[t_col['name']] = dp.nested_cols(t_col, field_new)

    t_col = coldict['job_start_date']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    t_col = coldict['hh']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['sj']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_city']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_city_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_go_sb']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))

    res = pd.DataFrame(dict_data)
    res = res.dropna().reset_index()
    return res
