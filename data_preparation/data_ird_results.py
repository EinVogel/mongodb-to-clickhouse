import numpy as np
import pandas as pd
import time
import datetime

import libs.data_parser as dp

# Функции для работы с данными 'пользователи'
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_tin']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_passport_num']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_passport_date']
    dict_data[t_col['name']] = list(map(dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])))
    
    t_col = coldict['cust_passport_s']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_passport_issued_by']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['cust_snils']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))

    res = pd.DataFrame(dict_data)
    res = res.dropna().reset_index()
    return res
