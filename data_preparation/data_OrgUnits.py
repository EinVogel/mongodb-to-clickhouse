import numpy as np
import pandas as pd
import datetime
import time
import pdb

import libs.data_parser as dp
import libs.structure as stc

# Функции для работы с данными 'кандидаты'

# Словарь дополнительных источников
other_sours = {
    0 : 'null',
    1 : 'zarplata.ru',
    2 : 'Отклик zarplata.ru',
    3 : 'rabota.ru',
    4 : 'Отклик rabota.ru',
    5 : 'Видео-интервью',
    6 : 'Прочие работные сайты',
    7 : 'Отклик прочие работные сайты',
    8 : 'СС VK',
    9 : 'СС Facebook',
    10: 'СС OK',
    11: 'СС профессиональные',
    12: 'Лендинг',
    13: 'Листовки',
    14: 'Баннеры наружные',
    15: 'Реклама на транспорте',
    16: 'ВУЗы',
    17: 'Газеты',
    18: 'Рекомендации',
    19: 'Реклама в интернете',
    20: 'Перенос',
    21: 'Чита.ру',
    22: 'РоботВера',
    23: 'Instagram',
    24: 'Иное',
    25: 'Radar - мобильные приложения',
    26: 'SMS-рассылка',
    27: 'Реклама в кинотеатре',
    28: 'Тайм-кафе (антикафе)',
    29: 'Ярмарка вакансий',
    30: 'Talantix',
    31: 'FarPost',
    32 : 'Yandeх Direct',
    33 : 'Google Direct',
    34 : 'РК VK',
    35 : 'Viber'
}

# Сохраняем сколько разных статусов было у записи.
def n_status_change(status_history):
    # Добавляем единичку так как текущий статус считается отдельно.
    return pd.Series(map(lambda item: len(item) + 1, status_history))

# Добираемся до значений статуса по путю в зависимости от того текущий он или бывший
def path_for_status(i, l, param):
    fulpath = dp.Path()
    fulpath.db = 'candidates'
    fulpath.collection = 'Candidates'
    #  Если статус текущий - берем данные из другого поля так как в StatusHistory этой информации нет.
    if i < l:
        fulpath.fields = {stc.special_mongo_names['status_history_cand']: 1}
        fulpath.path = str(i) + '/' + param
    else:
        fulpath.fields = {stc.special_mongo_names['current_status_cand']: 1}
        fulpath.path = param
    return fulpath


# Берет на вход датафрейм из кандидатов и выдает датафрейм ID кандидатов и заявок
def jobr_join_prepare(data, f1, f2, r1, r2):
    res = []
    for i in range(data.shape[0]):
        if type(data[f2].loc[i]) == float:
            data[f2].loc[i] = []
        for j in range(len(data[f2].loc[i])):
            if (data[f2].loc[i] != []):
                res.append([data[f2].loc[i][j], data[f1].loc[i]])
    res = pd.DataFrame(res, columns = [r1, r2])
    return res


# Принимает на вход связь c MongoDB и DataFrame выгруженных кандидатов.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null
def preparation(conn, data, coldict, scope):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    m_current_status = stc.special_mongo_names['current_status_cand']

    dict_data = {}

    # Для последующего разложения на статусы
    dict_data[m_status_history] = list(map(dp.some_to_null, dp.force_take_series(data, m_status_history)))
    dict_data[m_current_status] = list(map(dp.some_to_null, dp.force_take_series(data, m_current_status)))

    # Присоединение id кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение id компаниии
    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение id клиента
    t_col = coldict['client_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение контактных данных
    t_col = coldict['contacts']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение имени
    t_col = coldict['name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение города
    t_col = coldict['city']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение улицы
    t_col = coldict['street']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение дома
    t_col = coldict['house']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение дома
    t_col = coldict['building']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение описания
    t_col = coldict['description']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение ориентира
    t_col = coldict['landmark']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение широты
    t_col = coldict['latitude']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение широты
    t_col = coldict['longitude']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))

    # Присоединение responsible_ids
    t_col = coldict['responsible_ids']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение hire_needs
    t_col = coldict['hire_needs']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение is_archived
    t_col = coldict['is_archived']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение кол-ва кандидатов
    t_col = coldict['employees_total']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение общего коэффициента занятости
    t_col = coldict['occupancy_rate_total']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение кол-ва открытых позиций
    t_col = coldict['open_positions_total']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение id ответственного сотрудника
    t_col = coldict['responsible_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение staff_total
    t_col = coldict['staff_total']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение даты закрытия
    t_col = coldict['closing_date']
    dict_data[t_col['name']] = list(map(dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение текста поиска
    t_col = coldict['text_search']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение геолокации
    t_col = coldict['geo_location']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение даты с которой выгружаются кандидаты
    t_col = coldict['status_set_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    res = pd.DataFrame(dict_data)
    return res


# Принимает на вход DataFrame по кандидатам.
# Выдает новый DataFrame, где на каждый статус записи создается новая запись.
def to_flat(data, coldict, field_last_upd):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    n_statuse_change = coldict['n_statuse_change']['name']

    data[n_statuse_change] = n_status_change(dp.force_take_series(data, m_status_history))
    flat_cols = [ x for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT ]
    old_cols_names = [ x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_PREPARATION ]
    t_time = time.time()
    t_len = 0
    new_rows = list()

    for i, row in data.iterrows():

        if i % 100 == 0 and i != 0:
            d_time = time.time() - t_time
            d_len = len(new_rows) - t_len
            print(' - Обрабатываем элемент номер '
                  + str(i)
                  + ' - '
                  + str("%.4f" % (d_time))
                  + ' секунд; '
                  + str(d_len)
                  + ' новых записей; '
                  + str("%.4f" % (float(d_len)/d_time))
                  + ' записей в секунду'
                 )
            t_time = time.time()
            t_len = len(new_rows)

        for j in range(row[n_statuse_change]):

            date_check = dp.under_value_line(
                row,
                path_for_status(j, row[n_statuse_change]-1, stc.special_mongo_names['set_at'])
            )
            try:
                date_check = date_check.replace(microsecond=0)
            except:
                date_check = row[field_last_upd]

            if row[field_last_upd] < date_check:
                t_new_value_row = np.empty(shape=[0, 0])
                for k in range(len(flat_cols)):
                    if flat_cols[k]['colsubtype'] == stc.COLSUBTYPE_FLATPATH:
                        new_value = dp.under_value_line(
                            row,
                            path_for_status(j, row[n_statuse_change]-1, flat_cols[k]['path'])
                        )
                        # Дополнительно обрабатываем дату
                        if flat_cols[k]['name'] == coldict['status_set_at']['name']:
                            new_value = dp.iso_date_to_datetime(new_value)

                        if flat_cols[k]['name'] == coldict['status_set_at_mil']['name']:
                            new_value = dp.iso_date_to_miliseconds(new_value)

                        if flat_cols[k]['name'] == coldict['status_stage_order']['name']:
                            try:
                                new_value = int(new_value)
                            except:
                                new_value = new_value
                    else:
                        if flat_cols[k]['name'] == coldict['status_index']['name']:
                            new_value = j

                    t_new_value_row = np.append(t_new_value_row, dp.some_to_null(new_value))
                new_rows.append(np.append(row[old_cols_names].values, t_new_value_row))

    res = pd.DataFrame(new_rows, columns = [
        x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT or x['coltype'] == stc.COLTYPE_PREPARATION
    ])

    return res
