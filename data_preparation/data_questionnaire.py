import numpy as np
import pandas as pd
import time
import datetime
import pdb
import libs.data_parser as dp

# Функции для работы с данными 'Анкетам'
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['last_name']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['first_name']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['middle_name']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['other_names']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['date_of_birth']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['place_of_birth']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_citizenship']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['gender']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['email']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cell_phone']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_phone_fact']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_phone_reg']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['address']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_address_reg']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_tin']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_passport_date_string']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_passport_issued_by']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_passport_num']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_passport_serial']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_int_passport']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_military_duty']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_military_exempt_reason']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_criminal_liability']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_marital_status']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_relative_sb']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_relative_sb_instruction']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['cust_relative_info']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['education_type']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['education']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['academic_degree']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['work_experience_comment']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['outside_work_experience']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['cust_combine_jobs']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_combine_jobs_instr']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['cust_authorized_capital']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_authorized_capital_instr']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['cust_authorized_capital_rel']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_authorized_capital_rel_instr']
    field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['cust_question_29_sb_partner_rel']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_question_29_sb_partner_rel_comment']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_question_30_civil_service']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_snils']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['cust_sb_check']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))
    res = pd.DataFrame(dict_data)
    res = res.dropna().reset_index()
    return res
