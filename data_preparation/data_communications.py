import numpy as np
import pandas as pd
import time
import datetime

import libs.data_parser as dp

# Функции для работы с данными 'коммуникации'
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict, comm_type, commtype_path):

    dict_data = {}
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['channel']
    dict_data[t_col['name']] = np.full(data.shape[0], comm_type)

    t_col = coldict['comm_id']
    dict_data[t_col['name']] = dp.conc_series(dict_data['_channel'], dict_data['_id'])

    t_col = coldict['candidate_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['datetime']
    dict_data[t_col['name']] = list(map(dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(commtype_path + 'AuditInfo/UpdatedAt'))))

    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['vacancy_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
         conn,
         data,
         key1=coldict['vacancy_id']['mongo_name'],
         vpath='/workspace/vacancies/name//',
         key2='_id',
         new_name=t_col['name'],
        )

    t_col = coldict['vacancy_location']
    dict_data[t_col['name']] = dp.join_by_keys_res(
         conn,
         data,
         key1=coldict['vacancy_id']['mongo_name'],
         vpath='/workspace/vacancies/city//',
         key2='_id',
         new_name=t_col['name'],
        )

    t_col = coldict['vacancy_department_id']
    dict_data[t_col['name']] = dp.join_by_keys_res(
         conn,
         data,
         key1=coldict['vacancy_id']['mongo_name'],
         vpath='/workspace/vacancies/DepartmentId//',
         key2='_id',
         new_name=t_col['name'],
        )

    t_col = coldict['vacancy_department_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
          conn,
          dp.join_by_keys(
                     conn,
                     data,
                     key1=coldict['vacancy_id']['mongo_name'],
                     vpath='/workspace/vacancies/DepartmentId//',
                     key2='_id',
                     new_name='DepartmentId',
                    ),
          key1='DepartmentId',
          vpath='/workspace/Departments/Name//',
          key2='_id',
          new_name=t_col['name'],
         )

    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['company_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
          conn,
          data,
          key1=coldict['company_id']['mongo_name'],
          vpath='/workspace/companies/name//',
          key2='_id',
          new_name=t_col['name'],
         )

    t_col = coldict['message_type']
    if comm_type == 'Call':
        dict_data[t_col['name']] = np.full(data.shape[0], 'null')
    else:
        dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_datetime')))

    res = pd.DataFrame(dict_data)
    return res
