import numpy as np
import pandas as pd
import datetime
import time

import libs.data_parser as dp
import libs.structure as stc


# Функции для работы непосредственно с нашими данными по заявкам


# Словари (пока что захардкоженные в продукт)

# Словарь статусов заявки
jobr_stats = {
    0 : 'Unknown',
    1 : 'Created',
    2 : 'Approved',
    3 : 'Rejected',
    4 : 'Closed',
    5 : 'Stopped',
    6 : 'ExplanationNeeded',
    7 : 'InWork',
}

# Словарь дополнительных источников
other_sours = {
    0 : 'null',
    1 : 'zarplata.ru',
    2 : 'Отклик zarplata.ru',
    3 : 'rabota.ru',
    4 : 'Отклик rabota.ru',
    5 : 'Видео-интервью',
    6 : 'Прочие работные сайты',
    7 : 'Отклик прочие работные сайты',
    8 : 'СС VK',
    9 : 'СС Facebook',
    10: 'СС OK',
    11: 'СС профессиональные',
    12: 'Лендинг',
    13: 'Листовки',
    14: 'Баннеры наружные',
    15: 'Реклама на транспорте',
    16: 'ВУЗы',
    17: 'Газеты',
    18: 'Рекомендации',
    19: 'Реклама в интернете',
    20: 'Перенос',
    21: 'Чита.ру',
    22: 'РоботВера',
    23: 'Instagram',
    24: 'Иное',
    25: 'Radar - мобильные приложения',
    26: 'SMS-рассылка',
    27: 'Реклама в кинотеатре',
    28: 'Тайм-кафе (антикафе)',
    29: 'Ярмарка вакансий',
    30: 'Talantix',
    31: 'FarPost',
}

# Сохраняем сколько разных статусов было у записи.
def n_status_change(status_history):
    # Все статусы включая текущий есть в StatusHistory.
    return pd.Series(map(lambda item: len(item), status_history))

# Добираемся до значений статуса по путю в зависимости от того текущий он или бывший
def path_for_status(i, l, param):
    fulpath = dp.Path()
    fulpath.db = 'workspace'
    fulpath.collection = 'JobRequisitions'
    fulpath.fields = {stc.special_mongo_names['status_history_jobr']: 1}
    fulpath.path = str(i) + '/' + param
    return fulpath

# Принимает на вход связь c MongoDB и DataFrame выгруженных заявок.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_jobr']

    dict_data = {}

    # Для последующего разложения на статусы
    dict_data[m_status_history] = list(map(dp.some_to_null, dp.force_take_series(data, m_status_history)))

    # Присоединение ID заявки
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение даты создания заявки
    t_col = coldict['created_on']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    # Присоединение Id ответственного менеджера
    t_col = coldict['responsible_manager_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение ФИО ответсвенного менеджера
    t_col = coldict['responsible_manager_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['responsible_manager_id']['mongo_name'],
        vpath = '/Auth/User/Fio//',
        key2 = coldict['id_field']['mongo_name'],
        new_name = t_col['name'],
    )

    # Присоединение Id компании
    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение названия компании
    t_col = coldict['company_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['company_id']['mongo_name'],
        vpath = '/workspace/companies/name//',
        key2 = coldict['id_field']['mongo_name'],
        new_name = t_col['name'],
    )

    # Присоединение должности заявки
    t_col = coldict['title']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение позиции по должности (стажер, ...)
    t_col = coldict['position']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение даты и времени начала исполнения заявки
    t_col = coldict['jobreq_work_start_on']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    # присоединение срока закрытия заявки
    t_col = coldict['deadline']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.force_take_series(data, t_col['mongo_name'])
    ))

    # Присоединение Id вакансии
    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение названия вакансии
    t_col = coldict['vacancy_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/name//',
        key2 = coldict['id_field']['mongo_name'],
        new_name = t_col['name'],
    )

    # Присоединение города
    t_col = coldict['adress_city']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение полного адреса
    t_col = coldict['adress_full']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение строчности заявки
    t_col = coldict['is_urgent_tf']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение описания заявки
    t_col = coldict['description']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение типа договора
    t_col = coldict['contract_type']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение графика работы
    t_col = coldict['schedule']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение формата обучения
    t_col = coldict['education_format']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение срока обучения
    t_col = coldict['education_period']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение количества вакансий
    t_col = coldict['candidates_to_hire']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение коэффициента расчета потребности в кандидатах по целевому процессу
    t_col = coldict['candidates_coefficient']
    dict_data[t_col['name']] = np.full(data.shape[0], 1.4)

    # Присоединение потребности в кандидатах по заявке
    t_col = coldict['candidates_limit']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение количества смен статуса заявки
    t_col = coldict['n_statuse_change']
    dict_data[t_col['name']] = list(map(dp.some_to_null, n_status_change(data[m_status_history])))

    res = pd.DataFrame(dict_data)
    return res

# Принимает на вход DataFrame по заявкам.
# Выдает новый DataFrame, где на каждый статус записи создается новая запись.
def to_flat(data, coldict, field_last_upd):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_jobr']
    n_statuse_change = coldict['n_statuse_change']['name']

    data[n_statuse_change] = n_status_change(dp.force_take_series(data, m_status_history))
    flat_cols = [ x for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT ]
    old_cols_names = [ x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_PREPARATION ]
    t_time = time.time()
    t_len = 0
    new_rows = []

    for i, row in data.iterrows():

        if i % 100 == 0 and i != 0:
            d_time = time.time() - t_time
            d_len = len(new_rows) - t_len
            print(' - Обрабатываем элемент номер '
                  + str(i)
                  + ' - '
                  + str("%.4f" % (d_time))
                  + ' секунд; '
                  + str(d_len)
                  + ' новых записей; '
                  + str("%.4f" % (float(d_len)/d_time))
                  + ' записей в секунду'
                 )
            t_time = time.time()
            t_len = len(new_rows)

        status_index = 0
        for j in range(row[n_statuse_change]):

            date_check = dp.under_value_line(
                row,
                path_for_status(j, row[n_statuse_change]-1, stc.special_mongo_names['set_at'])
            )
            date_check = date_check.replace(microsecond=0)
            null_check = dp.under_value_line(
                row,
                path_for_status(j, row[n_statuse_change]-1, stc.special_mongo_names['status_jobr'])
            )

            if null_check != 'null' and null_check != 'None':
                if row[field_last_upd] < date_check:
                    t_new_value_row = np.empty(shape=[0, 0])
                    for k in range(len(flat_cols)):
                        if flat_cols[k]['colsubtype'] == stc.COLSUBTYPE_FLATPATH:
                            new_value = dp.under_value_line(
                                row,
                                path_for_status(j, row[n_statuse_change]-1, flat_cols[k]['path'])
                            )
                            # Дополнительно обрабатываем дату
                            if flat_cols[k]['name'] == coldict['status_set_at']['name']:
                                new_value = dp.iso_date_to_datetime(new_value)
                            # Дополнительно обрабатываем название статуса
                            elif flat_cols[k]['name'] == coldict['status_name']['name']:
                                new_value = dp.from_dict(new_value, jobr_stats)

                        else:
                            if flat_cols[k]['name'] == coldict['status_index']['name']:
                                new_value = status_index

                        t_new_value_row = np.append(t_new_value_row, dp.some_to_null(new_value))

                    new_rows.append(np.append(row[old_cols_names].values, t_new_value_row))

            # Делаем так, что бы не учитывать статус если там стоит null
            status_index += 1

    res = pd.DataFrame(new_rows, columns = [
        x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT or x['coltype'] == stc.COLTYPE_PREPARATION
    ])

    return res
