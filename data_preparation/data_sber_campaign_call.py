import numpy as np
import pandas as pd
import datetime
import time

import libs.data_parser as dp
import libs.structure as stc


# Функции для работы с данными 'вакансии ' сбербанк
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}

    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['campaign_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['mighty_call_campaign_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['customer_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['status']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['priority']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['timezone']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['is_exit']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['phone_number']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['display_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['audit_created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['audit_created_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['audit_created_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['audit_updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['audit_updated_at_mil']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_miliseconds, list(map(dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))
    ))

    t_col = coldict['audit_updated_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['audit_updated_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['audit_deleted_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['audit_deleted_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['audit_deleted_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_audit_updated_at')))

    res = pd.DataFrame(dict_data)
    return res
