import numpy as np
import pandas as pd
import datetime
import time
import pdb

import libs.data_parser as dp
import libs.structure as stc

# Сохраняем сколько разных статусов было у записи.
def n_status_change(status_history):
    # Добавляем единичку так как текущий статус считается отдельно.
    return pd.Series(map(lambda item: len(item) + 1, status_history))

# Добираемся до значений статуса по путю в зависимости от того текущий он или бывший
def path_for_status(i, l, param):
    fulpath = dp.Path()
    fulpath.db = 'candidates'
    fulpath.collection = 'Candidates'
    #  Если статус текущий - берем данные из другого поля так как в StatusHistory этой информации нет.
    if i < l:
        fulpath.fields = {stc.special_mongo_names['status_history_cand']: 1}
        fulpath.path = str(i) + '/' + param
    else:
        fulpath.fields = {stc.special_mongo_names['current_status_cand']: 1}
        fulpath.path = param
    return fulpath

# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    m_current_status = stc.special_mongo_names['current_status_cand']

    dict_data = {}

    # Для последующего разложения на статусы
    dict_data[m_status_history] = list(map(dp.some_to_null, dp.force_take_series(data, m_status_history)))
    dict_data[m_current_status] = list(map(dp.some_to_null, dp.force_take_series(data, m_current_status)))

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['n_statuse_change']
    dict_data[t_col['name']] = list(map(dp.some_to_null, n_status_change(data[m_status_history])))

    res = pd.DataFrame(dict_data)
    return res


def to_flat(data, coldict, field_last_upd):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    n_statuse_change = coldict['n_statuse_change']['name']

    data[n_statuse_change] = n_status_change(dp.force_take_series(data, m_status_history))
    flat_cols = [ x for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT ]
    old_cols_names = [ x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_PREPARATION ]
    t_time = time.time()
    t_len = 0
    new_rows = list()

    for i, row in data.iterrows():

        if i % 100 == 0 and i != 0:
            d_time = time.time() - t_time
            d_len = len(new_rows) - t_len
            print(' - Обрабатываем элемент номер '
                  + str(i)
                  + ' - '
                  + str("%.4f" % (d_time))
                  + ' секунд; '
                  + str(d_len)
                  + ' новых записей; '
                  + str("%.4f" % (float(d_len)/d_time))
                  + ' записей в секунду'
                 )
            t_time = time.time()
            t_len = len(new_rows)


        for j in range(row[n_statuse_change]):

            date_check = dp.under_value_line(
                row,
                path_for_status(j, row[n_statuse_change]-1, stc.special_mongo_names['set_at'])
            )
            try:
                date_check = date_check.replace(microsecond=0)
            except:
                date_check = row[field_last_upd]
            if date_check == 'null':
                date_check = row[field_last_upd]

            if row[field_last_upd] < date_check:
                t_new_value_row = np.empty(shape=[0, 0])
                for k in range(len(flat_cols)):
                    if flat_cols[k]['colsubtype'] == stc.COLSUBTYPE_FLATPATH:
                        new_value = dp.under_value_line(
                            row,
                            path_for_status(j, row[n_statuse_change]-1, flat_cols[k]['path'])
                        )
                        # Дополнительно обрабатываем дату
                        if flat_cols[k]['name'] == coldict['status_set_at']['name']:
                            new_value = dp.iso_date_to_datetime(new_value)

                        if flat_cols[k]['name'] == coldict['status_set_at_mil']['name']:
                            new_value = dp.iso_date_to_miliseconds(new_value)
                    else:
                        if flat_cols[k]['name'] == coldict['status_index']['name']:
                            new_value = j

                    t_new_value_row = np.append(t_new_value_row, dp.some_to_null(new_value))
                new_rows.append(np.append(row[old_cols_names].values, t_new_value_row))

    res = pd.DataFrame(new_rows, columns = [
        x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT or x['coltype'] == stc.COLTYPE_PREPARATION
    ])

    return res
