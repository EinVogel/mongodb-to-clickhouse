import numpy as np
import pandas as pd
import datetime
import time

import libs.data_parser as dp
import libs.structure as stc

# Функции для работы с данными 'Response'
# Каждый блок ответственен за обработку своего параметра, заданного в stucture.py
# dict_data - словарь с данными для дальнейшей отправки в Clickhouse
# t_col['mongo_name'] - значение ключа 'mongo_name' указанного в структуре
# t_col['path'] - полный путь к данным, используеться для вложенных структур
# Для вложенных структур используется dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
# Для данных "на поверхности" используется dp.force_take_series(data, t_col['mongo_name'])
# Для даты после извлечения данных они дополнительно мапаются функцией dp.iso_date_to_datetime
# Для Nested структур используется функция dp.nested_cols
# Для остальных типов данных, данные мапаются функцией dp.some_to_null

def preparation(conn, data, coldict):

    dict_data = {}

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['phone_number']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['company_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['company_id']['mongo_name'],
        vpath = '/workspace/companies/name//',
        key2 = '_id',
        new_name = t_col['name'],
    )

    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение названия вакансии
    t_col = coldict['vacancy_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/name//',
        key2 = '_id',
        new_name = t_col['name'],
    )

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['salary_from']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/salary//',
        key2 = '_id',
        new_name = t_col['name'],
    )
    for i in range(len(dict_data['_salary_from'])):
        if dict_data['_salary_from'][i] == 'null':
            dict_data['_salary_from'][i] = 0
        else:
            dict_data['_salary_from'][i] = dict_data['_salary_from'][i]['from']

    t_col = coldict['salary_to']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/salary//',
        key2 = '_id',
        new_name = t_col['name'],
    )
    for i in range(len(dict_data['_salary_to'])):
        if dict_data['_salary_to'][i] == 'null':
            dict_data['_salary_to'][i] = 0
        else:
            dict_data['_salary_to'][i] = dict_data['_salary_to'][i]['to']

    t_col = coldict['requisition_id']
    dict_data[t_col['name']] = dp.under_value_df(data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['city']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/city//',
        key2 = '_id',
        new_name = t_col['name'],
    )

    t_col = coldict['requisition_location']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['vacancy_location']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1 = coldict['vacancy_id']['mongo_name'],
        vpath = '/workspace/vacancies/RecruiterContacts//',
        key2 = '_id',
        new_name = t_col['name'],
    )
    for i in range(len(dict_data['_vacancy_location'])):
        try:
            dict_data['_vacancy_location'][i] = dict_data['_vacancy_location'][i][0]
        except:
            dict_data['_vacancy_location'] = dict_data['_vacancy_location']
    dict_data['_vacancy_location'] = list(map(dp.some_to_null_loc, dict_data['_vacancy_location']))

    t_col = coldict['comm_channel']
    dict_data[t_col['name']] = ['phoneCall' for i in range(data.shape[0])]

    t_col = coldict['response']
    dict_data[t_col['name']] = list(map(dp.addway_to_binary, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['interview_appeared']
    interview = []
    for i in range(data.shape[0]):
        if ('interviewAppeared' in data['CurrentStatus'][i].values()):
            interview.append(1)
        elif any(d.get('_id', None) == 'interviewAppeared' for d in data['StatusHistory'][i]):
            interview.append(1)
        elif data['CompanyId'][i] == 'zwwX3KYPBGdbcN6Xa':
            if any(d.get('_id', None) == 'groupInterview' for d in data['StatusHistory'][i]) or ('groupInterview' in data['CurrentStatus'][i].values()):
                if any(d.get('_id', None) == 'groupInterviewDidNotCome' for d in data['StatusHistory'][i]) or ('groupInterviewDidNotCome' in data['CurrentStatus'][i].values()):
                    interview.append(0)
                else:
                    interview.append(1)
            else:
                interview.append(0)
        else:
            interview.append(0)
    dict_data[t_col['name']] = interview

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_created_at')))

    res = pd.DataFrame(dict_data)

    return res
