import numpy as np
import pandas as pd
import dpath.util
import datetime
import re
import pdb
import dateutil.parser
import libs.my_clickhouse as clh

# Функции для работы с MongoDB

# Класс для указания пути к данным в MongoDB. Умеет парсить строку. Ниже функции тащат данные через него.
class Path:

    def __init__(self):
        self.db = ''
        self.collection = ''
        self.fields = {}
        self.path = ''

    # Парсинг пути до Mongo поля
    def pars_path(fullpath):
        r = Path()
        try:
            options = fullpath.split('/', maxsplit=3)
            r.db = options[1]
            r.collection = options[2]
            r.path = options[3]
            if options[3].split('/')[0] != '':
                r.fields = {options[3].split('/')[0]: 1}
        except:
            print('Error: Wrong fullpath')
        return r

    # Курсор для Mongo
    def to_cursor(conn, my_path, query):
        if my_path.fields != {}:
            cursor = conn[my_path.db][my_path.collection].find(query, my_path.fields)
        else:
            cursor = conn[my_path.db][my_path.collection].find(query)
        return cursor

    # Курсор для Mongo датасета Communications. Со включением ограничений
    def comm_to_cursor(conn, my_path, query, restictions):
        cursor = conn[my_path.db][my_path.collection].find(query, restictions)
        return cursor

    # Подсчёт количества записей
    def cursor_count(conn, my_path, query):
        if my_path.fields != {}:
            cursor = conn[my_path.db][my_path.collection].count(query, my_path.fields)
        else:
            cursor = conn[my_path.db][my_path.collection].count(query)
        return cursor
    # Получение всех компаний в датасете. Для other scope
    def get_companies(conn, my_path):
        cursor = conn[my_path.db][my_path.collection].distinct('CompanyId')
        cursor = list(cursor)
        return cursor

# Выкачиваем информацию из переданого указателя в базе
def cursor_to_df(cursor):
    return pd.DataFrame(list(cursor))

# Построчная конкатенация двух Series
def conc_series(S1, S2):
    newseries = pd.Series([])
    for i in range(S1.shape[0]):
        newseries[i] = str(S1[i]) + '_' + str(S2[i])
    return newseries

# Добираемся до значения, внутри словаря
def under_value_dict(upDict, path):
    val = ''
    try:
        val = dpath.util.get(upDict, path)
    except:
        val = 'null'
    val = some_to_null(val)
    return val

# Добираемся до значения, внутри строчки
def under_value_line(up_line, fullpath):
    field = list(fullpath.fields.keys())[0]
    try:
        val = under_value_dict(up_line[field], fullpath.path)
    except:
        val = fullpath.path
    return val

# Добираемся до значения внутри DataFrame
def under_value_df(df, fullpath):
    field = list(fullpath.fields.keys())[0]
    try:
        newseries = pd.Series(len(df[field]))
        path = fullpath.path.split('/', maxsplit=1)
        for i, item in enumerate(df[field]):
            try:
                newseries[i] = under_value_dict(item, path[1])
            except:
                newseries[i] = 'null'
    except:
        newseries = pd.Series('null', index=range(0, len(df)))
    return newseries

# Делаем джойн DataFrame и данных из базы по ключу. Получаем Dataframe.
def join_by_keys(conn, id_df, key1, vpath, key2, new_name):
    if not(new_name in id_df.columns.values):
        v_p = Path.pars_path(vpath)
        v_DF = cursor_to_df(Path.to_cursor(conn, v_p, {}))
        df = id_df.join(v_DF.set_index(key2), on=key1)
        df = df.rename(columns={list(v_p.fields.keys())[0]: new_name})
    else:
        df = id_df
    return df

def allbut(df, but):
    names = df.columns
    names = set(names)
    names = [a for a in names if a not in but]
    return names

# Делаем джойн DataFrame и данных из базы по ключу. Кастомная функция по
# присоединению ссылок на вакансии. Тестировалась на коллекциях HHVacancy, SJVacancy, ZpVacancy
def join_by_keys_links(conn, id_df, key1, vpath, key2, new_name):
    if not(new_name in id_df.columns.values):
        v_p = Path.pars_path(vpath)
        v_p.fields[key2] = 1
        v_DF = cursor_to_df(Path.to_cursor(conn, v_p, {}))
        # Разложения поля, по которому происходит присоединение, если это поле - массив
        v_DF = v_DF.dropna(subset=[key2]) \
            [key2].apply(pd.Series) \
            .merge(v_DF, right_index = True, left_index = True) \
            .drop([key2], axis = 1) \
            .melt(id_vars = [list(v_p.fields.keys())[0], '_id'], value_name = key2) \
            .drop(["variable"], axis = 1) \
            .dropna()
        if key2 != "_id":
            v_DF = v_DF.drop("_id", axis = 1)
        df = id_df.join(v_DF.set_index(key2), on=key1)
        # Формирование массива из значений, которые присоединяются, в случае если по одной вакансии несколько ссылок
        df = df.rename(columns={list(v_p.fields.keys())[0]: new_name})
        df = df.drop(allbut(df, [new_name, "_id"]), axis=1) \
            .groupby("_id")[new_name].apply(list) \
            .reset_index().set_index("_id")
        df = id_df.join(df, on=key1)
    else:
        df = id_df
    return df

# Делаем джойн DataFrame и данных из базы по ключу. Получаем Series.
def join_by_keys_res(conn, id_df, key1, vpath, key2, new_name):
    return list(map(some_to_null, join_by_keys(conn, id_df, key1, vpath, key2, new_name)[new_name]))

# Делаем джойн DataFrame и данных из базы по ключу. Получаем Series.
def join_by_keys_res_links(conn, id_df, key1, vpath, key2, new_name):
    return list(map(some_to_null, join_by_keys_links(conn, id_df, key1, vpath, key2, new_name)[new_name]))

# Получаем данные из DataFrame. Если их нет - получаем Series из 'null'
def force_take_series(data, name):
    try:
        return data[name]
    except:
        return np.full(data.shape[0], 'null')

# Проверяем в каких элементах Series есть вхождение строки
def check_if_sub_str_series(arr, s):
    n_series = pd.Series(len(arr))
    for i, item in enumerate(arr):
        if str(item) == 'null':
            n_series[i] = 'null'
        elif item.find(s) > -1:
            n_series[i] = True
        else:
            n_series[i] = False
    return n_series

# Вычисляем количество элементов в массиве не учитывая 'null'-ы возвращаем число
def true_len_arr(arr):
    res = 0
    for item in arr:
        if some_to_null(item) != 'null':
            res += 1
    return res

# Сохраняем сколько разных записей было в массивах, возвращаем Series
def n_arr(arr):
    n_series = pd.Series(len(arr))
    for i, item in enumerate(arr):
        if some_to_null(item) == 'null':
            n_series[i] = 0
        else:
            n_series[i] = true_len_arr(item)
    return n_series


# Функции для работы с датами

# Превращаем строку, инт или дату в дату
def iso_date_to_datetime(iso_date):
    res = 'null'
    if isinstance(iso_date, int):
        if iso_date >= 0:
            res = datetime.datetime.utcfromtimestamp(iso_date / 1e9)
    elif isinstance(iso_date, str):
        try:
            if iso_date != 'null' and int(iso_date) >= 0:
                res = datetime.datetime.strptime(iso_date, '%Y-%m-%d %H:%M:%S')
        except:
            res = datetime.datetime.strptime(iso_date, '%Y-%m-%dT%H:%M:%SZ')
    elif isinstance(iso_date, datetime.datetime):
        if iso_date != datetime.datetime(1, 1, 1, 0, 0):
            res = iso_date
    else:
        try:
            res = datetime.datetime.strptime(str(iso_date), '%Y-%m-%d %H:%M:%S')
        except:
            res = 'null'
    return res

def iso_date_to_miliseconds(iso_date):
    try:
        res = int(iso_date.microsecond/1000)
    except:
        res = 0
    return res

def addway_to_binary(addway):
    res = addway
    if addway != 'negotiation':
        res = 'search'
    return res

def birth_process(date):
    res = 'null'
    if date != 'null':
        res = date + datetime.timedelta(seconds=1)

    return res

# Вычисляем дату n лет назад (нужно для проверки весокосных годов)
def years_ago(years, from_date=None):
    if from_date is None:
        from_date = datetime.datetime.now()
    try:
        return from_date.replace(year=from_date.year - years)
    except ValueError:
        # Must be 2/29!
        assert from_date.month == 2 and from_date.day == 29 # can be removed
        return from_date.replace(month=2, day=28,
                                 year=from_date.year-years)

# Вычисляем сколько лет прошло с указанной даты
def num_years(begin, end=None):
    try:
        if end is None:
            end = datetime.datetime.now()
        num_years = int((end - begin).days / 365.25)
        if begin > years_ago(num_years, end):
            return num_years - 1
        else:
            return num_years
    except:
        return 'null'

def nested_cols(t_col, data):
    resp = []
    for i in range(len(data)):
        resp.append([])
        for key in t_col['nested_cols']:
            coltype = t_col['nested_cols'][key]['type']
            key = t_col['nested_cols'][key]['path']
            if key == 'index':
                res = list(range(len(data[i])))
            elif coltype == 'Int8':
                try:
                    res = [clh.any_to_int8('null' if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res  = [False]
            elif coltype == 'Date' or type == 'Nulable(Date)':
                try:
                    res = [clh.any_to_date('null' if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res = ['0000-00-00']
            elif coltype == 'DateTime':
                try:
                    res = [clh.any_to_datetime('null' if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res = ['0000-00-00 00:00:00']
            elif coltype == 'Int64':
                try:
                    res = [int(-1 if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res = [-1]
            elif coltype == 'Float64':
                try:
                    res = [clh.any_to_float64('null' if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res = ['null']
            else:
                try:
                    res = [clh.any_to_string('null' if d.get(key) == None else d.get(key)) for d in data[i]]
                except:
                    res = ['null']

            resp[i].append(res)

    return resp

# Другое

# Если значение похоже на null возвращаем null
def some_to_null(s):
    if str(s) == 'nan' or str(s) == 'None' or str(s) == 'NaT' or s == '':
        return 'null'
    else:
        return s

def some_to_null_loc(s):
    if str(s) == 'nan' or str(s) == 'None' or str(s) == 'NaT' or s == '':
        return 'null'
    elif s == []:
        return 'null'
    elif s == 'n':
        return 'null'
    else:
        return s

def process_arrays(s):
    if type(s) is list:
        if  s == [None] or len(s) == 0:
            return ['null']
        return s
    elif type(s) is str:
        if s == 'null':
            return ['null']
        return list(s)

def process_arrays_num(s):
    if type(s) is list:
        if  s == [None] or len(s) == 0:
            return [-1]
        return s
    elif type(s) is str:
        if s == 'null':
            return [-1]
        return list(s)


# Выводим данные из словаря
def from_dict(ind, my_dict):
    try:
        ind = int(ind)
        return my_dict[ind]
    except:
        return ind

# Создаем строку из набора в массиве со словарями по имени
def arr_to_text(arr):
    arr = some_to_null(arr)
    if arr == 'null':
        return arr
    res = ''
    for item in arr:
        test = some_to_null(item)
        if test != 'null':
            res = res + str(item) + ' '
    if res == '': res = 'null'
    return res
