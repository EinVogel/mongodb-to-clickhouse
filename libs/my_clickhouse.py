from __future__ import unicode_literals
import requests
import codecs
from six import string_types, binary_type, text_type, PY3
import datetime
import pdb
import numpy as np
import pandas as pd


# Преобразует запрос в строчку, которую можно ввести в терминал
def query_for_terminal(conn, q):
    q = term_to_string(q)
    return "echo \"" + q + "\" | curl \"http://" + conn + "/?\" --data-binary @-"


# Декодирует value
def unescape(value):
    return codecs.escape_decode(value)[0].decode('utf-8')


# Парсит tsv
def parse_tsv(line):
    if PY3 and isinstance(line, binary_type):
        line = line.decode()
    if line and line[-1] == '\n':
        line = line[:-1]
    return [unescape(value) for value in line.split(str('\t'))]


# Отправляет строку в clickhouse
def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


def _get(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = r.text
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Insert в КХ
# query - массив из 2 элементов
# query[0] - название таблицы с указанием базы
# query[1] - массив строк, которые необходимо вставить
def insert(url, query, showquery=False):
    r = False
    s = ''
    sub_query = 'INSERT INTO ' + query[0] + ' VALUES'
    for i, q in enumerate(query[1]):
        sub_query = sub_query + ' ' + q
        if i < len(query[1])-1:
            sub_query += ','
        else:
            sub_query += ';'
    if showquery:
        print(sub_query)
    r, _ = _post(url, sub_query.encode('utf-8'))
    if r.status_code != 200:
        s = r.text
    return r, s


# Select в КХ
# query - массив из 2 элементов
# query[0] - названия полей через запятую
# query[1] - текст запроса после 'FROM'
def select(url, query, showquery=False):
    # "SETTINGS max_query_size = 256" - увеличивает квоту на размер запроса
    sub_query = 'SELECT ' + query[0]
    if query[1] != '':
        sub_query = sub_query + ' FROM ' + query[1]
    sub_query = sub_query + ' SETTINGS max_query_size = 100000 FORMAT TabSeparatedWithNamesAndTypes'
    if showquery:
        print(sub_query)
    r, _ = _post(url, sub_query)
    data = []
    field_types = []
    field_names = []
    if r != False and r.status_code == 200:
        lines = r.iter_lines()
        field_names = parse_tsv(next(lines))
        field_types = parse_tsv(next(lines))
        for line in lines:
            data.append(parse_tsv(line))
    return field_names, field_types, data


# Создание таблицы
def create_table(url, query):
    sub_query = 'CREATE TABLE IF NOT EXISTS ' + query[0] + ' ('
    for i, q in enumerate(query[1]):
        sub_query = sub_query + ' ' + q
        if i < len(query[1])-1 :
            sub_query += ','
        else:
            sub_query = sub_query + ') ENGINE = '
    sub_query = sub_query + query[2] + ';'
    _, s = _post(url, sub_query)
    return s


# Создание баз данных с именем db_name
def create_db(url, db_name):
    sub_query = 'CREATE DATABASE  ' + db_name + ';'
    _, s = _post(url, sub_query)
    return s


# удаление таблицы с именем tbl_name
def drop_table(url, tbl_name):
    sub_query = 'DROP TABLE IF EXISTS ' + tbl_name
    _, s = _post(url, sub_query)
    return s


# Проверка на наличие таблицы с именем tbl_name
def exsists_table(url, tbl_name):
    sub_query = 'EXISTS TABLE ' + tbl_name
    _, s = _get(url, sub_query)
    return s


# Наличие соединения с сервером
def connection_exists(url):
    sub_query = 'SELECT 1'
    _, s = _get(url, sub_query)
    return s


# Блок прелбразования словаря в запрос на создание таблицы
# Добавляет движок 'MergeTree'
def cols_to_create_query(coldict, table_name, id_field, date_field):
    res = [
        table_name,
        [],
        'MergeTree(' + date_field + ', (' + id_field + ', ' + date_field + '), 8192)'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    return res


# Добавляет движок 'ReplacingMergeTree'
def cols_to_create_replacing_tree_query(coldict, table_name, id_field, date_field, primary_keys):
    res = [
        table_name,
        [],
        'ReplacingMergeTree(' + date_field + ')' + ' ORDER BY ' + '(' + primary_keys + ')'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    return res


# Блок функций для работы с разными типами данных
def any_to_int8(a):
    if a == True:
        r = '1'
    else:
        r = '0'
    return r


def any_to_date(a):
    r = str(a)[:10]
    if r == 'NaT' or r == 'null':
        r = '0000-00-00'
    return r


def any_to_datetime(a):
    r = str(a)[:19]
    if r == 'NaT' or r == 'null':
        r = '0000-00-00 00:00:00'
    return r


def any_to_int64(a):
    if str(a) != 'null':
        r = str(a)
    else:
        r = str(-1)
    return r


def any_to_float64(a):
    if str(a) != 'null':
        r = str(a)
    else:
        r = 'nan'
    return r


def term_to_string(a):
    r = str(a)
    r = r.replace('\'', '\\\'')
    r = r.replace('\"', '\\\"')
    return r


def any_to_string(a):
    r = str(a)
    r = r.replace('\'', '')
    r = r.replace('\"', '')
    r = r.replace('\\', '')
    return r


# Подготовка запроса insert из dateframe
def df_to_strs(data, coldict):
    res = []
    my_cols = [x for x in coldict.values()]
    for i in range(data.shape[0]):
        res.append('(')
        for j in range(len(my_cols)):
            my_type = my_cols[j]['chtype']
            my_data = data[my_cols[j]['name']][i]
            if my_type == 'Int8':
                res[i] = res[i] + any_to_int8(my_data)
            elif my_type == 'Date':
                res[i] += '\'' + any_to_date(my_data) + '\''
            elif my_type == 'DateTime':
                res[i] += '\'' + any_to_datetime(my_data) + '\''
            elif my_type == 'Int64':
                res[i] = res[i] + any_to_int64(my_data)
            elif my_type == 'Float64':
                res[i] = res[i] + any_to_float64(my_data)
            elif my_type == 'Array(String)':
                res[i] = res[i] + str(list(map(any_to_string, my_data)))
            elif my_type.startswith('Nested'):
                res[i] = res[i] + str(my_data)[1: -1]
            elif my_type == 'Array(Int64)':
                res[i] = res[i] + str(my_data)
            else:
                res[i] = res[i] + '\'' + any_to_string(my_data) + '\''
            if j < len(my_cols)-1:
                res[i] = res[i] + ', '
            else:
                res[i] = res[i] + ')'
    return res


# Получаем на вход DataFrame, название таблицы и DataFrame по колонкам, готовим запрос для insert
def data_to_insert_query(data, coldict, table_name):
    res = [
        table_name,
        []
    ]
    res[1] = df_to_strs(data, coldict)
    return res


# Добавляем столбец даты специально для clickhouse
def add_main_data_col(data, source_name, new_name):
    data[new_name] = list(map(lambda s: datetime.datetime.date(s), data[source_name]))
    return data


# Получаем на вход массив из ID кандидатов, выдаем таблицу с максимальной датой смены статуса
def df_last_state_update(url, ids, db_name, col_dataframe_name, col_db_name, new_col_name):
    query = [
        col_dataframe_name + ", max(" + col_db_name + ")",
        db_name
    ]
    # Фильтр по ID включаем только если записей немного - иначе все будет упираться в размер запроса
    if (ids.shape[0] < 3000):
        query[1] += " where " + col_dataframe_name + " in ("
        for i in range(ids.shape[0]):
            query[1] += '\'' + ids[i] + '\''
            if i < ids.shape[0]-1:
                query[1] += ", "
        query[1] += ")"
    query[1] += " group by " + col_dataframe_name
    data = []
    cols, _, data = select(url, query)
    # Записываем результат в датафрейм
    if (len(cols) > 0):
        res = pd.DataFrame(data, columns = [col_dataframe_name, new_col_name])
        res[new_col_name] = res[new_col_name].apply(
            lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'),
            convert_dtype=True
        )
    else:
        res = pd.DataFrame()
    return res


# Блок получения максимальных значений
# для значение столбца
def take_max_value(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, ['max(' + col_name + ')', full_tbl_name])
    return max_val[0][0]


# для миллисекунды
def take_max_value_mil(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, [col_name + ', ' + col_name + '_mil', full_tbl_name + ' ORDER BY ' + col_name + ' DESC, ' + col_name + '_mil'  +  ' DESC LIMIT 1'])
    return max_val[0][0], max_val[0][1]


# для коммуникации
def take_max_value_comm(clickhouse_url, full_tbl_name, col_name, commtype):
    _, _, max_val = select(clickhouse_url, ['max(' + col_name + ')', full_tbl_name + ' where _channel=' + '\''+ commtype + '\''])
    return max_val[0][0]
