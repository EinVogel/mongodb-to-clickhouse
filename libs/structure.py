S = '_'
COLTYPE_PREPARATION = 'preparation'
COLTYPE_FLAT = 'flat'
COLSUBTYPE_FLATPATH = 'flatpath'
COLTYPE_OTHER = 'other'

# Выносим названия повторяющихся в запросе полей из MongoDB в словарь
special_mongo_names = {
    # Для кандидатов
    'status_history_cand' : 'StatusHistory',
    'current_status_cand' : 'CurrentStatus',
    'candidate_ids'       : 'CandidateIds',

    'notification_history': 'NotificationHistory',
    'current_notification': 'CurrentNotification',

    'tags_history'        : 'TagsHistory',
    'current_tags'        : 'CurrentTags',

    'educations'          : 'CommonCVInfo',
    'educations_field'    : 'Educations',

    'works'               : 'CommonCVInfo',
    'works_field'         : 'WorkExperience',

    'comments'            : 'Notes',
    'comments_filed'      : 'Comments',
    'compenence'          : 'CompenenceAssessments',

    'vacancy_sources'     : 'LinkedNegotiationSources',

    'quiz_results'        : 'Results',

    'video_results'       : 'Results',

    'audio_results'       : 'Results',

    'task_status_history' : 'StateHistory',
    # Для заявок
    'status_history_jobr' : 'StateHistory',
    'status_jobr'         : 'Status',

    # общее
    'set_at'              : 'SetAt',
}

# name - имя структуры в ClickHouse
# chtype - тип данных нужен для создание таблицы ипреобразования данных
# Для типа данных формата Nested нужно в типе данных указывать все остальные столбцы.
# Яркий пример такого типа данных используется в colsnames_questionnare. Также необходимо добавить дополнительный
# Ключ nested_cols с указание пути до структуры и его типа данных.
# coltype - тип столбца. Нужен для обработки данных:
# COLTYPE_PREPARATION - для столбцов обрабатываемых в preparation функции в data_....py - файле
# COLTYPE_FLAT + COLSUBTYPE_FLATPATH- для столбцов раскладываемых в плоские структуры to_flat в data_....py - файле
# mongo_name - имя структуры в MongoDb
# path - используется для вложенных структур, указывать полный путь к структуре

colsnames_scenario_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/Scenarios/AuditInfo/UpdatedAt'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Name'
    },
    'vacancy_ids': {
        'name'      : S + 'vacancy_ids',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyIds'
    },
    'vacancy_links': {
        'name'      : S + 'vacancy_links',
        'chtype'    : '''Nested (
                                vacancy_id String,
                                funnel_id String,
                                status_id String
                                )''',
        'nested_cols': {
                    'vacancy_id' : {
                        'path' : 'VacancyId',
                        'type' : 'String'
                    },
                    'funnel_id' : {
                        'path' : 'FunnelId',
                        'type' : 'String'
                    },
                    'status_id' : {
                        'path' : 'StatusId',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyLinks'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_ird_stat = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'use_recognition': {
        'name'      : S + 'use_recognition',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'UseRecognition'
    },
    'cust_tin': {
        'name'      : S + 'cust_tin',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustTIN'
    },
    'cust_passport_num': {
        'name'      : S + 'cust_passport_num',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportNum'
    },
    'cust_passport_date': {
        'name'      : S + 'cust_passport_date',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportDate'
    },
    'cust_passport_s': {
        'name'      : S + 'cust_passport_s',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportS'
    },
    'cust_passport_issued_by': {
        'name'      : S + 'cust_passport_issued_by',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportIssuedBy'
    },
    'cust_snils': {
        'name'      : S + 'cust_snils',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustSNILS'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/IrdStat/AuditInfo/UpdatedAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_ird_results = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'cust_tin': {
        'name'      : S + 'cust_tin',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustTIN'
    },
    'cust_passport_num': {
        'name'      : S + 'cust_passport_num',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportNum'
    },
    'cust_passport_date': {
        'name'      : S + 'cust_passport_date',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportDate'
    },
    'cust_passport_s': {
        'name'      : S + 'cust_passport_s',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportS'
    },
    'cust_passport_issued_by': {
        'name'      : S + 'cust_passport_issued_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustPassportIssuedBy'
    },
    'cust_snils': {
        'name'      : S + 'cust_snils',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustSNILS'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/IrdResults/AuditInfo/UpdatedAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_jobr_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobRequisitionDocument/AuditInfo/UpdatedAt'
    },
    'updated_at_mil': {
        'name'      : S + 'updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobRequisitionDocument/AuditInfo/UpdatedAt'
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'sf_status': {
        'name'      : S + 'sf_status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SfStatus'
    },
    'import_status': {
        'name'      : S + 'import_status',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ImportStatus'
    },
    'last_modified_datetime': {
        'name'      : S + 'last_modified_datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LastModifiedDateTime'
    },
    'division': {
        'name'      : S + 'division',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Division'
    },
    'internal_title': {
        'name'      : S + 'internal_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'InternalTitle'
    },
    'external_title': {
        'name'      : S + 'external_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ExternalTitle'
    },
    'recruiter_email': {
        'name'      : S + 'recruiter_email',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RecruiterEmail'
    },
    'title': {
        'name'      : S + 'title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Title'
    },
    'origination_name': {
        'name'      : S + 'origination_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'OriginationName'
    },
    'initiator': {
        'name'      : S + 'initiator',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Initiator'
    },
    'address': {
        'name'      : S + 'address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Address'
    },
    'openings_filled_count': {
        'name'      : S + 'openings_filled_count',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'OpeningsFilledCount'
    },
    'candidates_in_progress_count': {
        'name'      : S + 'candidates_in_progress_count',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidatesInProgressCount'
    },
    'rated_applicant_count': {
        'name'      : S + 'rated_applicant_count',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RatedApplicantCount'
    },
    'recruiter_groups': {
        'name'      : S + 'recruiter_groups',
        'chtype'    : '''Nested (
                                _id String,
                                name String,
                                operator_role String,
                                related_id String
                                )''',
        'nested_cols': {
                    '_id' : {
                        'path' : '_id',
                        'type' : 'String'
                    },
                    'name' : {
                        'path' : 'Name',
                        'type' : 'String'
                    },
                    'operator_role' : {
                        'path' : 'OperatorRole',
                        'type' : 'String'
                    },
                    'related_id' : {
                        'path' : 'RelatedId',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RecruiterGroups'
    },
    'recruiter_contacts': {
        'name'      : S + 'recruiter_contacts',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RecruiterContacts'
    },
    'salary_min': {
        'name'      : S + 'salary_min',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SalaryMin'
    },
    'salary_mid': {
        'name'      : S + 'salary_mid',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SalaryMid'
    },
    'salary_max': {
        'name'      : S + 'salary_max',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SalaryMax'
    },
    'recruiter_name': {
        'name'      : S + 'recruiter_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RecruiterName'
    },
    'recruiting_groups': {
        'name'      : S + 'recruiting_groups',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'RecruitingGroups'
    },
    'department': {
        'name'      : S + 'department',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Department'
    },
    'is_evergreen': {
        'name'      : S + 'is_evergreen',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsEvergreen'
    },
    'is_standard': {
        'name'      : S + 'is_standard',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsStandard'
    },
    'app_template_id': {
        'name'      : S + 'app_template_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'AppTemplateId'
    },
    'hiring_manager': {
        'name'      : S + 'hiring_manager',
        'chtype'    : '''Nested (
                                operator_role String,
                                last_name String,
                                phone String,
                                fax String,
                                email String,
                                user_name String,
                                first_name String
                                )''',
        'nested_cols': {
                    'operator_role' : {
                        'path' : 'OperatorRole',
                        'type' : 'String'
                    },
                    'last_name' : {
                        'path' : 'LastName',
                        'type' : 'String'
                    },
                    'phone' : {
                        'path' : 'Phone',
                        'type' : 'String'
                    },
                    'fax' : {
                        'path' : 'Fax',
                        'type' : 'String'
                    },
                    'email' : {
                        'path' : 'Email',
                        'type' : 'String'
                    },
                    'user_name' : {
                        'path' : 'UserName',
                        'type' : 'String'
                    },
                    'first_name' : {
                        'path' : 'FirstName',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'HiringManager'
    },
    'job_start_date': {
        'name'      : S + 'job_start_date',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'JobStartDate'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CreatedAt'
    },
    'hh': {
        'name'      : S + 'hh',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'HH'
    },
    'sj': {
        'name'      : S + 'sj',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SJ'
    },
    'cust_city': {
        'name'      : S + 'cust_city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustCity'
    },
    'cust_city_id': {
        'name'      : S + 'cust_city_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustCityId'
    },
    'cust_go_sb': {
        'name'      : S + 'cust_go_sb',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustGOSB'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_candidates_request_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'start': {
        'name'      : S + 'start',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Start'
    },
    'finish': {
        'name'      : S + 'finish',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Finish'
    },
    'source': {
        'name'      : S + 'source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Source',
    },
    'additional_source': {
        'name'      : S + 'additional_source',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'AdditionalSourceId',
    },
    'comment': {
        'name'      : S + 'comment',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Comment',
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'user_id': {
        'name'      : S + 'user_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'UserId',
    },
    'user_name': {
        'name'      : S + 'user_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'UserName',
    },
    'is_archive': {
        'name'      : S + 'is_archive',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsArchive'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/VacancyCandidatesRequest/AuditInfo/UpdatedAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_video_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'questionnaire_name': {
        'name'      : S + 'questionare_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/VideoInterview/Questionnaire/Name'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/VideoInterview/AuditInfo/UpdatedAt',
    },
    'updated_at_mil': {
        'name'      : S + 'updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/VideoInterview/AuditInfo/UpdatedAt',
    },
    'completed_at': {
        'name'      : S + 'completed_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompletedAt',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'question_order': {
        'name'      : S + 'question_order',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'QuestionOrder'
    },
    'answered_at': {
        'name'      : S + 'answered_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'AnsweredAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_audio_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'questionnaire_name': {
        'name'      : S + 'questionare_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/AudioInterview/Questionnaire/Name'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/AudioInterview/AuditInfo/UpdatedAt',
    },
    'updated_at_mil': {
        'name'      : S + 'updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/AudioInterview/AuditInfo/UpdatedAt',
    },
    'completed_at': {
        'name'      : S + 'completed_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompletedAt',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'question_order': {
        'name'      : S + 'question_order',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'QuestionOrder'
    },
    'answered_at': {
        'name'      : S + 'answered_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'AnsweredAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_quiz_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'questionnaire_name': {
        'name'      : S + 'questionare_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/QuizInterview/Questionnaire/Name'
    },
    'completed_at': {
        'name'      : S + 'completed_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompletedAt',
    },
    'completed_at_mil': {
        'name'      : S + 'completed_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompletedAt',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'question_order': {
        'name'      : S + 'question_order',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'QuestionOrder'
    },
    'answer_text': {
        'name'      : S + 'answer_text',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Answers'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_simulator_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/AuditInfo/UpdatedAt'
    },
    'interview_id': {
        'name'      : S + 'interview_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'InterviewId'
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'questionnaire_id': {
        'name'      : S + 'questionare_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/_id'
    },
    'questionnaire_name': {
        'name'      : S + 'questionare_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/Name'
    },
    'questionnaire_display_name': {
        'name'      : S + 'questionare_display_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/DisplayName'
    },
    'questionnaire_company_id': {
        'name'      : S + 'questionnaire_company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/CompanyId'
    },
    'questionnaire_iframe_url': {
        'name'      : S + 'questionnaire_iframe_url',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/IframeUrl'
    },
    'questionnaire_text': {
        'name'      : S + 'questionnaire_text',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/SimulationInterview/Questionnaire/Text'
    },
    'status': {
        'name'      : S + 'status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Status',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_sources_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Name'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'first_source_sf_source': {
        'name'      : S + 'first_source_sf_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SourceId',
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/AdditionalSources/AuditInfo/UpdatedAt'
    },
    'updated_by': {
        'name'      : S + 'updated_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/AdditionalSources/AuditInfo/UpdatedBy'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_vacancy_sources_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'hh_link': {
        'name'      : S + 'hh_link',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'audit_updated_at': {
        'name'      : S + 'audit_updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/UpdatedAt'
    },
    'source': {
        'name'      : S + 'source',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Source'
    },
    'external_vacancy_id': {
        'name'      : S + 'external_vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'VacancyId'
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'VacancyName'
    },
    'account_id': {
        'name'      : S + 'account_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'AccountId'
    },
    'account_name': {
        'name'      : S + 'account_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'AccountName'
    },
    'last_updated_at': {
        'name'      : S + 'last_updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'LastUpdatedAt'
    },
    'source_index': {
        'name'      : S + 'source_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_vacancies_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'name'
    },
    'tb': {
        'name'      : S + 'tb',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/ExtraData/TerritoryBank'
    },
    'gosb': {
        'name'      : S + 'gosb',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/ExtraData/HeadOffice'
    },
    'audit_created_at': {
        'name'      : S + 'audit_created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/CreatedAt'
    },
    'audit_created_by': {
        'name'      : S + 'audit_created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/CreatedBy'
    },
    'audit_created_by_name': {
        'name'      : S + 'audit_created_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/CreatedByName'
    },
    'audit_updated_at': {
        'name'      : S + 'audit_updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/UpdatedAt'
    },
    'audit_updated_by': {
        'name'      : S + 'audit_updated_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/UpdatedBy'
    },
    'audit_updated_by_name': {
        'name'      : S + 'audit_updated_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/UpdatedByName'
    },
    'audit_deleted_at': {
        'name'      : S + 'audit_deleted_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/DeletedAt'
    },
    'audit_deleted_by': {
        'name'      : S + 'audit_deleted_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/DeletedBy'
    },
    'audit_deleted_by_name': {
        'name'      : S + 'audit_deleted_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/DeletedByName'
    },
    'contact_phone': {
        'name'      : S + 'contact_phone',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ContactPhone'
    },
    'gmt': {
        'name'      : S + 'gmt',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Gmt'
    },
    'landing_key': {
        'name'      : S + 'landing_key',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LandingKey'
    },
    'funnel_id': {
        'name'      : S + 'funnel_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'FunnelId'
    },
    'salary_from': {
        'name'      : S + 'salary_from',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/salary/from'
    },
    'salary_to': {
        'name'      : S + 'salary_to',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/salary/to'
    },
    'city': {
        'name'      : S + 'city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'city'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'createdAt'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'updatedAt'
    },
    'deleted_at': {
        'name'      : S + 'deleted_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'deletedAt'
    },
    'archived_at': {
        'name'      : S + 'archived_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'archivedAt'
    },
    'is_archived': {
        'name'      : S + 'is_archived',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'isArchived'
    },
    'external_services_id': {
        'name'      : S + 'external_services_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ExternalId'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_campaign_call_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'campaign_id': {
        'name'      : S + 'campaign_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CampaignId'
    },
    'mighty_call_campaign_id': {
        'name'      : S + 'mighty_call_campaign_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MightyCallCampaignId'
    },
    'customer_id': {
        'name'      : S + 'customer_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustomerId'
    },
    'status': {
        'name'      : S + 'status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Status'
    },
    'priority': {
        'name'      : S + 'priority',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Priority'
    },
    'timezone': {
        'name'      : S + 'timezone',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Timezone'
    },
    'is_exit': {
        'name'      : S + 'is_exit',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsExit'
    },
    'phone_number': {
        'name'      : S + 'phone_number',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'PhoneNumber'
    },
    'display_name': {
        'name'      : S + 'display_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'DisplayName'
    },
    'audit_created_at': {
        'name'      : S + 'audit_created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/CreatedAt'
    },
    'audit_created_by': {
        'name'      : S + 'audit_created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/CreatedBy'
    },
    'audit_created_by_name': {
        'name'      : S + 'audit_created_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/CreatedByName'
    },
    'audit_updated_at': {
        'name'      : S + 'audit_updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/UpdatedAt'
    },
    'audit_updated_at_mil': {
        'name'      : S + 'audit_updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/UpdatedAt'
    },
    'audit_updated_by': {
        'name'      : S + 'audit_updated_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/UpdatedBy'
    },
    'audit_updated_by_name': {
        'name'      : S + 'audit_updated_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/UpdatedByName'
    },
    'audit_deleted_at': {
        'name'      : S + 'audit_deleted_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/DeletedAt'
    },
    'audit_deleted_by': {
        'name'      : S + 'audit_deleted_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/DeletedBy'
    },
    'audit_deleted_by_name': {
        'name'      : S + 'audit_deleted_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Telephony/CampaignCallListItem/AuditInfo/DeletedByName'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_comments_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'created_by_id': {
        'name'      : S + 'created_by_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'CreatedById'
    },
    'created_by_name': {
        'name'      : S + 'created_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'CreatedByName'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'CreatedAt'
    },
    'text': {
        'name'      : S + 'text',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Text'
    },
    'comment_id': {
        'name'      : S + 'comment_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : '_id'
    },
    'comment_index': {
        'name'      : S + 'comment_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_works_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'work_compamy_name': {
        'name'      : S + 'work_company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'CompanyName',
    },
    'work_position': {
        'name'      : S + 'work_position',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Position',
    },
    'work_start_date': {
        'name'      : S + 'work_start_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'StartDate',
    },
    'work_end_date': {
        'name'      : S + 'work_end_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'EndDate',
    },
    'work_description': {
        'name'      : S + 'work_description',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Description',
    },
    'work_industries': {
        'name'      : S + 'work_industries',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Industries',
    },
    'work_city': {
        'name'      : S + 'work_city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'City',
    },
    'work_index': {
        'name'      : S + 'work_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_task_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/TaskTracker/Task/AuditInfo/UpdatedAt'
    },
    'task_defenition_id': {
        'name'      : S + 'task_defenition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'TaskDefinitionId',
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId',
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'header': {
        'name'      : S + 'header',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Header',
    },
    'description': {
        'name'      : S + 'description',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Description',
    },
    'checklist': {
        'name'      : S + 'checklist',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Checklist',
    },
    'role': {
        'name'      : S + 'role',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Role',
    },
    'current_assignee': {
        'name'      : S + 'current_assignee',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CurrentAssignee',
    },
    'current_assignee_lock_till': {
        'name'      : S + 'current_assignee_lock_till',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CurrentAssigneeLockTill',
    },
    'activation_time': {
        'name'      : S + 'activation_time',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ActivationTime',
    },
    'priority': {
        'name'      : S + 'priority',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Priority',
    },
    'current_status': {
        'name'      : S + 'current_status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Status',
    },
    'object_type': {
        'name'      : S + 'object_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/TaskTracker/Task/Object/Type'
    },
    'object_id': {
        'name'      : S + 'object_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/TaskTracker/Task/Object/_id'
    },
    'status_id': {
        'name'      : S + 'status_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Status',
    },
    'status_user_id': {
        'name'      : S + 'status_user_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'UserId',
    },
    'status_set_at': {
        'name'      : S + 'status_set_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'SetAt',
    },
    'status_set_at_mil': {
        'name'      : S + 'status_set_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'SetAt',
    },
    'status_index': {
        'name'      : S + 'status_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'object_index': {
        'name'      : S + 'object_index',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_OTHER,
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_educations_sber = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'education_organization': {
        'name'      : S + 'education_organization',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Organization',
    },
    'educations_s   pecialization': {
        'name'      : S + 'education_specialization',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Specialization',
    },
    'education_form': {
        'name'      : S + 'education_form',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'EducationForm',
    },
    'education_start_year': {
        'name'      : S + 'education_start_year',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'StartYear',
    },
    'education_end_year': {
        'name'      : S + 'education_end_year',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'EndYear',
    },
    'education_index': {
        'name'      : S + 'education_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

# Структура кандидатов
colsnames_videointerview = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Assessments/VideoInterview/AuditInfo/UpdatedAt'
    },
    'question_order': {
        'name'      : S + 'question_order',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'QuestionOrder',
    },
    'competence_name': {
        'name'      : S + 'competence_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'CompetenceName',
    },
    'value': {
        'name'      : S + 'value',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'Value',
    },
    'by_id': {
        'name'      : S + 'by_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'ById',
    },
    'by_name': {
        'name'      : S + 'by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'ByName',
    },
    'at': {
        'name'      : S + 'at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
        'path'      : 'At',
    },
    'index': {
        'name'      : S + 'index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_tags_sber = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'tag_id': {
        'name'      : S + 'tag_id',
        'chtype'    : 'String',
        'path'      : '_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_label': {
        'name'      : S + 'tag_label',
        'chtype'    : 'String',
        'path'      : 'Label',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_type': {
        'name'      : S + 'tag_type',
        'chtype'    : 'String',
        'path'      : 'Type',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_stage': {
        'name'      : S + 'tag_stage',
        'chtype'    : 'Int64',
        'path'      : 'Stage',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_trait': {
        'name'      : S + 'tag_trait',
        'chtype'    : 'Int64',
        'path'      : 'Trait',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_by_id': {
        'name'      : S + 'tag_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_by_name': {
        'name'      : S + 'tag_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_at': {
        'name'      : S + 'tag_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_type_label': {
        'name'      : S + 'tag_type_label',
        'chtype'    : 'String',
        'path'      : 'TypeLabel',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_index': {
        'name'      : S + 'tag_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_tags = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'updated_at_mil': {
        'name'      : S + 'updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'tag_id': {
        'name'      : S + 'tag_id',
        'chtype'    : 'String',
        'path'      : '_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_label': {
        'name'      : S + 'tag_label',
        'chtype'    : 'String',
        'path'      : 'Label',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_type': {
        'name'      : S + 'tag_type',
        'chtype'    : 'String',
        'path'      : 'Type',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_stage': {
        'name'      : S + 'tag_stage',
        'chtype'    : 'Int64',
        'path'      : 'Stage',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_trait': {
        'name'      : S + 'tag_trait',
        'chtype'    : 'Int64',
        'path'      : 'Trait',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_by_id': {
        'name'      : S + 'tag_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_by_name': {
        'name'      : S + 'tag_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_set_at': {
        'name'      : S + 'tag_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_type_label': {
        'name'      : S + 'tag_type_label',
        'chtype'    : 'String',
        'path'      : 'TypeLabel',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'tag_index': {
        'name'      : S + 'tag_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsmanes_notifications_sber = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'notification_comm_id': {
        'name'      : S + 'notification_comm_id',
        'chtype'    : 'String',
        'path'      : 'CommentaryId',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_notify_at': {
        'name'      : S + 'notification_notify_at',
        'chtype'    : 'DateTime',
        'path'      : 'NotifyAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_current_status_dependent': {
        'name'      : S + 'notification_current_status_dependent',
        'chtype'    : 'String',
        'path'      : 'CurrentStatusDependent',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_worked_out': {
        'name'      : S + 'notification_worked_out',
        'chtype'    : 'String',
        'path'      : 'WorkedOut',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_worked_out_id': {
        'name'      : S + 'notification_worked_out_id',
        'chtype'    : 'String',
        'path'      : 'WorkedOutById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_worked_out_name': {
        'name'      : S + 'notification_worked_out_name',
        'chtype'    : 'String',
        'path'      : 'WorkedOutByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_user_id': {
        'name'      : S + 'notification_user_id',
        'chtype'    : 'String',
        'path'      : 'UserId',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'notification_index': {
        'name'      : S + 'notification_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_applications_sber = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'sf_candidate_id': {
        'name'      : S + 'sf_candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SFCandidateId'
    },
    'skillaz_id': {
        'name'      : S + 'skillaz_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SkillazId'
    },
    'candidate_email': {
        'name'      : S + 'candidate_email',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateEmail'
    },
    'sf_job_requisition_id': {
        'name'      : S + 'sf_job_requisition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SFJobRequisitionId'
    },
    'is_evergreen': {
        'name'      : S + 'is_evergreen',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsEvergreen'
    },
    'is_standard': {
        'name'      : S + 'is_standard',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsStandard'
    },
    'transferred_from': {
        'name'      : S + 'transferred_from',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'TransferredFrom'
    },
    'salary': {
        'name'      : S + 'salary',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Salary'
    },
    'cust_probation': {
        'name'      : S + 'cust_probation',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustProbation'
    },
    'cust_start_date': {
        'name'      : S + 'cust_start_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustStartDate'
    },
    'cust_trainee_status': {
        'name'      : S + 'cust_trainee_status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustTraineeStatus'
    },
    'cust_work_schedule': {
        'name'      : S + 'cust_work_schedule',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CustWorkSchedule'
    },
    'security_add_info_request': {
        'name'      : S + 'security_add_info_request',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SecurityAdditionalInfoRequest'
    },
    'security_check_result': {
        'name'      : S + 'security_check_result',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SecurityCheckResult'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/CreatedAt'
    },
    'created_by': {
        'name'      : S + 'created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/CreatedBy'
    },
    'created_by_name': {
        'name'      : S + 'created_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/CreatedByName'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/UpdatedAt'
    },
    'updated_by': {
        'name'      : S + 'updated_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/UpdatedBy'
    },
    'updated_by_name': {
        'name'      : S + 'updated_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/UpdatedByName'
    },
    'deleted_at': {
        'name'      : S + 'deleted_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/DeletedAt'
    },
    'deleted_by': {
        'name'      : S + 'deleted_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/DeletedBy'
    },
    'deleted_by_name': {
        'name'      : S + 'deleted_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/AuditInfo/DeletedByName'
    },
    'current_status_sf_label': {
        'name'      : S + 'current_status_sf_label',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/CurrentSFStatus/Label'
    },
    'current_status_sf_set_at': {
        'name'      : S + 'current_status_sf_set_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/CurrentSFStatus/SetAt'
    },
    'current_status_set_at': {
        'name'      : S + 'current_status_set_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/CurrentStatus/SetAt'
    },
    'current_status_value': {
        'name'      : S + 'current_status_value',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/CurrentStatus/Value'
    },
    'interview_at': {
        'name'      : S + 'interview_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/Interview/At'
    },
    'interview_is_finished': {
        'name'      : S + 'interview_is_finished',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SFJobApplicationDocument/Interview/IsFinished'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_statuses_sber = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'status_id': {
        'name'      : S + 'status_id',
        'chtype'    : 'String',
        'path'      : '_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_name': {
        'name'      : S + 'status_name',
        'chtype'    : 'String',
        'path'      : 'Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_weight': {
        'name'      : S + 'status_weight',
        'chtype'    : 'Int64',
        'path'      : 'Weight',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_is_hot': {
        'name'      : S + 'status_is_hot',
        'chtype'    : 'String',
        'path'      : 'IsHot',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_is_readonly': {
        'name'      : S + 'status_is_readonly',
        'chtype'    : 'String',
        'path'      : 'IsReadonly',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_type': {
        'name'      : S + 'status_type',
        'chtype'    : 'String',
        'path'      : 'Type',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at': {
        'name'      : S + 'status_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at_mil':{
        'name'      : S + 'status_set_at_mil',
        'chtype'    : 'Int64',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_id': {
        'name'      : S + 'status_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_name': {
        'name'      : S + 'status_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_comment': {
        'name'      : S + 'status_comment',
        'chtype'    : 'String',
        'path'      : 'Comment',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_id': {
        'name'      : S + 'status_details_id',
        'chtype'    : 'String',
        'path'      : 'Details/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_name': {
        'name'      : S + 'status_details_name',
        'chtype'    : 'String',
        'path'      : 'Details/Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_id': {
        'name'      : S + 'status_stage_id',
        'chtype'    : 'String',
        'path'      : 'Stage/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_label': {
        'name'      : S + 'status_stage_label',
        'chtype'    : 'String',
        'path'      : 'Stage/Label',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_order': {
        'name'      : S + 'status_stage_order',
        'chtype'    : 'Int64',
        'path'      : 'Stage/Order',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_funnel_id': {
        'name'      : S + 'status_funnel_id',
        'chtype'    : 'String',
        'path'      : 'FunnelId',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_index': {
        'name'      : S + 'status_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_candidates_sber = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'email': {
        'name'      : S + 'email',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ContactEmail',
    },
    'phone_number': {
        'name'      : S + 'phone_number',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ContactPhoneNumber',
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'first_name': {
        'name'      : S + 'first_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'FirstName'
    },
    'middle_name': {
        'name'      : S + 'middle_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MiddleName'
    },
    'last_name': {
        'name'      : S + 'last_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LastName'
    },
    'fio': {
        'name'      : S + 'fio',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Fio'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedAt'
    },
    'created_by': {
        'name'      : S + 'created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedBy'
    },
    'created_by_name': {
        'name'      : S + 'created_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedByName'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'updated_at_mil': {
        'name'      : S + 'updated_at_mil',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt'
    },
    'updated_by': {
        'name'      : S + 'updated_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedBy'
    },
    'updated_by_name': {
        'name'      : S + 'updated_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedByName'
    },
    'deleted_at': {
        'name'      : S + 'deleted_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/DeletedAt'
    },
    'deleted_by': {
        'name'      : S + 'deleted_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/DeletedBy'
    },
    'deleted_by_name': {
        'name'      : S + 'deleted_by_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/DeletedByName'
    },
    'first_source_add_way': {
        'name'      : S + 'first_source_add_way',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/cadidates/Candidates/FirstSource/AddWay',
    },
    'first_source_search_id': {
        'name'      : S + 'first_source_search_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/cadidates/Candidates/FirstSource/SearchId',
    },
    'first_source_sf_source': {
        'name'      : S + 'first_source_sf_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/SfSource',
    },
    'first_source_source': {
        'name'      : S + 'first_source_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/Source',
    },
    'first_source_source_url': {
        'name'      : S + 'first_source_source_url',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/SourceUrl',
    },
    'first_source_id': {
        'name'      : S + 'first_source_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/_id',
    },
    'first_source_call_type': {
        'name'      : S + 'first_source_call_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/CallType',
    },
    'first_source_external_source_url': {
        'name'      : S + 'first_source_external_source_url',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/ExternalSourceUrl',
    },
    'first_source_utm_source' : {
        'name'      : S + 'first_source_utm_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmSource',
    },
    'first_source_utm_campaign' : {
        'name'      : S + 'first_source_utm_campaign',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmCampaign',
    },
    'about': {
        'name'      : S + 'about',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/About',
    },
    'area': {
        'name'      : S + 'area',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Area',
    },
    'avatar': {
        'name'      : S + 'avatar',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Avatar',
    },
    'average_experience': {
        'name'      : S + 'average_experience',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/AverageExperience',
    },
    'birth_date': {
        'name'      : S + 'birth_date',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/BirthDate',
    },
    'citizenship': {
        'name'      : S + 'citizenship',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Citizenship',
    },
    'current_company': {
        'name'      : S + 'current_company',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentCompany',
    },
    'current_title': {
        'name'      : S + 'current_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentTitle',
    },
    'education': {
        'name'      : S + 'education',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Education',
    },
    'education_type': {
        'name'      : S + 'education_type',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/EducationType',
    },
    # 'educations': {
    #     'name'      : S + 'educations',
    #     'chtype'    : '''Nested (
    #                             index Int64,
    #                             organization String,
    #                             specialization String,
    #                             education_form Int64,
    #                             start_year Int64,
    #                             end_year Int64,
    #                             degree String
    #                             )''',
    #     'nested_cols': {
    #                 'index' : {
    #                     'path' : 'index',
    #                     'type' : 'Int64'
    #                 },
    #                 'organization' : {
    #                     'path' : 'Organization',
    #                     'type' : 'String'
    #                 },
    #                 'specialization' : {
    #                     'path' : 'Specialization',
    #                     'type' : 'String'
    #                 },
    #                 'education_form' : {
    #                     'path' : 'EducationForm',
    #                     'type' : 'Int64'
    #                 },
    #                 'start_year' : {
    #                     'path' : 'StartYear',
    #                     'type' : 'Int64'
    #                 },
    #                 'end_year' : {
    #                     'path' : 'EndYear',
    #                     'type' : 'Int64'
    #                 },
    #                 'degree'   : {
    #                     'path' : 'Degree',
    #                     'type' : 'String'
    #                 }
    #     },
    #     'coltype'   : COLTYPE_PREPARATION,
    #     'path'      : '/candidates/Candidates/CommonCVInfo/Educations'
    # },
    # 'works': {
    #     'name'      : S + 'works',
    #     'chtype'    : '''Nested (
    #                             index Int64,
    #                             company_name String,
    #                             position String,
    #                             start_date Date,
    #                             end_date Date,
    #                             description String,
    #                             industiries String,
    #                             city String
    #                             )''',
    #     'nested_cols': {
    #                 'index' : {
    #                     'path' : 'index',
    #                     'type' : 'Int64'
    #                 },
    #                 'company_name' : {
    #                     'path' : 'CompanyName',
    #                     'type' : 'String'
    #                 },
    #                 'position' : {
    #                     'path' : 'Position',
    #                     'type' : 'String'
    #                 },
    #                 'start_date' : {
    #                     'path' : 'StartDate',
    #                     'type' : 'Date'
    #                 },
    #                 'end_date' : {
    #                     'path' : 'EndDate',
    #                     'type' : 'Date'
    #                 },
    #                 'description' : {
    #                     'path' : 'Description',
    #                     'type' : 'String'
    #                 },
    #                 'industiries'   : {
    #                     'path' : 'Industries',
    #                     'type' : 'String'
    #                 },
    #                 'city'   : {
    #                     'path' : 'City',
    #                     'type' : 'String'
    #                 }
    #     },
    #     'coltype'   : COLTYPE_PREPARATION,
    #     'path'      : '/candidates/Candidates/CommonCVInfo/WorkExperience'
    # },
    'gender': {
        'name'      : S + 'gender',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Gender',
    },
    'gender_type': {
        'name'      : S + 'gender_type',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/GenderType',
    },
    'metro': {
        'name'      : S + 'metro',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Metro',
    },
    'skills': {
        'name'      : S + 'skills',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Skills',
    },
    'total_experience': {
        'name'      : S + 'total_experience',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/TotalExperience',
    },
    'cv_updated_at': {
        'name'      : S + 'cv_updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/UpdatedAt',
    },
    'preferred_employments': {
        'name'      : S + 'preferred_employments',
        'chtype'    : 'Array(Int64)',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/PreferredEmployments',
    },
    'preferred_salary': {
        'name'      : S + 'preferred_salary',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/PreferredSalary',
    },
    'preferred_schedules': {
        'name'      : S + 'preferred_schedules',
        'chtype'    : 'Array(Int64)',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/PreferredSchedules',
    },
    'resume_title': {
        'name'      : S + 'resume_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/ResumeTitle',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_vacancies = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'name'
    },
    'description': {
        'name'      : S + 'description',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'description'
    },
    'is_archived': {
        'name'      : S + 'is_archived',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'isArchived'
    },
    'specialization_id': {
        'name'      : S + 'specialization_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SpecializationId'
    },
    'city': {
        'name'      : S + 'city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'city'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'companyId'
    },
    'client_id': {
        'name'      : S + 'client_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClientId'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/UpdatedAt'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/vacancies/AuditInfo/CreatedAt'
    },
    'external_services_id': {
        'name'      : S + 'external_services_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ExternalId'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_responses = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'phone_number': {
        'name'      : S + 'phone_number',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ContactPhoneNumber'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'company_name': {
        'name'      : S + 'company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedAt'
    },
    'salary_from': {
        'name'      : S + 'salary_from',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION
    },
    'salary_to': {
        'name'      : S + 'salary_to',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION
    },
    'requisition_id': {
        'name'      : S + 'requisition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/_id',
    },
    'city': {
        'name'      : S + 'city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'requisition_location': {
        'name'      : S + 'requisition_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/AddressFull',
    },
    'vacancy_location': {
        'name'      : S + 'vacancy_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'comm_channel': {
        'name'      : S + 'comm_channel',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'response': {
        'name'      : S + 'response',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/AddWay'
    },
    'interview_appeared': {
        'name'      : S + 'interview_appeared',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

colsnames_client = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Name'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/UserTeam/AuditInfo/UpdatedAt'

    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER
    }
}

colsnames_user_team = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Name'
    },
    'leader_id': {
        'name'      : S + 'leader_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LeaderId'
    },
    'member_ids': {
        'name'      : S + 'member_ids',
        'chtype'    : 'Array(String)',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MemberIds'
    },
    'default_invitation_capacity': {
        'name'      : S + 'default_invitation_capacity',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'DefaultInvitationCapacity'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/UserTeam/AuditInfo/UpdatedAt'

    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER
    }
}

colsnames_users = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'user_name': {
        'name'      : S + 'user_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'UserName'
    },
    'last_name': {
        'name'      : S + 'last_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LastName'
    },
    'middle_name': {
        'name'      : S + 'middle_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MiddleName'
    },
    'fio': {
        'name'      : S + 'fio',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Fio'
    },
    'email': {
        'name'      : S + 'email',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Email'
    },
    'position': {
        'name'      : S + 'position',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Position'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/Auth/User/AuditInfo/UpdatedAt'

    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER
    }
}

colsnames_questionnare = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'last_name': {
        'name'      : S + 'last_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/LastName'
    },
    'first_name': {
        'name'      : S + 'first_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/FirstName'
    },
    'middle_name': {
        'name'      : S + 'middle_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/MiddleName'
    },
    'other_names': {
        'name'      : S + 'other_names',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/OtherNames'
    },
    'date_of_birth': {
        'name'      : S + 'date_of_birth',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/DateOfBirthString'
    },
    'place_of_birth': {
        'name'      : S + 'place_of_birth',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/PlaceOfBirth'
    },
    'cust_citizenship': {
        'name'      : S + 'cust_citizenship',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustCity'
    },
    'gender': {
        'name'      : S + 'gender',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/Gender'
    },
    'email': {
        'name'      : S + 'email',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/Email'
    },
    'cell_phone': {
        'name'      : S + 'cell_phone',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CellPhone'
    },
    'cust_phone_fact': {
        'name'      : S + 'cust_phone_fact',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPhoneFact'
    },
    'cust_phone_reg': {
        'name'      : S + 'cust_phone_reg',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPhoneReg'
    },
    'address': {
        'name'      : S + 'address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/Address'
    },
    'cust_address_reg': {
        'name'      : S + 'cust_address_reg',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustAddressReg'
    },
    'cust_tin': {
        'name'      : S + 'cust_tin',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustTIN'
    },
    'cust_passport_date_string': {
        'name'      : S + 'cust_passport_date_string',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPassport/DateString'
    },
    'cust_passport_issued_by': {
        'name'      : S + 'cust_passport_issued_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPassport/IssuedBy'
    },
    'cust_passport_num': {
        'name'      : S + 'cust_passport_num',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPassport/Num'
    },
    'cust_passport_serial': {
        'name'      : S + 'cust_passport_serial',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustPassport/Serial'
    },
    'cust_int_passport': {
        'name'      : S + 'cust_int_passport',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustIntPassport'
    },
    'cust_military_duty': {
        'name'      : S + 'cust_military_duty',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustMilitaryDuty'
    },
    'cust_military_exempt_reason': {
        'name'      : S + 'cust_military_exempt_reason',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustMilitaryExemptReason'
    },
    'cust_criminal_liability': {
        'name'      : S + 'cust_criminal_liability',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustCriminalLiability'
    },
    'cust_marital_status': {
        'name'      : S + 'cust_marital_status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustMaritalStatus'
    },
    'cust_relative_sb': {
        'name'      : S + 'cust_relative_sb',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustRelativeSB'
    },
    'cust_relative_sb_instruction': {
        'name'      : S + 'cust_relative_sb_instruction',
        'chtype'    : '''Nested (
                                fio String,
                                date_of_birth String,
                                type String,
                                work_partition String,
                                work_place String,
                                work_type String
                                )''',
        'nested_cols': {
                    'fio' : {
                        'path' : 'FIO',
                        'type' : 'String'
                    },
                    'date_of_birth' : {
                        'path' : 'dateOfBirth',
                        'type' : 'String'
                    },
                    'type' : {
                        'path' : 'type',
                        'type' : 'String'
                    },
                    'work_partition' : {
                        'path' : 'workPartition',
                        'type' : 'String'
                    },
                    'work_place' : {
                        'path' : 'workPlace',
                        'type' : 'String'
                    },
                    'work_type' : {
                        'path' : 'workType',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustRelativeSBInstruction'
    },
    'cust_relative_info': {
        'name'      : S + 'cust_relative_info',
        'chtype'    : '''Nested (
                                fio String,
                                date_of_birth String,
                                home_address String,
                                place_of_birth String,
                                type String,
                                work_place String,
                                work_type String
                                )''',
        'nested_cols': {
                    'fio' : {
                        'path' : 'FIO',
                        'type' : 'String'
                    },
                    'date_of_birth' : {
                        'path' : 'dateOfBirth',
                        'type' : 'String'
                    },
                    'home_address' : {
                        'path' : 'homeAddress',
                        'type' : 'String'
                    },
                    'place_of_birth' : {
                        'path' : 'PlaceOfBirth',
                        'type' : 'String'
                    },
                    'type' : {
                        'path' : 'type',
                        'type' : 'String'
                    },
                    'work_place' : {
                        'path' : 'workPlace',
                        'type' : 'String'
                    },
                    'work_type' : {
                        'path' : 'workType',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustRelativeInfo'
    },
    'education_type': {
        'name'      : S + 'education_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/EducationType'
    },
    'education': {
        'name'      : S + 'education',
        'chtype'    : '''Nested (
                                first_and_last_years String,
                                university_name_and_address String,
                                format String,
                                spec String,
                                diplom_series_and_num String
                                )''',
        'nested_cols': {
                    'first_and_last_years' : {
                        'path' : 'firstAndLastYears',
                        'type' : 'String'
                    },
                    'university_name_and_address' : {
                        'path' : 'universityNameAndAddress',
                        'type' : 'String'
                    },
                    'format' : {
                        'path' : 'format',
                        'type' : 'String'
                    },
                    'spec' : {
                        'path' : 'spec',
                        'type' : 'String'
                    },
                    'diplom_series_and_num' : {
                        'path' : 'diplomSeriesAndNum',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/Education'
    },
    'academic_degree': {
        'name'      : S + 'academic_degree',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/AcademicDegree'
    },
    'work_experience_comment': {
        'name'      : S + 'work_experience_comment',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/WorkExperienceComment'
    },
    'outside_work_experience': {
        'name'      : S + 'outside_work_experience',
        'chtype'    : '''Nested (
                                dates String,
                                work_info String,
                                position_name String,
                                responsibility String,
                                reason_of_termination String
                                )''',
        'nested_cols': {
                    'dates' : {
                        'path' : 'dates',
                        'type' : 'String'
                    },
                    'work_info' : {
                        'path' : 'workInfo',
                        'type' : 'String'
                    },
                    'position_name' : {
                        'path' : 'positionName',
                        'type' : 'String'
                    },
                    'responsibility' : {
                        'path' : 'responsibility',
                        'type' : 'String'
                    },
                    'reason_of_termination' : {
                        'path' : 'reasonOfTermination',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/Education'
    },
    'cust_combine_jobs': {
        'name'      : S + 'cust_combine_jobs',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustCombineJobs'
    },
    'cust_combine_jobs_instr': {
        'name'      : S + 'cust_combine_jobs_instr',
        'chtype'    : '''Nested (
                                org String,
                                relation_to_bank String,
                                actual_activity String,
                                responsibility String
                                )''',
        'nested_cols': {
                    'org' : {
                        'path' : 'org',
                        'type' : 'String'
                    },
                    'relation_to_bank' : {
                        'path' : 'relationToBank',
                        'type' : 'String'
                    },
                    'actual_activity' : {
                        'path' : 'actualActivity',
                        'type' : 'String'
                    },
                    'responsibility' : {
                        'path' : 'responsibility',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustCombineJobsInstr'
    },
    'cust_authorized_capital': {
        'name'      : S + 'cust_authorized_capital',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustAuthorizedCapital'
    },
    'cust_authorized_capital_instr': {
        'name'      : S + 'cust_authorized_capital_instr',
        'chtype'    : '''Nested (
                                org String,
                                relation_to_bank String,
                                portion String,
                                actual_activity String,
                                responsibility String
                                )''',
        'nested_cols': {
                    'org' : {
                        'path' : 'org',
                        'type' : 'String'
                    },
                    'relation_to_bank' : {
                        'path' : 'relationToBank',
                        'type' : 'String'
                    },
                    'portion' : {
                        'path' : 'portion',
                        'type' : 'String'
                    },
                    'actual_activity' : {
                        'path' : 'actualActivity',
                        'type' : 'String'
                    },
                    'responsibility' : {
                        'path' : 'responsibility',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustAuthorizedCapitalRelInstr'
    },
    'cust_authorized_capital_rel': {
        'name'      : S + 'cust_authorized_capital_rel',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustAuthorizedCapitalRel'
    },
    'cust_authorized_capital_rel_instr': {
        'name'      : S + 'cust_authorized_capital_rel_instr',
        'chtype'    : '''Nested (
                                relation_type String,
                                org String,
                                relation_to_bank String,
                                actual_activity String,
                                portion String,
                                responsibility String
                                )''',
        'nested_cols': {
                    'relation_type' : {
                        'path' : 'relationType',
                        'type' : 'String'
                    },
                    'org' : {
                        'path' : 'org',
                        'type' : 'String'
                    },
                    'relation_to_bank' : {
                        'path' : 'relationToBank',
                        'type' : 'String'
                    },
                    'actual_activity' : {
                        'path' : 'actualActivity',
                        'type' : 'String'
                    },
                    'portion' : {
                        'path' : 'portion',
                        'type' : 'String'
                    },
                    'responsibility' : {
                        'path' : 'responsibility',
                        'type' : 'String'
                    }
        },
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustAuthorizedCapitalRelInstr'
    },
    'cust_question_29_sb_partner_rel': {
        'name'      : S + 'cust_question_29_sb_partner_rel',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustQuestion29SBPartnerRel'
    },
    'cust_question_29_sb_partner_rel_comment': {
        'name'      : S + 'cust_question_29_sb_partner_rel_comment',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustQuestion29SBPartnerRelComment'
    },
    'cust_question_30_civil_service': {
        'name'      : S + 'cust_question_30_civil_service',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustQuestion30CivilService'
    },
    'cust_snils': {
        'name'      : S + 'cust_snils',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustSNILS'
    },
    'cust_sb_check': {
        'name'      : S + 'cust_sb_check',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/Questionnaire/CustSbCheck'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/sbersf/SfSecQuestionnaire/AuditInfo/UpdatedAt'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER
    }
}

colsnames_appointment = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'appointment_id': {
        'name'      : S + 'appointment_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'AppointmentId'
    },
    'status': {
        'name'      : S + 'status',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Status'
    },
    'type': {
        'name'      : S + 'type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Type'
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'address': {
        'name'      : S + 'address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Address'
    },
    'at': {
        'name'      : S + 'at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'At'
    },
    'set_by_id': {
        'name'      : S + 'set_by_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SetById'
    },
    'set_at': {
        'name'      : S + 'set_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'SetAt'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'client_id': {
        'name'      : S + 'client_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClientId'
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'team_id': {
        'name'      : S + 'team_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'TeamId'
    },
    'job_requisition_id': {
        'name'      : S + 'job_requisition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'JobRequisitionId'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    }
}

colsnames_comms = {
    'id_field': {
        'name'      : S + 'id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id'
    },
    'channel': {
        'name'      : S + 'channel',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'comm_id': {
        'name'      : S + 'comm_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'candidate_id': {
        'name'      : S + 'candidate_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidateId'
    },
    'datetime': {
        'name'      : S + 'datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId'
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'vacancy_location': {
        'name'      : S + 'vacancy_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'vacancy_department_id': {
        'name'      : S + 'vacancy_department_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'vacancy_department_name': {
        'name'      : S + 'vacancy_department_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId'
    },
    'company_name': {
        'name'      : S + 'company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION
    },
    'message_type': {
        'name'      : S + 'message_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MessageType'
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,

    }
}

# Структура кандидатов
# Структура кандидатов
colsnames_candidates = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'first_name': {
        'name'      : S + 'first_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'FirstName',
    },
    'middle_name': {
        'name'      : S + 'middle_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'MiddleName',
    },
    'last_name': {
        'name'      : S + 'last_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'LastName',
    },

    'client_id': {
        'name'      : '_client_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClientId',
    },
    'requisition_id': {
        'name'      : S + 'requisition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/_id',
    },
    'requisition_location_id': {
        'name'      : S + 'requisition_location_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/LocationId',
    },
    'requisition_binding_date': {
        'name'      : S + 'requisition_binding_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/BindingDate',
    },
    'first_source_call_type': {
        'name'      : S + 'first_source_call_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/CallType',
    },
    'first_source_name': {
        'name'      : S + 'first_source_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/Source',
    },
    'first_source_sf_sorce': {
        'name'      : S + 'first_source_sf_sorce',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/SfSource',
    },
    'first_source_utm_source' : {
        'name'      : S + 'first_source_utm_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmSource',
    },
    'first_source_utm_campaign' : {
        'name'      : S + 'first_source_utm_campaign',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmCampaign',
    },
    'add_way': {
        'name'      : S + 'add_way',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/cadidates/Candidates/FirstSource/AddWay',
    },
    'extra_data_page': {
        'name'      : S + 'extra_data_page',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/cadidates/Candidates/ExtraData/HHRecommendationsPage',
    },
    'created_by': {
        'name'      : S + 'created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedBy'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedAt'
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId',
    },
    'company_name': {
        'name'      : S + 'company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_location': {
        'name'      : S + 'vacancy_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_department_id': {
        'name'      : S + 'vacancy_department_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_department_name': {
        'name'      : S + 'vacancy_department_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'cv_updated': {
        'name'      : S + 'cv_updated',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/UpdatedAt',
    },
    'birth_date': {
        'name'      : S + 'birth_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/BirthDate',
    },
    'age': {
        'name'      : S + 'age',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'metro': {
        'name'      : S + 'metro',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Metro',
    },
    'education': {
        'name'      : S + 'education',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Education',
    },
    'education_specialization': {
        'name'      : S + 'education_specialization',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Educations/0/Specialization',
    },
    'education_end_year': {
        'name'      : S + 'education_end_year',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Educations/0/EndYear',
    },
    'experience_total': {
        'name'      : S + 'experience_total',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/TotalExperience',
    },
    'experience_average': {
        'name'      : S + 'experience_average',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/AverageExperience',
    },
    'current_company': {
        'name'      : S + 'current_company',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentCompany',
    },
    'current_title': {
        'name'      : S + 'current_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentTitle',
    },
    'area': {
        'name'      : S + 'area',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Area',
    },
    'citizenship_all': {
        'name'      : S + 'citizenship_all',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Citizenship',
    },
    'citizenship_rus': {
        'name'      : S + 'citizenship_rus',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'citizenship_count': {
        'name'      : S + 'citizenship_count',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Citizenship',
    },
    'group_interview_address': {
        'name'      : S + 'group_interview_address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/GroupInterview/Address',
    },
    'group_interview_datetime': {
        'name'      : S + 'group_interview_datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/GroupInterview/At',
    },
    'training_type': {
        'name'      : S + 'training_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/Type',
    },
    'training_address': {
        'name'      : S + 'training_address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/Address',
    },
    'training_datetime': {
        'name'      : S + 'training_datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/At',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'jobr_id': {
        'name'      : S + 'jobr_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },

    'status_id': {
        'name'      : S + 'status_id',
        'chtype'    : 'String',
        'path'      : '_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_name': {
        'name'      : S + 'status_name',
        'chtype'    : 'String',
        'path'      : 'Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_type': {
        'name'      : S + 'status_type',
        'chtype'    : 'String',
        'path'      : 'Type',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at': {
        'name'      : S + 'status_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at_mil': {
        'name'      : S + 'status_set_at_mil',
        'chtype'    : 'Int64',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_id': {
        'name'      : S + 'status_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_name': {
        'name'      : S + 'status_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_comment': {
        'name'      : S + 'status_comment',
        'chtype'    : 'String',
        'path'      : 'Comment',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_id': {
        'name'      : S + 'status_details_id',
        'chtype'    : 'String',
        'path'      : 'Details/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_name': {
        'name'      : S + 'status_details_name',
        'chtype'    : 'String',
        'path'      : 'Details/Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_id': {
        'name'      : S + 'status_stage_id',
        'chtype'    : 'String',
        'path'      : 'Stage/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_label': {
        'name'      : S + 'status_stage_label',
        'chtype'    : 'String',
        'path'      : 'Stage/Label',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_order': {
        'name'      : S + 'status_stage_order',
        'chtype'    : 'Int64',
        'path'      : 'Stage/Order',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_funnel_id': {
        'name'      : S + 'status_funnel_id',
        'chtype'    : 'String',
        'path'      : 'FunnelId',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_index': {
        'name'      : S + 'status_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },

    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

# Структура кандидатов
colsnames_candidates_OrgUnits = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId',
    },
    'client_id': {
        'name'      : S + 'client_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClientId',
    },
    'name': {
        'name'      : S + 'name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Name',
    },
    'contacts': {
        'name'      : S + 'contacts',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Contacts',
    },
    'city': {
        'name'      : S + 'city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/City',
    },
    'street': {
        'name'      : S + 'street',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Street',
    },
    'house': {
        'name'      : S + 'house',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/House',
    },
    'building': {
        'name'      : S + 'building',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Building',
    },
    'description': {
        'name'      : S + 'description',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Description',
    },
    'landmark': {
        'name'      : S + 'landmark',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Landmark',
    },
    'latitude': {
        'name'      : S + 'latitude',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Latitude',
    },
    'longitude': {
        'name'      : S + 'Longitude',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/OrgUnits/Address/Longitude',
    },
    'responsible_ids': {
        'name'      : S + 'responsible_ids',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ResponsibleIds',
    },
    'hire_needs': {
        'name'      : S + 'hire_needs',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'HireNeeds',
    },
    'is_archived': {
        'name'      : S + 'is_archived',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsArchived',
    },
    'employees_total': {
        'name'      : S + 'employees_total',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'EmployeesTotal',
    },
    'occupancy_rate_total': {
        'name'      : S + 'occupancy_rate_total',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'OccupancyRateTotal',
    },
    'open_positions_total': {
        'name'      : S + 'open_positions_total',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'OpenPositionsTotal',
    },
    'responsible_id': {
        'name'      : S + 'responsible_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ResponsibleId',
    },
    'staff_total': {
        'name'      : S + 'staff_total',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'StaffTotal',
    },
    'closing_date': {
        'name'      : S + 'closing_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClosingDate',
    },
    'text_search': {
        'name'      : S + 'text_search',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'TextSearch',
    },
    'geo_location': {
        'name'      : S + 'geo_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'GeoLocation',
    },
    'status_set_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'path'      : '/workspace/OrgUnits/AuditInfo/UpdatedAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

# Структура кандидатов
colsnames_other_candidates = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'client_id': {
        'name'      : '_client_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ClientId',
    },
    'requisition_id': {
        'name'      : S + 'requisition_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/_id',
    },
    'requisition_location_id': {
        'name'      : S + 'requisition_location_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/LocationId',
    },
    'requisition_binding_date': {
        'name'      : S + 'requisition_binding_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/JobRequisition/BindingDate',
    },
    'first_source_call_type': {
        'name'      : S + 'first_source_call_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/CallType',
    },
    'first_source_name': {
        'name'      : S + 'first_source_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/Source',
    },
    'first_source_sf_sorce': {
        'name'      : S + 'first_source_sf_sorce',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/SfSource',
    },
    'first_source_utm_source': {
        'name'      : S + 'first_source_utm_source',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmSource',
    },
    'first_source_utm_campaign': {
        'name'      : S + 'first_source_utm_campaign',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/FirstSource/UtmCampaign',
    },    
    'add_way': {
        'name'      : S + 'add_way',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/cadidates/Candidates/FirstSource/AddWay',
    },
    'created_by': {
        'name'      : S + 'created_by',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedBy'
    },
    'created_at': {
        'name'      : S + 'created_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/CreatedAt'
    },
    'updated_at': {
        'name'      : S + 'updated_at',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/AuditInfo/UpdatedAt',
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId',
    },
    'company_name': {
        'name'      : S + 'company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_location': {
        'name'      : S + 'vacancy_location',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_department_id': {
        'name'      : S + 'vacancy_department_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'vacancy_department_name': {
        'name'      : S + 'vacancy_department_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'cv_updated': {
        'name'      : S + 'cv_updated',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/UpdatedAt',
    },
    'birth_date': {
        'name'      : S + 'birth_date',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/BirthDate',
    },
    'age': {
        'name'      : S + 'age',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'metro': {
        'name'      : S + 'metro',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Metro',
    },
    'education': {
        'name'      : S + 'education',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Education',
    },
    'education_specialization': {
        'name'      : S + 'education_specialization',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Educations/0/Specialization',
    },
    'education_end_year': {
        'name'      : S + 'education_end_year',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Educations/0/EndYear',
    },
    'experience_total': {
        'name'      : S + 'experience_total',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/TotalExperience',
    },
    'experience_average': {
        'name'      : S + 'experience_average',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/AverageExperience',
    },
    'current_company': {
        'name'      : S + 'current_company',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentCompany',
    },
    'current_title': {
        'name'      : S + 'current_title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/CurrentTitle',
    },
    'area': {
        'name'      : S + 'area',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Area',
    },
    'citizenship_all': {
        'name'      : S + 'citizenship_all',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Citizenship',
    },
    'citizenship_rus': {
        'name'      : S + 'citizenship_rus',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'citizenship_count': {
        'name'      : S + 'citizenship_count',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/CommonCVInfo/Citizenship',
    },
    'group_interview_address': {
        'name'      : S + 'group_interview_address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/GroupInterview/Address',
    },
    'group_interview_datetime': {
        'name'      : S + 'group_interview_datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/GroupInterview/At',
    },
    'training_type': {
        'name'      : S + 'training_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/Type',
    },
    'training_address': {
        'name'      : S + 'training_address',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/Address',
    },
    'training_datetime': {
        'name'      : S + 'training_datetime',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/candidates/Candidates/TrainingInfo/At',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'jobr_id': {
        'name'      : S + 'jobr_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },

    'status_id': {
        'name'      : S + 'status_id',
        'chtype'    : 'String',
        'path'      : '_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_name': {
        'name'      : S + 'status_name',
        'chtype'    : 'String',
        'path'      : 'Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_type': {
        'name'      : S + 'status_type',
        'chtype'    : 'String',
        'path'      : 'Type',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at': {
        'name'      : S + 'status_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at_mil':{
        'name'      : S + 'status_set_at_mil',
        'chtype'    : 'Int64',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_id': {
        'name'      : S + 'status_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_name': {
        'name'      : S + 'status_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_comment': {
        'name'      : S + 'status_comment',
        'chtype'    : 'String',
        'path'      : 'Comment',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_id': {
        'name'      : S + 'status_details_id',
        'chtype'    : 'String',
        'path'      : 'Details/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_details_name': {
        'name'      : S + 'status_details_name',
        'chtype'    : 'String',
        'path'      : 'Details/Name',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_id': {
        'name'      : S + 'status_stage_id',
        'chtype'    : 'String',
        'path'      : 'Stage/_id',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_label': {
        'name'      : S + 'status_stage_label',
        'chtype'    : 'String',
        'path'      : 'Stage/Label',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_stage_order': {
        'name'      : S + 'status_stage_order',
        'chtype'    : 'Int64',
        'path'      : 'Stage/Order',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_funnel_id': {
        'name'      : S + 'status_funnel_id',
        'chtype'    : 'String',
        'path'      : 'FunnelId',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_index': {
        'name'      : S + 'status_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },
    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}

# Структура заявок
colsnames_jobr = {
    'id_field': {
        'name'      : '_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': '_id',
    },
    'created_on': {
        'name'      : S + 'created_on',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CreatedOn',
    },
    'responsible_manager_id': {
        'name'      : S + 'responsible_manager_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ResponsibleManagerId',
    },
    'responsible_manager_name': {
        'name'      : S + 'responsible_manager_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'company_id': {
        'name'      : S + 'company_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CompanyId',
    },
    'company_name': {
        'name'      : S + 'company_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'title': {
        'name'      : S + 'title',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Title',
    },
    'position': {
        'name'      : S + 'position',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Position',
    },
    'jobreq_work_start_on': {
        'name'      : S + 'jobreq_work_start_on',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'JobReqWorkStartOn',
    },
    'deadline': {
        'name'      : S + 'deadline',
        'chtype'    : 'DateTime',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'JobStartOn',
    },
    'vacancy_id': {
        'name'      : S + 'vacancy_id',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'VacancyId',
    },
    'vacancy_name': {
        'name'      : S + 'vacancy_name',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'adress_city': {
        'name'      : S + 'adress_city',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'City',
    },
    'adress_full': {
        'name'      : S + 'adress_full',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'path'      : '/workspace/JobRequisitions/Location/AddressFull',
    },
    'is_urgent_tf': {
        'name'      : S + 'is_urgent_tf',
        'chtype'    : 'Int8',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'IsUrgent',
    },
    'description': {
        'name'      : S + 'description',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Description',
    },
    'contract_type': {
        'name'      : S + 'contract_type',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'ContractType',
    },
    'schedule': {
        'name'      : S + 'schedule',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'Schedule',
    },
    'education_format': {
        'name'      : S + 'education_format',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'EducationFormat',
    },
    'education_period': {
        'name'      : S + 'education_period',
        'chtype'    : 'String',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'EducationPeriodInDays',
    },
    'candidates_to_hire': {
        'name'      : S + 'candidates_to_hire',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidatesToHire',
    },
    'candidates_coefficient': {
        'name'      : S + 'candidates_coefficient',
        'chtype'    : 'Float64',
        'coltype'   : COLTYPE_PREPARATION,
    },
    'candidates_limit': {
        'name'      : S + 'candidates_limit',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
        'mongo_name': 'CandidatesLimit',
    },
    'n_statuse_change': {
        'name'      : S + 'n_statuse_change',
        'chtype'    : 'Int64',
        'coltype'   : COLTYPE_PREPARATION,
    },

    'status_name': {
        'name'      : S + 'status_name',
        'chtype'    : 'String',
        'path'      : 'Status',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_at': {
        'name'      : S + 'status_set_at',
        'chtype'    : 'DateTime',
        'path'      : 'SetAt',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_id': {
        'name'      : S + 'status_set_by_id',
        'chtype'    : 'String',
        'path'      : 'SetById',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_set_by_name': {
        'name'      : S + 'status_set_by_name',
        'chtype'    : 'String',
        'path'      : 'SetByName',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': COLSUBTYPE_FLATPATH,
    },
    'status_index': {
        'name'      : S + 'status_index',
        'chtype'    : 'Int64',
        'path'      : '',
        'coltype'   : COLTYPE_FLAT,
        'colsubtype': '',
    },

    'date_for_clickhouse': {
        'name'      : S + 'date_for_clickhouse',
        'chtype'    : 'Date',
        'coltype'   : COLTYPE_OTHER,
    },
}
