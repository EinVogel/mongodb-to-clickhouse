import numpy as np
import pymongo as pm
import dpath.util
import time
from datetime import datetime, date, timedelta
import pdb
import math

import libs.my_clickhouse as clh
import libs.data_parser as dp
import libs.structure as stc
import data_preparation.data_OrgUnits as Org
import data_preparation.data_videointerview as video
import data_preparation.data_jobr as jobr
import data_preparation.data_communications as comm
import data_preparation.data_appointment as app
import data_preparation.data_users as usr
import data_preparation.data_user_team as team
import data_preparation.data_client as cli
import data_preparation.data_response as res
import data_preparation.data_vacancies as vac
import data_preparation.data_sber_candidates as sber_cand
import data_preparation.data_sber_statuses as sber_stat
import data_preparation.data_sber_applications as sber_apps
import data_preparation.data_sber_notifications as sber_notif
import data_preparation.data_sber_tags as sber_tags
import data_preparation.data_other_tags as other_tags
import data_preparation.data_sber_educations as sber_eds
import data_preparation.data_videointerview as video
import data_preparation.data_sber_works as sber_wrk
import data_preparation.data_sber_comments as sber_coms
import data_preparation.data_sber_vacancies as sber_vac
import data_preparation.data_sber_sources as source
import data_preparation.data_sber_vacancy_sources as sber_vac_sources
import data_preparation.data_sber_questionare as quiz
import data_preparation.data_sber_video as vid
import data_preparation.data_questionnaire as qst
import data_preparation.data_sber_jobr as sber_jobr
import data_preparation.data_sber_candidates_request as sber_req
import data_preparation.data_sber_audio as aud
import data_preparation.data_sber_scenarios as scn
import data_preparation.data_ird_stat as ird_stat
import data_preparation.data_ird_results as ird_res
import data_preparation.data_sber_task as task
import data_preparation.data_sber_campaign_call as ccil
import data_preparation.data_sber_simulation as sim
import data_preparation.data_candidates as cand
import data_preparation.data_OrgUnits as org
import libs.queries as myq


# Создание таблицы в clickhouse, с параметрами указанными в coldict, если ее там нет
# Удаление таблиц в ClickHouse
def drop_table(params):
    state = clh.drop_table(
        params['clickhouse_url'],
        params['full_tbl_name']
        )
    return state


# Создание базы данных, с именем указаными в db_name
def create_db(params):
    print('- Создаем базу данных, если её нет')
    # Выбор первого части строки до точки. Например data.base, выберем data
    db_name = params['full_tbl_name'].split('.')[0]
    state = clh.create_db(params['clickhouse_url'], db_name)
    if not state:
        print(' - Не удалось создать БД')
        return state


# Создание таблиц данных
def create_table(params):
    print('- Создаем таблицу в clickhouse, если её там нет')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        c_query = clh.cols_to_create_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field']
        )
    elif params['scope'] == 'sberbank':
        c_query = clh.cols_to_create_replacing_tree_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field'],
            params['primary_keys']
        )
    state = clh.create_table(params['clickhouse_url'], c_query)
    if not state:
        print(' - Не удалось связаться с clickhouse')
        return state


# Запрос всех компаний из Mongo коллекции
def get_all_companies(params):
    companies = dp.Path.get_companies(params['conn'], params['path'])
    return companies


# Проверка на наличия таблицы в ClickHouse
def table_exists(params):
    print('- Проверяем есть ли в КХ такая таблица')
    response = clh.exsists_table(params['clickhouse_url'], params['full_tbl_name'])
    return int(response)


# Проверка соединения с сервером
def check_for_connection(params):
    print('- Проверяем есть ли соединение с КХ')
    # Вызывается функция connection_exists из файла my_clickhouse
    response = clh.connection_exists(params['clickhouse_url'])
    return response


# Огрничения количества одной выгрузки
# размеры этой выборки задаються в batch_size
def batching_size(params, count):
    return math.ceil(count/params['batch_size'])


# Блок формирования запросов create_ в MongoDb
# query - Запрос отправляемый на сервер Mongo
# text - дата последнего обновления
# для запросов MongoDB
def create_mongodb_query(params, min_date):
    query, text = myq.candidates_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# для статусов MongoDB
def create_statuses_mongodb_query(params, min_date, type):
    query, text = myq.sber_statuses_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope'],
        type
    )
    return query, text


# для Quiz Interview
def create_quiz_query(params, min_date):
    query, text = myq.sber_quiz_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# на 'видео'
def create_video_query(params, min_date):
    query, text = myq.sber_video_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


#  на 'аудио'
def create_audio_query(params, min_date):
    query, text = myq.sber_audio_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# для 'вакансии'
def create_vacancy_query(params, min_date):
    if params['scope'] == 'megafon':
        query, text = myq.vacancies_query(
            params['path_to_audit_updated_at'],
            params['clickhouse_url'],
            params['full_tbl_name'],
            params['status_set_at_field']
        )
    elif params['scope'] == 'sberbank' :
        query, text = myq.sber_vacancies_query(
            params['path_to_audit_updated_at'],
            params['clickhouse_url'],
            params['full_tbl_name'],
            params['status_set_at_field'],
            min_date,
            params['companies']
        )
    return query, text


# для 'коммуникаций'
def create_comm_query(params, min_date, commtype):
    query, restrictions, text = myq.communications_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        commtype
    )
    return query, restrictions, text


def create_query(params, min_date):
    query, text = myq.basic_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        params['scope'],
        params['companies'],
        min_date
    )
    return query, text


def create_campaign_call_query(params, min_date):
    query, text = myq.campaign_call_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        params['scope'],
        params['companies'],
        min_date
    )
    return query, text


# для Анкет
def create_quest_query(params, min_date):
    query, text = myq.questionnaire_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        params['scope'],
        params['companies'],
        min_date
    )
    return query, text


def create_jobr_sber_query(params, min_date):
    query, text = myq.jobr_sber_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        params['scope'],
        params['companies'],
        min_date
    )
    return query, text


# для Applications
def create_app_query(params, min_date):
    query, text = myq.sber_applications_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date
    )
    return query, text


# для 'нотификаций'
def create_notif_query(params, min_date):
    query, text = myq.sber_notifications_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'теги'
def create_tags_query(params, min_date):
    query, text = myq.sber_tags_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


def create_other_tags_query(params, min_date):
    query, text = myq.other_tags_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'Educations'
def create_educations_query(params, min_date):
    query, text = myq.sber_educations_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'Videointerview'
def create_videointerview_query(params, min_date):
    query, text = myq.videointerview_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'Works'
def create_works_query(params, min_date):
    query, text = myq.sber_works_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'Comment'
def create_comments_query(params, min_date):
    query, text = myq.sber_comments_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'привязанные источники вакансий'
def create_vacancy_sources_query(params, min_date):
    query, text = myq.sber_vacancy_sources_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies']
    )
    return query, text


# для 'мероприятия'
def create_appointment_query(params, min_date):
    query, text = myq.appointment_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# Выделяет SMS в MongoDb
def select_sms_mongodb(params, query, restictions, query_text):
    print('- Производится выгрузка SMS c ' + query_text)
    cursor = dp.Path.comm_to_cursor(
        params['conn'],
        params['sms_path'],
        query,
        restictions
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Подсчёт SMS в MongoDb
def count_sms_mongodb(params, query):
    print(' - Производится подсчет SMS')
    cursor = dp.Path.cursor_count(
        params['conn'],
        params['sms_path'],
        query
        )
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Cursor Email в MongoDb
def select_email_mongodb(params, query, restictions, query_text):
    print('- Производится выгрузка Email\'ов c ' + query_text)
    cursor = dp.Path.comm_to_cursor(
        params['conn'],
        params['email_path'],
        query,
        restictions
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Подсчёт Email в MongoDb
def count_email_mongodb(params, query):
    print(' - Производится подсчет Email\'ов')
    cursor = dp.Path.cursor_count(
        params['conn'],
        params['email_path'],
        query
        )
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Cusor звонки в MongoDb
def select_call_mongodb(params, query, restictions, query_text):
    print('- Производится выгрузка звонков c ' + query_text)
    cursor = dp.Path.comm_to_cursor(
        params['conn'],
        params['VATS_path'],
        query,
        restictions
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])

    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Подсчёт количетсва звонков в MongoDb
def count_call_mongodb(params, query):
    print(' - Производится подсчет звонков')
    cursor = dp.Path.cursor_count(
        params['conn'],
        params['VATS_path'],
        query
        )
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Поиск максимального числа записей в MongoDb
def max_comm_count(params, sms, email, call):
    count = max(sms, email, call)
    if count > params['batch_size']:
        print('- Максимальное число записей: %d записей'%(count))
        print('- ' + str(math.ceil(count/params['batch_size'])) + ' итераций, при размере батча %d записей' %(params['batch_size']))
    return count


#  Cursor mongodb по сформированному запросу query
def select_mongodb(params, query, query_text):
    print('- Производится выгрузка с ' + query_text)
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['path'],
        query
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Подсчёт количества записей
def count_mongodb(params, query, query_text):
    print('- Производится подсчет записей')
    cursor = dp.Path.cursor_count(
        params['conn'],
        params['path'],
        query
        )
    if cursor > params['batch_size']:
        print('- Общее число записей: %d записей c '%(cursor) + query_text)
        print('- ' + str(math.ceil(cursor/params['batch_size'])) + ' итераций, при размере батча %d записей' %(params['batch_size']))
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Cursor мероприятий в MongoDb
def select_appointment_mongodb(params, query):
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['appointment_path'],
        query
    ).sort([(params['id_field'], -1)])
    if params['limit'] >= 0: cursor = cursor.limit(params['limit'])
    return cursor


# Сursor кандидатов в MongoDB
def select_candidates_mongodb(params, query):
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['candidates_path'],
        query
    ).sort([(params['id_field'], -1)])
    if params['limit'] >= 0: cursor = cursor.limit(params['limit'])
    return cursor


# Приводим Cursor в pandas DataFrame
def cursor_to_df(cursor):
    data = dp.cursor_to_df(cursor)
    print('  Количество записей: ' + str(data.shape[0]))
    return data


# Блок preparation
# Принимает на вход связь c MongoDB и DataFrame выгруженных коммуникаций.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
def cand_df_preparation(params, data):
    print('- Первичная обработка кандидатов с догрузкой из базы')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_candidates = cand.preparation(params['conn'], data, params['coldict'], params['scope'])
        print('- Добавление столбца с ID Заявки в кандидатов')
    elif params['scope'] == 'sberbank':
        dataset_candidates = sber_cand.preparation(params['conn'], data, params['coldict'])
    return dataset_candidates


def org_df_preparation(params, data):
    print('- Первичная обработка кандидатов с догрузкой из базы')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_candidates = org.preparation(params['conn'], data, params['coldict'], params['scope'])
        print('- Добавление столбца с ID Заявки в кандидатов')
    elif params['scope'] == 'sberbank':
        dataset_candidates = sber_cand.preparation(params['conn'], data, params['coldict'])
    return dataset_candidates


# Блок preparation
# Принимает на вход связь c MongoDB и DataFrame выгруженных коммуникаций.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
def videointerview_preparation(params, data):
    print('- Первичная обработка videointerviews с догрузкой из базы')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_candidates = video.preparation(params['conn'], data, params['coldict'], params['scope'])
        print('- Добавление столбца с ID Заявки в кандидатов')
    elif params['scope'] == 'sberbank':
        dataset_candidates = sber_cand.preparation(params['conn'], data, params['coldict'])
    return dataset_candidates


def stat_df_preparation(params, data):
    print('- Первичная обработка статусов с догрузкой из базы')
    dataset_candidates = sber_stat.preparation(params['conn'], data, params['coldict'])
    return dataset_candidates


def apps_df_preparation(params, data):
    print('- Первичная обработка Applications с догрузкой из базы')
    dataset_applications = sber_apps.preparation(params['conn'], data, params['coldict'])
    return dataset_applications


def notif_df_preparation(params, data):
    print('- Первичная обработка нотификаций с догрузкой из базы')
    dataset_notifications = sber_notif.preparation(params['conn'], data, params['coldict'])
    return dataset_notifications


def tags_df_preparation(params, data):
    print('- Первичная обработка тегов с догрузкой из базы')
    dataset_tags = sber_tags.preparation(params['conn'], data, params['coldict'])
    return dataset_tags


def other_tags_df_preparation(params, data):
    print('- Первичная обработка тегов с догрузкой из базы')
    dataset_tags = other_tags.preparation(params['conn'], data, params['coldict'])
    return dataset_tags


def eds_df_preparation(params, data):
    print('- Первичная обработка Educations с догрузкой из базы')
    dataset_educations = sber_eds.preparation(params['conn'], data, params['coldict'])
    return dataset_educations


def video_preparation(params, data):
    print('- Первичная обработка videointerviews с догрузкой из базы')
    dataset_videointerview = video.preparation(params['conn'], data, params['coldict'])
    return dataset_videointerview


def works_df_preparation(params, data):
    print('- Первичная обработка Works с догрузкой из базы')
    dataset_works = sber_wrk.preparation(params['conn'], data, params['coldict'])
    return dataset_works


def comments_df_preparation(params, data):
    print('- Первичная обработка Comments с догрузкой из базы')
    dataset_comments = sber_coms.preparation(params['conn'], data, params['coldict'])
    return dataset_comments


def vacancy_sources_df_preparation(params, data):
    print('- Первичная обработка Источников вакансий с догрузкой из базы')
    dataset_vacancies = sber_vac_sources.preparation(params['conn'], data, params['coldict'])
    return dataset_vacancies


def candidates_request_df_preparation(params, data):
    print('- Первичная обработка Candidates Request с догрузкой из базы')
    dataset_requests = sber_req.preparation(params['conn'], data, params['coldict'])
    return dataset_requests


def comm_df_preparation(params, data, commtype):
    print('- Первичная обработка ' + commtype + ' с догрузкой из базы')
    dataset_communications = comm.preparation(params['conn'], data, params['coldict'], commtype, params[commtype])
    return dataset_communications


def appointment_df_preparation(params, data):
    print('- Первичная обработка мероприятий с догрузкой из базы')
    dataset_appointments = app.preparation(params['conn'], data, params['coldict'])
    return dataset_appointments


def users_df_preparation(params, data):
    print('- Первичная обработка User\'ов')
    dataset_users = usr.preparation(params['conn'], data, params['coldict'])
    return dataset_users


def scenarios_df_preparation(params, data):
    print('- Первичная обработка Сценариев')
    dataset_scenarios = scn.preparation(params['conn'], data, params['coldict'])
    return dataset_scenarios


def questionnaire_df_preparation(params, data):
    print('- Первичная обработка Опросников')
    dataset_questionnaire = qst.preparation(params['conn'], data, params['coldict'])
    return dataset_questionnaire


def jobr_sber_df_preparation(params, data):
    print('- Первичная обработка Заявок')
    dataset_jobr = sber_jobr.preparation(params['conn'], data, params['coldict'])
    return dataset_jobr


def ird_stat_df_preparation(params, data):
    print('- Первичная обработка Ird Stat')
    dataset_stat = ird_stat.preparation(params['conn'], data, params['coldict'])
    return dataset_stat


def task_df_preparation(params, data):
    print('- Первичная обработка Task')
    dataset_stat = task.preparation(params['conn'], data, params['coldict'])
    return dataset_stat


def ird_results_df_preparation(params, data):
    print('- Первичная обработка Ird Results')
    dataset_results = ird_res.preparation(params['conn'], data, params['coldict'])
    return dataset_results


def user_team_df_preparation(params, data):
    print('- Первичная обработка User Team')
    dataset_user_team = team.preparation(params['conn'], data, params['coldict'])
    return dataset_user_team


def client_df_preparation(params, data):
    print('- Первичная обработка клиентов')
    dataset_client = cli.preparation(params['conn'], data, params['coldict'])
    return dataset_client


def response_df_preparation(params, data):
    print('- Первичная обработка клиентов')
    dataset_response = res.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def sources_df_preparation(params, data):
    print('- Первичная обработка источников')
    dataset_response = source.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def quiz_df_preparation(params, data):
    print('- Первичная обработка опросников')
    dataset_response = quiz.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def simulation_df_preparation(params, data):
    print('- Первичная обработка Simulation Interview')
    dataset_response = sim.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def video_df_preparation(params, data):
    print('- Первичная обработка видео опросов')
    dataset_response = vid.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def audio_df_preparation(params, data):
    print('- Первичная обработка видео опросов')
    dataset_response = aud.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def campaign_call_df_preparation(params, data):
    print('- Первичная обработка Campaign Call Item List')
    dataset_response = ccil.preparation(params['conn'], data, params['coldict'])
    return dataset_response


def vacancies_df_preparation(params, data):
    print('- Первичная обработка вакансий')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_vacancies = vac.preparation(params['conn'], data, params['coldict'])
    elif params['scope'] == 'sberbank':
        dataset_vacancies = sber_vac.preparation(params['conn'], data, params['coldict'])
    return dataset_vacancies


# Cursor заявки в MongoDB для связи с кандидатами
def jobr_cand_cursor(params):
    print('  Производим предварительную загрузку заявок, для создания связи кандидат-заявка.', end='\n')
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['jobr_path'],
        myq.all_jobr_query(params['companies'])
    ).sort([(params['id_field'], -1)])
    return cursor


# Берет на вход датафрейм из кандидатов и выдает датафрейм ID кандидатов и заявок
def cand_jobr_df_join(params, dataset_candidates):
    print('  Создание таблицы связи кандидат-заявка')
    dataset_candidates['_jobr_id'] = dataset_candidates['_requisition_id']
    return dataset_candidates


# Получает на вход массив из ID кандидатов, выдает таблицу с максимальной датой смены статуса по кандидату
def update_df(params, dataset_candidates, min_date):
    last_state_update_DF = clh.df_last_state_update(
        params['clickhouse_url'],
        dataset_candidates[params['id_field']],
        params['full_tbl_name'],
        params['id_field'],
        params['status_set_at_field'],
        params['last_state_update_field']
    )
    if last_state_update_DF.shape[0] == 0:
        last_state_update_DF[params['id_field']] = dataset_candidates[params['id_field']]
        last_state_update_DF[params['last_state_update_field']] = np.full(dataset_candidates.shape[0], datetime(2017, 1, 1, 0, 0, 0))
    dataset_candidates = dataset_candidates.join(last_state_update_DF.set_index(params['id_field']), on=params['id_field'])
    dataset_candidates[params['last_state_update_field']].fillna(datetime(2017, 1, 1, 0, 0, 0), inplace=True)

    return dataset_candidates


# Блок flat
# Принимает на вход DataFrame по кандидатам.
# Выдает новый DataFrame, где на каждый элемент вложенной структуры создается новая запись.
def df_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = cand.to_flat(
        dataset_candidates,
        params['coldict'],
        params['last_state_update_field'],
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def statuses_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_stat.to_flat(
        dataset_candidates,
        params['coldict'],
        params['last_state_update_field'],
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def notif_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_notif.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def quiz_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = quiz.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def video_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = vid.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def audio_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = aud.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def tags_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_tags.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def other_tags_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = other_tags.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def eds_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_eds.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def videointerview_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = video.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def works_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_wrk.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def task_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = task.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def comments_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_coms.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


def vacancy_sources_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = sber_vac_sources.to_flat(
        dataset_candidates,
        params['coldict']
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


# Добавляем столбец даты специально для clickhouse
def data_preprocess(params, dataset_candidates):
    dataset_candidates = clh.add_main_data_col(
        dataset_candidates,
        params['status_set_at_field'],
        params['date_field']
    )
    return dataset_candidates


# Перегоняем данные в набор строк.
def df_to_str(params, dataset_candidates):
    i_query = clh.data_to_insert_query(
        dataset_candidates[[x['name'] for x in params['coldict'].values()]],
        params['coldict'],
        params['full_tbl_name']
    )
    return i_query


# Выгрузка данных в clickhouse
def data_to_clickhouse(params, i_query, start_time_total):
    print('- Выгружаем данные в clickhouse')
    state, text = clh.insert(params['clickhouse_url'], i_query, params['show_queries'])
    if not state:
        print(' - Не удалось добавить данные в clickhouse')
        print(text)
    # Выводим результат по времени.
    estimation = str("%.4f" % ((time.time() - start_time_total)/60))
    limit = params['limit']
    print(f'{estimation} минут - весь процесс при лимите {limit} записей')
    return state


# query - Запрос отправляемый на сервер Mongo
# text - дата последнего обновления
# для запросов  заявок
def create_jobr_mongodb_query(params, min_date):
    query, text = myq.jobr_query(
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# Выделяем заявки в MongoDB
def select_jobr_data(params, query, query_text):
    print('- Производится выгрузка с ' + query_text)
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['jobr_path'],
        query
    ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if params['limit'] >= 0: cursor = cursor.limit(params['limit'])
    return cursor


# Подсчёт заявок в MongoDB
def count_jobr_data(params, query, query_text):
    cursor = dp.Path.cursor_count(
        params['conn'],
        params['jobr_path'],
        query
    )
    if cursor > params['batch_size']:
        print('- Общее число записей: %d записей c '%(cursor) + query_text)
        print('- ' + str(math.ceil(cursor/params['batch_size'])) + ' итераций, при размере батча %d записей' %(params['batch_size']))
    if params['limit'] >= 0: cursor = cursor.limit(params['limit'])
    return cursor


# Принимает на вход связь c MongoDB и DataFrame выгруженных заявок.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
def jobr_data_preparation(params, data):
    print('- Первичная обработка заявок с догрузкой из базы')
    dataset_jobr = jobr.preparation(params['conn'], data, params['coldict'])
    return dataset_jobr


# Принимает на вход DataFrame по заявкам.
# Выдает новый DataFrame, где на каждый статус записи создается новая запись.
def jobr_data_to_flat(params, dataset_jobr):
    # Создаем новое представление данных, раскрывая каждую запись по новым статусам, которых нет в clickhouse
    print('- Раскладываем заявки в плоскую структуру, если есть новые статусы')
    dataset_jobr = jobr.to_flat(
        dataset_jobr,
        params['coldict'],
        params['last_state_update_field'],
    )
    print(' - Суммарное количество записей: ' + str(dataset_jobr.shape[0]))
    return dataset_jobr
