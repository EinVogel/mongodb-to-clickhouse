import datetime
from dateutil.parser import parse
import libs.my_clickhouse as clh
import pdb


# Тут все запросы к MongoDB
# Запрос для кандидатов
def candidates_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    # Берем последнюю дату из clickhouse. Если таблица пустая, берется значение из min_date
    time_to_query = min_date
    if scope == 'megafon' or scope == 'other':
        try:
            last_date = clh.take_max_value(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            time_to_query = last_date - datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    elif scope == 'sberbank':
        try:
            last_date, microseconds = clh.take_max_value_mil(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            microseconds = int(microseconds) * 1000
            last_date = last_date + datetime.timedelta(microseconds=microseconds)
            time_to_query = last_date
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки. Уже не нужен с связи с реализацией батичнга.
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Компании по которым выгружаются данные
            { 'CompanyId': { '$in': companies } },
        # Поле даты по которому выгружаются данные
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
            # {
            #     # Этот фильтр нужен что бы исключить ошибки в базе, при которых наша процедура падает.
            #     "CurrentStatus": {
            #         '$ne': None
            #     }
            #     # Для sberbank выгрузка реализована иначе можно опустить
            # } if scope in ('megafon', 'other') else {},
        ]
    }
    return query, text


def sber_statuses_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope, type):

    # Берем последнюю дату из clickhouse
    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date #.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)

    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
            {
                # Этот фильтр нужен что бы исключить ошибки в базе, при которых наша процедура падает.
                "CurrentStatus": {
                    '$ne': None
                }
            } if type == 'batch' else {},
        ]
    }
    return query, text

def sber_applications_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date):

    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            }
        ]
    }
    return query, text

def sber_notifications_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия Нотификаций у кандидата для исключения "пустых"
            {'$or': [{ '$where': 'this.NotificationHistory.length > 0'}, {'CurrentNotification': {'$ne': None}}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_quiz_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия результата опросов для исключения "пустых"
            { '$where': 'this.Results.length > 0'},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_video_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия результата опроса для исключения "пустых"

            { '$where': 'this.Results.length > 0'},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_audio_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия результата опроса для исключения "пустых"
            { '$where': 'this.Results.length > 0'},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_tags_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            # Условие присутствия тегов кандидата для исключения "пустых"
            { '$and': [{'$where': 'this.TagsHistory.length > 0'}, {'TagsHistory': {'$exists': True}}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def other_tags_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            # Условие присутствия тегов кандидата для исключения "пустых"
            { '$and': [{'$where': 'this.TagsHistory.length > 0'}, {'TagsHistory': {'$exists': True}}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_educations_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия образований у кандидата для исключения "пустых"
            { '$and': [{'CommonCVInfo.Educations': {'$ne': None}}, {'$where': 'this.CommonCVInfo.Educations.length > 0'}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text


def videointerview_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Условие присутствия  у кандидата для исключения "пустых"
            {'$and': [{'CompenenceAssessments': {'$ne': None}}, {'$where': 'this.CompenenceAssessments.length > 0'}]},
            {'CompanyId': {'$in': companies}},
            {
                mongodb_field: {
                    '$gt': time_to_query,  # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text


def sber_works_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            # Условие присутствия опыта работы кандидата для исключения "пустых"
            { '$and': [{'CommonCVInfo.WorkExperience': {'$ne': None}}, {'$where': 'this.CommonCVInfo.WorkExperience.length > 0'}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_comments_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):

    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            # Условие присутствия коментариев кандидата для исключения "пустых"
            { '$and': [{'Notes': {'$ne': None}}, {'$where': 'this.Notes.length > 0'}]},
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def sber_vacancy_sources_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):
    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
            # Условие присутствия источника вакансии для исключения "пустых"
            { 'LinkedNegotiationSources' : { '$exists' : True}},
            { '$where' :'this.LinkedNegotiationSources.length > 0'},
            { 'companyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

# Запрос для заявок
def jobr_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):
    # Берем последнюю дату из clickhouse и вычитаем 2 минуты на всякий случай

    time_to_query = min_date

    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date - datetime.timedelta(minutes=2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # Формируем запрос по фильтру
    query = {
        '$and':[
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gte': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                }
            },
            {
                # Этот фильтр нужен что бы исключить ошибки в базе, при которых наша процедура падает.
                "StateHistory": {
                    '$exists': True
                }
            },
        ]
    }
    if scope == 'other':
        del query['$and'][0]
    return query, text

# Запрос для коммуникаций
def communications_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, commtype):

    time_to_query = min_date

    try:
        last_date = clh.take_max_value_comm(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name,
            commtype=commtype
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
                { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    restrictions = {'Text' : 0, 'Html' : 0}
    return query, restrictions, text

def appointment_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    time_to_query = min_date

    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
            { 'CompanyId': { '$in': companies } },
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    if scope == 'other':
        del query['$and'][0]
    return query, text


def basic_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, scope, companies, min_date):

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    # Используется для нескольких датасетов sberbank
    if scope == 'sberbank':
        query['$and'].append({'CompanyId': {'$in': companies}})
    return query, text

def campaign_call_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, scope, companies, min_date):

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def questionnaire_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, scope, companies, min_date):

    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def jobr_sber_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, scope, companies, min_date):

    time_to_query = min_date
    try:
        last_date, microseconds = clh.take_max_value_mil(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        microseconds = int(microseconds) * 1000
        last_date = last_date + datetime.timedelta(microseconds=microseconds)
        time_to_query = last_date
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and':[
            {
                mongodb_field: {
                    '$gt': time_to_query,
                    # '$lte': max_date
                }
            },
        ]
    }
    return query, text

def vacancies_query(mongodb_field, clickhouse_url, full_tbl_name, col_name):
    text = 'Производится выгрузка вакансий за все время'
    query = {}
    return query, text

def sber_vacancies_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies):
    time_to_query = min_date
    try:
        last_date = clh.take_max_value(
            clickhouse_url=clickhouse_url,
            full_tbl_name=full_tbl_name,
            col_name=col_name
        )
        last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
        time_to_query = last_date + datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
        text = str(time_to_query)
    except:
        text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки
    # max_date = min_date + datetime.timedelta(1)
    query = {


                mongodb_field: {
                    '$gt': time_to_query, # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            ,
             'companyId': { '$in': companies } ,


    }
    return query, text

# Запрос для всех заявок для последующего джойна с кандидатами
def all_jobr_query(companies):
    query = { 'CompanyId': { '$in': companies } }
    return query
