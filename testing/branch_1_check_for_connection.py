import requests
from confs.utils import candidates_parameters_dict_OrgUnits


# Ветка 1.1 [my_clickhouse]
# Отправление запроса в базу данных и получения ответа
def _get(url, query):
    try:
        # получаем запрос с сайта
        r = requests.post(url=url, data=query, stream=False)
        # s - ответ с сервера
        s = r.text
        # статус 200 означает что все ОК, получена информация с сайта
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    if r.status_code == 200:
        print('- Соединение с базой данных Clickhouse получено')
    else:
        print('- Соединение с базой данных Clickhouse не установлено')
    return r, s


# Ветка 1.2 [my_clickhouse]
# Наличие соединения с сервером
def connection_exists(url):
    # Посылаем запрос в базу данных Clickhouse
    sub_query = 'SELECT 1'
    # Вызываем функцию _get и передаем туда запрос
    _, s = _get(url, sub_query)
    return s


# Ветка 1.3 [mongo_clickhouse]
# Проверка соединения с сервером
def check_for_connection(params):
    print('- Проверяем есть ли соединение с КХ')
    # Вызывается функция connection_exists из файла my_clickhouse
    response = connection_exists(params['clickhouse_url'])
    return response


check_for_connection(candidates_parameters_dict_OrgUnits)
