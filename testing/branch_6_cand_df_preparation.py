import pandas as pd
import libs.structure as stc
import libs.data_parser as dp
from datetime import datetime, date, timedelta
from confs.utils import candidates_parameters_dict_OrgUnits
from branch_5_select_mongoDB import data

# Ветка 7.1
# Словарь дополнительных источников
other_sours = {
    0: 'null',
    1: 'zarplata.ru',
    2: 'Отклик zarplata.ru',
    3: 'rabota.ru',
    4: 'Отклик rabota.ru',
    5: 'Видео-интервью',
    6: 'Прочие работные сайты',
    7: 'Отклик прочие работные сайты',
    8: 'СС VK',
    9: 'СС Facebook',
    10: 'СС OK',
    11: 'СС профессиональные',
    12: 'Лендинг',
    13: 'Листовки',
    14: 'Баннеры наружные',
    15: 'Реклама на транспорте',
    16: 'ВУЗы',
    17: 'Газеты',
    18: 'Рекомендации',
    19: 'Реклама в интернете',
    20: 'Перенос',
    21: 'Чита.ру',
    22: 'РоботВера',
    23: 'Instagram',
    24: 'Иное',
    25: 'Radar - мобильные приложения',
    26: 'SMS-рассылка',
    27: 'Реклама в кинотеатре',
    28: 'Тайм-кафе (антикафе)',
    29: 'Ярмарка вакансий',
    30: 'Talantix',
    31: 'FarPost',
    32: 'Yandeх Direct',
    33: 'Google Direct',
    34: 'РК VK',
    35: 'Viber'
}


# Ветка 7.2
# Сохраняем сколько разных статусов было у записи.
def n_status_change(status_history):
    # Добавляем единичку так как текущий статус считается отдельно.
    return pd.Series(map(lambda item: len(item) + 1, status_history))


# Ветка 7.3
def preparation_1(conn, data, coldict, scope):
    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    m_current_status = stc.special_mongo_names['current_status_cand']

    dict_data = {}

    # Для последующего разложения на статусы
    dict_data[m_status_history] = list(map(dp.some_to_null, dp.force_take_series(data, m_status_history)))
    dict_data[m_current_status] = list(map(dp.some_to_null, dp.force_take_series(data, m_current_status)))

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    res = pd.DataFrame(dict_data)
    return res


# Ветка 7.4
def preparation_2(conn, data, coldict):
    dict_data = {}

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['email']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['phone_number']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['first_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['middle_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['last_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['fio']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['created_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['created_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['updated_at_mil']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_miliseconds,
        list(map(dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))
    ))

    t_col = coldict['updated_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['deleted_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_add_way']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_search_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_sf_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_source_url']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_call_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_external_source_url']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_utm_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_utm_campaign']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['about']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['area']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['avatar']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['average_experience']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['birth_date']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['citizenship']
    dict_data[t_col['name']] = list(map(dp.process_arrays, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['current_company']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['current_title']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['education']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['education_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # t_col = coldict['educations']
    # field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    # dict_data[t_col['name']] = dp.nested_cols(t_col, field)
    #
    # t_col = coldict['works']
    # field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    # dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['gender']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['gender_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['metro']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['skills']
    dict_data[t_col['name']] = list(map(dp.process_arrays, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['total_experience']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['cv_updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['preferred_employments']
    dict_data[t_col['name']] = list(
        map(dp.process_arrays_num, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['preferred_salary']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['preferred_schedules']
    dict_data[t_col['name']] = list(
        map(dp.process_arrays_num, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['resume_title']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))

    res = pd.DataFrame(dict_data)
    return res


# Ветка 7.5
# Блок preparation
# Принимает на вход связь c MongoDB и DataFrame выгруженных коммуникаций.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
def cand_df_preparation(params, data):
    print('- Первичная обработка кандидатов с догрузкой из базы')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_candidates = preparation_1(params['conn'], data, params['coldict'], params['scope'])
        print('- Добавление столбца с ID Заявки в кандидатов')
    elif params['scope'] == 'sberbank':
        dataset_candidates = preparation_2(params['conn'], data, params['coldict'])
    print('dataset_candidates', dataset_candidates)
    return dataset_candidates


cand_df_preparation(candidates_parameters_dict_OrgUnits, data)