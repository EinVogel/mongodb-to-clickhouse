import pandas as pd
from confs.utils import candidates_parameters_dict_OrgUnits
from branch_4_create_mongoDB_query import mongo_query, query_text


# Класс для указания пути к данным в MongoDB. Умеет парсить строку. Ниже функции тащат данные через него.
class Path:

    def __init__(self):
        self.db = ''
        self.collection = ''
        self.fields = {}
        self.path = ''

    # Парсинг пути до Mongo поля
    def pars_path(fullpath):
        r = Path()
        try:
            options = fullpath.split('/', maxsplit=3)
            r.db = options[1]
            r.collection = options[2]
            r.path = options[3]
            if options[3].split('/')[0] != '':
                r.fields = {options[3].split('/')[0]: 1}
        except:
            print('Error: Wrong fullpath')
        return r

    # Курсор для Mongo
    def to_cursor(conn, my_path, query):
        if my_path.fields != {}:
            cursor = conn[my_path.db][my_path.collection].find(query, my_path.fields)
        else:
            cursor = conn[my_path.db][my_path.collection].find(query)
        return cursor

    # Курсор для Mongo датасета Communications. Со включением ограничений
    def comm_to_cursor(conn, my_path, query, restictions):
        cursor = conn[my_path.db][my_path.collection].find(query, restictions)
        return cursor

    # Подсчёт количества записей
    def cursor_count(conn, my_path, query):
        if my_path.fields != {}:
            cursor = conn[my_path.db][my_path.collection].count(query, my_path.fields)
        else:
            cursor = conn[my_path.db][my_path.collection].count(query)
        return cursor

    # Получение всех компаний в датасете. Для other scope
    def get_companies(conn, my_path):
        cursor = conn[my_path.db][my_path.collection].distinct('CompanyId')
        cursor = list(cursor)
        return cursor


# Ветка 5
#  Cursor mongodb по сформированному запросу query
def select_mongodb(params, query, query_text):
    print('- Производится выгрузка с ' + query_text)
    cursor = Path.to_cursor(
        params['conn'],
        params['path'],
        query
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    count = 0
    for i in cursor:
        print('i = ', i)
        count += 1
    data = pd.DataFrame(list(cursor))
    print('cursor', cursor)
    print('  Количество записей: ' + str(count))
    return data


data = select_mongodb(candidates_parameters_dict_OrgUnits, mongo_query, query_text)
