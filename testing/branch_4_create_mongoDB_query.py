import codecs
import requests
from six import string_types, binary_type, text_type, PY3
import time
from datetime import datetime, date, timedelta
from confs.utils import candidates_parameters_dict_OrgUnits, config


def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 4.1
# Декодирует value
def unescape(value):
    return codecs.escape_decode(value)[0].decode('utf-8')


# Ветка 4.2
# Парсит tsv
def parse_tsv(line):
    if PY3 and isinstance(line, binary_type):
        line = line.decode()
    if line and line[-1] == '\n':
        line = line[:-1]
    return [unescape(value) for value in line.split(str('\t'))]


# Ветка 4.3
# Select в КХ
# query - массив из 2 элементов
# query[0] - названия полей через запятую
# query[1] - текст запроса после 'FROM'
def select(url, query, showquery=False):
    # "SETTINGS max_query_size = 256" - увеличивает квоту на размер запроса
    sub_query = 'SELECT ' + query[0]
    if query[1] != '':
        sub_query = sub_query + ' FROM ' + query[1]
    sub_query = sub_query + ' SETTINGS max_query_size = 100000 FORMAT TabSeparatedWithNamesAndTypes'
    if showquery:
        print(sub_query)
    r, _ = _post(url, sub_query)
    data = []
    field_types = []
    field_names = []
    if r != False and r.status_code == 200:
        lines = r.iter_lines()
        field_names = parse_tsv(next(lines))
        field_types = parse_tsv(next(lines))
        for line in lines:
            data.append(parse_tsv(line))
    print('field_names field_types, data', field_names, field_types, data)
    return field_names, field_types, data


# Ветка 4.4
# Блок получения максимальных значений
# для значение столбца
def take_max_value(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, ['max(' + col_name + ')', full_tbl_name])
    return max_val[0][0]


# Ветка 4.5
# для миллисекунды
def take_max_value_mil(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, [col_name + ', ' + col_name + '_mil', full_tbl_name + ' ORDER BY ' + col_name + ' DESC, ' + col_name + '_mil'  +  ' DESC LIMIT 1'])
    return max_val[0][0], max_val[0][1]


# Ветка 4.6
# Тут все запросы к MongoDB
# Запрос для кандидатов
def candidates_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    # Берем последнюю дату из clickhouse. Если таблица пустая, берется значение из min_date
    time_to_query = min_date
    if scope == 'megafon' or scope == 'other':
        try:
            last_date = take_max_value(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            time_to_query = last_date - datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    elif scope == 'sberbank':
        try:
            last_date, microseconds = take_max_value_mil(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            microseconds = int(microseconds) * 1000
            last_date = last_date + datetime.timedelta(microseconds=microseconds)
            time_to_query = last_date
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки. Уже не нужен с связи с реализацией батичнга.
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Компании по которым выгружаются данные
            {'CompanyId': {'$in': companies}},
        # Поле даты по которому выгружаются данные
            {
                mongodb_field: {
                    '$gt': time_to_query,  # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
                # Для sberbank выгрузка реализована иначе можно опустить
            } if scope in ('megafon', 'other') else {},
        ]
    }
    return query, text


# Ветка 4.7
def create_mongodb_query(params, min_date):
    query, text = candidates_query(
        # Путь до поля с датой в MongoDB, по которому выгружаются данные
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        # pikta_dev.candidates_stages
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    print(query)
    print(text)
    return query, text


min_date = datetime.strptime(config['date_loadout'], ('%Y, %m, %d, %H, %M'))  # start date

mongo_query, query_text = create_mongodb_query(candidates_parameters_dict_OrgUnits, min_date)
