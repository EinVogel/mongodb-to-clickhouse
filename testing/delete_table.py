import requests
from confs.utils import OrgUnits_parameters_dict

def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 14.1
# удаление таблицы с именем tbl_name
def drop_table_1(url, tbl_name):
    sub_query = 'DROP TABLE IF EXISTS ' + tbl_name
    _, s = _post(url, sub_query)
    return s


# Создание таблицы в clickhouse, с параметрами указанными в coldict, если ее там нет
# Удаление таблиц в ClickHouse
def drop_table(params):
    state = drop_table_1(
        params['clickhouse_url'],
        params['full_tbl_name']
        )  # Ветка 14
    return state
