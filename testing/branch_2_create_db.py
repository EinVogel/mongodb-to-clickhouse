import requests
from confs.utils import candidates_parameters_dict_OrgUnits


def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 2.2
# Создание баз данных с именем db_name
def create_database(url, db_name):
    sub_query = 'CREATE DATABASE  ' + db_name + ';'
    _, s = _post(url, sub_query)
    return s


# Ветка 2.3
# Создание базы данных, с именем указаными в db_name
def create_db(params):
    print('- Создаем базу данных, если её нет')
    # Выбор первого части строки до точки. Например data.base, выберем data
    # В данном случае название базы данных будет pikta_dev
    db_name = params['full_tbl_name'].split('.')[0]
    print('- Название созданной базы данных ', db_name)
    state = create_database(params['clickhouse_url'], db_name)
    if not state:
        print(' - Не удалось создать БД')
        return state


create_db(candidates_parameters_dict_OrgUnits)
