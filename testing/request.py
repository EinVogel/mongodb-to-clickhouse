import pymongo as pm
import pandas as pd
import requests
import time
from datetime import datetime, date, timedelta
import yaml
import pdb
import os
from six import string_types, binary_type, text_type, PY3
import codecs
import numpy as np
import libs.data_parser as dp
import libs.structure as stc
# Импорт словаря в формате JSON
from confs.utils import candidates_parameters_dict

# Загрузка данных из yml файла
with open('confs/conf.yml', 'r') as yamlfile:
    config = yaml.load(yamlfile)

env = 'dev'


# Ветка 1.1 [my_clickhouse]
# Отправление запроса в базу данных и получения ответа
def _get(url, query):
    try:
        # получаем запрос с сайта
        r = requests.post(url=url, data=query, stream=False)
        # s - ответ с сервера
        s = r.text
        # статус 200 означает что все ОК, получена информация с сайта
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 1.2 [my_clickhouse]
# Наличие соединения с сервером
def connection_exists(url):
    # Посылаем запрос в базу данных Clickhouse
    sub_query = 'SELECT 1'
    # Вызываем функцию _get и передаем туда запрос
    _, s = _get(url, sub_query)
    return s


# Ветка 1.3 [mongo_clickhouse]
# Проверка соединения с сервером
def check_for_connection(params):
    print('- Проверяем есть ли соединение с КХ')
    # Вызывается функция connection_exists из файла my_clickhouse
    response = connection_exists(params['clickhouse_url'])
    return response


# Ветка 2.1
# Отправляет строку в clickhouse
def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 2.2
# Создание баз данных с именем db_name
def create_database(url, db_name):
    sub_query = 'CREATE DATABASE  ' + db_name + ';'
    _, s = _post(url, sub_query)
    return s


# Ветка 2.3
# Создание базы данных, с именем указаными в db_name
def create_db(params):
    print('- Создаем базу данных, если её нет')
    # Выбор первого части строки до точки. Например data.base, выберем data
    # В данном случае название базы данных будет pikta
    db_name = params['full_tbl_name'].split('.')[0]
    print('- Название созданной базы данных ', db_name)
    state = create_database(params['clickhouse_url'], db_name)
    if not state:
        print(' - Не удалось создать БД')
        return state


# Ветка 3.1
# Блок преобразования словаря в запрос на создание таблицы
# Добавляет движок 'MergeTree'
def cols_to_create_query(coldict, table_name, id_field, date_field):
    res = [
        table_name,
        [],
        'MergeTree(' + date_field + ', (' + id_field + ', ' + date_field + '), 8192)'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    print(res)
    return res


# Добавляет движок 'ReplacingMergeTree'
def cols_to_create_replacing_tree_query(coldict, table_name, id_field, date_field, primary_keys):
    res = [
        table_name,
        [],
        'ReplacingMergeTree(' + date_field + ')' + ' ORDER BY ' + '(' + primary_keys + ')'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    return res


# Создание таблицы
def create_table_1(url, query):
    sub_query = 'CREATE TABLE IF NOT EXISTS ' + query[0] + ' ('
    for i, q in enumerate(query[1]):
        sub_query = sub_query + ' ' + q
        if i < len(query[1])-1:
            sub_query += ','
        else:
            sub_query = sub_query + ') ENGINE = '
    sub_query = sub_query + query[2] + ';'
    _, s = _post(url, sub_query)
    return s


# Ветка 3.2
# Создание таблиц данных
def create_table(params):
    print('- Создаем таблицу в clickhouse, если её там нет')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        c_query = cols_to_create_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field']
        )
    elif params['scope'] == 'sberbank':
        c_query = cols_to_create_replacing_tree_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field'],
            params['primary_keys']
        )
    state = create_table_1(params['clickhouse_url'], c_query)
    if not state:
        print(' - Не удалось связаться с clickhouse')
        return state


# Ветка 4.1
# Декодирует value
def unescape(value):
    return codecs.escape_decode(value)[0].decode('utf-8')


# Ветка 4.2
# Парсит tsv
def parse_tsv(line):
    if PY3 and isinstance(line, binary_type):
        line = line.decode()
    if line and line[-1] == '\n':
        line = line[:-1]
    return [unescape(value) for value in line.split(str('\t'))]


# Ветка 4.3
# Select в КХ
# query - массив из 2 элементов
# query[0] - названия полей через запятую
# query[1] - текст запроса после 'FROM'
def select(url, query, showquery=False):
    # "SETTINGS max_query_size = 256" - увеличивает квоту на размер запроса
    sub_query = 'SELECT ' + query[0]
    if query[1] != '':
        sub_query = sub_query + ' FROM ' + query[1]
    sub_query = sub_query + ' SETTINGS max_query_size = 100000 FORMAT TabSeparatedWithNamesAndTypes'
    if showquery:
        print(sub_query)
    r, _ = _post(url, sub_query)
    data = []
    field_types = []
    field_names = []
    if r != False and r.status_code == 200:
        lines = r.iter_lines()
        field_names = parse_tsv(next(lines))
        field_types = parse_tsv(next(lines))
        for line in lines:
            data.append(parse_tsv(line))
    print('field_names, field_types, data', field_names, field_types, data)
    return field_names, field_types, data


# Ветка 4.4
# Блок получения максимальных значений
# для значение столбца
def take_max_value(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, ['max(' + col_name + ')', full_tbl_name])
    return max_val[0][0]


# Ветка 4.5
# для миллисекунды
def take_max_value_mil(clickhouse_url, full_tbl_name, col_name):
    _, _, max_val = select(clickhouse_url, [col_name + ', ' + col_name + '_mil', full_tbl_name + ' ORDER BY ' + col_name + ' DESC, ' + col_name + '_mil'  +  ' DESC LIMIT 1'])
    return max_val[0][0], max_val[0][1]


# Ветка 4.6
# Тут все запросы к MongoDB
# Запрос для кандидатов
def candidates_query(mongodb_field, clickhouse_url, full_tbl_name, col_name, min_date, companies, scope):

    # Берем последнюю дату из clickhouse. Если таблица пустая, берется значение из min_date
    time_to_query = min_date
    if scope == 'megafon' or scope == 'other':
        try:
            last_date = take_max_value(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            time_to_query = last_date - datetime.timedelta(seconds=1)#.replace(minute=last_date.minute-2)
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    elif scope == 'sberbank':
        try:
            last_date, microseconds = take_max_value_mil(
                clickhouse_url=clickhouse_url,
                full_tbl_name=full_tbl_name,
                col_name=col_name
            )
            last_date = datetime.datetime.strptime(last_date, '%Y-%m-%d %H:%M:%S')
            microseconds = int(microseconds) * 1000
            last_date = last_date + datetime.timedelta(microseconds=microseconds)
            time_to_query = last_date
            text = str(time_to_query)
        except:
            text = str(time_to_query)
    # max date нужен для батчинга данных и первичной выгрузки. Уже не нужен с связи с реализацией батичнга.
    # max_date = min_date + datetime.timedelta(1)
    query = {
        '$and': [
        # Компании по которым выгружаются данные
            {'CompanyId': {'$in': companies}},
        # Поле даты по которому выгружаются данные
            {
                mongodb_field: {
                    '$gt': time_to_query,  # ISODate("2018-04-01"), # datetime.datetime(2018, 5, 31),
                    # '$lte': max_date
                }
            },
            {
                # Этот фильтр нужен что бы исключить ошибки в базе, при которых наша процедура падает.
                "CurrentStatus": {
                    '$ne': None
                }
                # Для sberbank выгрузка реализована иначе можно опустить
            } if scope in ('megafon', 'other') else {},
        ]
    }
    return query, text


# Ветка 4.7
def create_mongodb_query(params, min_date):
    query, text = candidates_query(
        # Путь до поля с датой в MongoDB, по которому выгружаются данные
        params['path_to_audit_updated_at'],
        params['clickhouse_url'],
        # pikta_dev.candidates_stages
        params['full_tbl_name'],
        params['status_set_at_field'],
        min_date,
        params['companies'],
        params['scope']
    )
    return query, text


# Ветка 5
#  Cursor mongodb по сформированному запросу query
def select_mongodb(params, query, query_text):
    print('- Производится выгрузка с ' + query_text)
    cursor = dp.Path.to_cursor(
        params['conn'],
        params['path'],
        query
        ).sort([(params['path_to_audit_updated_at'], 1)]).limit(params['batch_size'])
    if (params['limit'] >= 0): cursor = cursor.limit(params['limit'])
    return cursor


# Ветка 6
# Приводим Cursor в pandas DataFrame
def cursor_to_df(cursor):
    data = dp.cursor_to_df(cursor)
    print('  Количество записей: ' + str(data.shape[0]))
    return data


# Ветка 7.1
# Словарь дополнительных источников
other_sours = {
    0: 'null',
    1: 'zarplata.ru',
    2: 'Отклик zarplata.ru',
    3: 'rabota.ru',
    4: 'Отклик rabota.ru',
    5: 'Видео-интервью',
    6: 'Прочие работные сайты',
    7: 'Отклик прочие работные сайты',
    8: 'СС VK',
    9: 'СС Facebook',
    10: 'СС OK',
    11: 'СС профессиональные',
    12: 'Лендинг',
    13: 'Листовки',
    14: 'Баннеры наружные',
    15: 'Реклама на транспорте',
    16: 'ВУЗы',
    17: 'Газеты',
    18: 'Рекомендации',
    19: 'Реклама в интернете',
    20: 'Перенос',
    21: 'Чита.ру',
    22: 'РоботВера',
    23: 'Instagram',
    24: 'Иное',
    25: 'Radar - мобильные приложения',
    26: 'SMS-рассылка',
    27: 'Реклама в кинотеатре',
    28: 'Тайм-кафе (антикафе)',
    29: 'Ярмарка вакансий',
    30: 'Talantix',
    31: 'FarPost',
    32: 'Yandeх Direct',
    33: 'Google Direct',
    34: 'РК VK',
    35: 'Viber'
}


# Ветка 7.2
# Сохраняем сколько разных статусов было у записи.
def n_status_change(status_history):
    # Добавляем единичку так как текущий статус считается отдельно.
    return pd.Series(map(lambda item: len(item) + 1, status_history))


# Ветка 7.3
def preparation_1(conn, data, coldict, scope):
    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    m_current_status = stc.special_mongo_names['current_status_cand']

    dict_data = {}

    # Для последующего разложения на статусы
    dict_data[m_status_history] = list(map(dp.some_to_null, dp.force_take_series(data, m_status_history)))
    dict_data[m_current_status] = list(map(dp.some_to_null, dp.force_take_series(data, m_current_status)))

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение first name
    t_col = coldict['first_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение middle name
    t_col = coldict['middle_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение last name
    t_col = coldict['last_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['client_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['requisition_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['requisition_location_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['requisition_binding_date']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['first_source_call_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение источника заявки кандидата
    t_col = coldict['first_source_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоеденение Sf источника заявки кандидата, названия подгружаются из other_sours
    t_col = coldict['first_source_sf_sorce']
    dict_data[t_col['name']] = list(map(lambda x: dp.from_dict(x, other_sours), dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )))

    t_col = coldict['first_source_utm_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_utm_campaign']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['add_way']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )
    if scope == 'megafon':
        t_col = coldict['extra_data_page']
        dict_data[t_col['name']] = dp.under_value_df(
            data, dp.Path.pars_path(t_col['path'])
        )

    t_col = coldict['created_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    if scope == 'other':
        t_col = coldict['updated_at']
        dict_data[t_col['name']] = list(map(
            dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
        ))

    # Присоеденение Id компании, если значения пожожи на null, приводит к null
    t_col = coldict['company_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение название компании
    t_col = coldict['company_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1=coldict['company_id']['mongo_name'],
        vpath='/workspace/companies/name//',
        key2='_id',
        new_name=t_col['name'],
    )

    # Присоединение Id вакансии, если значения пожожи на null, приводит к null
    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    # Присоединение названия вакансии
    t_col = coldict['vacancy_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1=coldict['vacancy_id']['mongo_name'],
        vpath='/workspace/vacancies/name//',
        key2='_id',
        new_name=t_col['name'],
    )

    # Присоединение города вакансии
    t_col = coldict['vacancy_location']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1=coldict['vacancy_id']['mongo_name'],
        vpath='/workspace/vacancies/city//',
        key2='_id',
        new_name=t_col['name'],
    )

    # Присоединение Id департамента вакансии
    t_col = coldict['vacancy_department_id']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        data,
        key1=coldict['vacancy_id']['mongo_name'],
        vpath='/workspace/vacancies/DepartmentId//',
        key2='_id',
        new_name=t_col['name'],
    )

    # Присоединение названия департамента вакансии
    t_col = coldict['vacancy_department_name']
    dict_data[t_col['name']] = dp.join_by_keys_res(
        conn,
        dp.join_by_keys(
            conn,
            data,
            key1=coldict['vacancy_id']['mongo_name'],
            vpath='/workspace/vacancies/DepartmentId//',
            key2='_id',
            new_name='DepartmentId',
        ),
        key1='DepartmentId',
        vpath='/workspace/Departments/Name//',
        key2='_id',
        new_name=t_col['name'],
    )

    # Присоединение даты обновление CV
    t_col = coldict['cv_updated']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    # Присоединение даты рождения
    t_col = coldict['birth_date']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))
    dict_data[t_col['name']] = list(map(dp.birth_process, dict_data[t_col['name']]))
    # Рассчет и присоединение возраста

    t_col = coldict['age']
    dict_data[t_col['name']] = list(map(
        dp.num_years, dict_data[coldict['birth_date']['name']]
    ))

    # Присоединение предпочтительной станции метро
    t_col = coldict['metro']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение образования кандидата
    t_col = coldict['education']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение специализации образования кандидата
    t_col = coldict['education_specialization']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение года окончания получения образования кандидата
    t_col = coldict['education_end_year']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение Total Experience
    t_col = coldict['experience_total']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение Average Experience
    t_col = coldict['experience_average']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение текущей компании работы кандидата
    t_col = coldict['current_company']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение текущей должности
    t_col = coldict['current_title']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение города проживания
    t_col = coldict['area']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение списка гражданств
    t_col = coldict['citizenship_all']
    dict_data[t_col['name']] = list(map(
        dp.arr_to_text, list(dp.under_value_df(data, dp.Path.pars_path(t_col['path'])))
    ))

    # Присоединение российского гражданства
    t_col = coldict['citizenship_rus']
    dict_data[t_col['name']] = list(map(
        dp.some_to_null, dp.check_if_sub_str_series(dict_data[(coldict['citizenship_all']['name'])], 'Россия')
    ))

    # Присоеднение количества гражданст
    t_col = coldict['citizenship_count']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.n_arr(
        dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    )))

    # Присоединение адреса группового интервью
    t_col = coldict['group_interview_address']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение даты группового интервью
    t_col = coldict['group_interview_datetime']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    # Присоединение Training Type
    t_col = coldict['training_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение адреса тренинга
    t_col = coldict['training_address']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # Присоединение даты тренинга
    t_col = coldict['training_datetime']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    # Присоединение количества смен статуса кандидата
    t_col = coldict['n_statuse_change']
    dict_data[t_col['name']] = list(map(dp.some_to_null, n_status_change(data[m_status_history])))

    res = pd.DataFrame(dict_data)
    return res


# Ветка 7.4
def preparation_2(conn, data, coldict):
    dict_data = {}

    # Присоединение ID кандидатов
    t_col = coldict['id_field']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['email']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['phone_number']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['vacancy_id']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['first_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['middle_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['last_name']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['fio']
    dict_data[t_col['name']] = list(map(dp.some_to_null, dp.force_take_series(data, t_col['mongo_name'])))

    t_col = coldict['created_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['created_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['created_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['updated_at_mil']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_miliseconds,
        list(map(dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))
    ))

    t_col = coldict['updated_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['updated_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['deleted_by']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['deleted_by_name']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_add_way']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_search_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_sf_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_source_url']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_id']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_call_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_external_source_url']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_utm_source']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['first_source_utm_campaign']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['about']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['area']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['avatar']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['average_experience']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['birth_date']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['citizenship']
    dict_data[t_col['name']] = list(map(dp.process_arrays, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['current_company']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['current_title']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['education']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['education_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    # t_col = coldict['educations']
    # field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    # dict_data[t_col['name']] = dp.nested_cols(t_col, field)
    #
    # t_col = coldict['works']
    # field = dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    # dict_data[t_col['name']] = dp.nested_cols(t_col, field)

    t_col = coldict['gender']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['gender_type']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['metro']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['skills']
    dict_data[t_col['name']] = list(map(dp.process_arrays, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['total_experience']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['cv_updated_at']
    dict_data[t_col['name']] = list(map(
        dp.iso_date_to_datetime, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))
    ))

    t_col = coldict['preferred_employments']
    dict_data[t_col['name']] = list(
        map(dp.process_arrays_num, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['preferred_salary']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['preferred_schedules']
    dict_data[t_col['name']] = list(
        map(dp.process_arrays_num, dp.under_value_df(data, dp.Path.pars_path(t_col['path']))))

    t_col = coldict['resume_title']
    dict_data[t_col['name']] = dp.under_value_df(
        data, dp.Path.pars_path(t_col['path'])
    )

    t_col = coldict['date_for_clickhouse']
    dict_data[t_col['name']] = list(map(datetime.datetime.date, dp.force_take_series(dict_data, '_updated_at')))

    res = pd.DataFrame(dict_data)
    return res


# Ветка 7.5
# Блок preparation
# Принимает на вход связь c MongoDB и DataFrame выгруженных коммуникаций.
# Подтягивает дополнительные данные из базы. Приводит данные в порядок.
def cand_df_preparation(params, data):
    print('- Первичная обработка кандидатов с догрузкой из базы')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        dataset_candidates = preparation_1(params['conn'], data, params['coldict'], params['scope'])
        print('- Добавление столбца с ID Заявки в кандидатов')
    elif params['scope'] == 'sberbank':
        dataset_candidates = preparation_2(params['conn'], data, params['coldict'])
    return dataset_candidates


# Ветка 8.1
# Берет на вход датафрейм из кандидатов и выдает датафрейм ID кандидатов и заявок
def cand_jobr_df_join(params, dataset_candidates):
    print('  Создание таблицы связи кандидат-заявка')
    dataset_candidates['_jobr_id'] = dataset_candidates['_requisition_id']
    return dataset_candidates


# Ветка 9.1
# Получаем на вход массив из ID кандидатов, выдаем таблицу с максимальной датой смены статуса
def df_last_state_update(url, ids, db_name, col_dataframe_name, col_db_name, new_col_name):
    query = [
        col_dataframe_name + ", max(" + col_db_name + ")",
        db_name
    ]
    # Фильтр по ID включаем только если записей немного - иначе все будет упираться в размер запроса
    if (ids.shape[0] < 3000):
        query[1] += " where " + col_dataframe_name + " in ("
        for i in range(ids.shape[0]):
            query[1] += '\'' + ids[i] + '\''
            if i < ids.shape[0]-1:
                query[1] += ", "
        query[1] += ")"
    query[1] += " group by " + col_dataframe_name
    data = []
    cols, _, data = select(url, query)
    # Записываем результат в датафрейм
    if (len(cols) > 0):
        res = pd.DataFrame(data, columns=[col_dataframe_name, new_col_name])
        res[new_col_name] = res[new_col_name].apply(
            lambda x: datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S'),
            convert_dtype=True
        )
    else:
        res = pd.DataFrame()
    return res


# Ветка 9.2
# Получает на вход массив из ID кандидатов, выдает таблицу с максимальной датой смены статуса по кандидату
def update_df(params, dataset_candidates, min_date):
    last_state_update_DF = df_last_state_update(
        params['clickhouse_url'],
        dataset_candidates[params['id_field']],
        params['full_tbl_name'],
        params['id_field'],
        params['status_set_at_field'],
        params['last_state_update_field']
    )
    if last_state_update_DF.shape[0] == 0:
        last_state_update_DF[params['id_field']] = dataset_candidates[params['id_field']]
        last_state_update_DF[params['last_state_update_field']] = np.full(dataset_candidates.shape[0], datetime(2017, 1, 1, 0, 0, 0))
    dataset_candidates = dataset_candidates.join(last_state_update_DF.set_index(params['id_field']), on=params['id_field'])
    dataset_candidates[params['last_state_update_field']].fillna(datetime(2017, 1, 1, 0, 0, 0), inplace=True)
    return dataset_candidates


# Ветка 10.1
# Добираемся до значений статуса по путю в зависимости от того текущий он или бывший
def path_for_status(i, l, param):
    fulpath = dp.Path()
    fulpath.db = 'candidates'
    fulpath.collection = 'Candidates'
    #  Если статус текущий - берем данные из другого поля так как в StatusHistory этой информации нет.
    if i < l:
        fulpath.fields = {stc.special_mongo_names['status_history_cand']: 1}
        fulpath.path = str(i) + '/' + param
    else:
        fulpath.fields = {stc.special_mongo_names['current_status_cand']: 1}
        fulpath.path = param
    return fulpath


# Ветка 10.2
# Принимает на вход DataFrame по кандидатам.
# Выдает новый DataFrame, где на каждый статус записи создается новая запись.
def to_flat(data, coldict, field_last_upd):

    # Выносим названия повторяющихся в запросе полей из MongoDB в словарь
    m_status_history = stc.special_mongo_names['status_history_cand']
    n_statuse_change = coldict['n_statuse_change']['name']

    data[n_statuse_change] = n_status_change(dp.force_take_series(data, m_status_history))
    flat_cols = [x for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT]
    old_cols_names = [x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_PREPARATION]
    t_time = time.time()
    t_len = 0
    new_rows = list()

    for i, row in data.iterrows():

        if i % 100 == 0 and i != 0:
            d_time = time.time() - t_time
            d_len = len(new_rows) - t_len
            print(' - Обрабатываем элемент номер '
                  + str(i)
                  + ' - '
                  + str("%.4f" % (d_time))
                  + ' секунд; '
                  + str(d_len)
                  + ' новых записей; '
                  + str("%.4f" % (float(d_len)/d_time))
                  + ' записей в секунду'
                 )
            t_time = time.time()
            t_len = len(new_rows)

        for j in range(row[n_statuse_change]):

            date_check = dp.under_value_line(
                row,
                path_for_status(j, row[n_statuse_change]-1, stc.special_mongo_names['set_at'])
            )
            try:
                date_check = date_check.replace(microsecond=0)
            except:
                date_check = row[field_last_upd]

            if row[field_last_upd] < date_check:
                t_new_value_row = np.empty(shape=[0, 0])
                for k in range(len(flat_cols)):
                    if flat_cols[k]['colsubtype'] == stc.COLSUBTYPE_FLATPATH:
                        new_value = dp.under_value_line(
                            row,
                            path_for_status(j, row[n_statuse_change]-1, flat_cols[k]['path'])
                        )
                        # Дополнительно обрабатываем дату
                        if flat_cols[k]['name'] == coldict['status_set_at']['name']:
                            new_value = dp.iso_date_to_datetime(new_value)

                        if flat_cols[k]['name'] == coldict['status_set_at_mil']['name']:
                            new_value = dp.iso_date_to_miliseconds(new_value)

                        if flat_cols[k]['name'] == coldict['status_stage_order']['name']:
                            try:
                                new_value = int(new_value)
                            except:
                                new_value = new_value
                    else:
                        if flat_cols[k]['name'] == coldict['status_index']['name']:
                            new_value = j

                    t_new_value_row = np.append(t_new_value_row, dp.some_to_null(new_value))
                new_rows.append(np.append(row[old_cols_names].values, t_new_value_row))

    res = pd.DataFrame(new_rows, columns=[
        x['name'] for x in coldict.values() if x['coltype'] == stc.COLTYPE_FLAT or x['coltype'] == stc.COLTYPE_PREPARATION
    ])

    return res


# Ветка 10.3
# Блок flat
# Принимает на вход DataFrame по кандидатам.
# Выдает новый DataFrame, где на каждый элемент вложенной структуры создается новая запись.
def df_to_flat(params, dataset_candidates):
    print('- Раскладываем данные в плоскую структуру, если есть новые статусы')
    dataset_candidates = to_flat(
        dataset_candidates,
        params['coldict'],
        params['last_state_update_field'],
    )
    print(' - Суммарное количество записей: ' + str(dataset_candidates.shape[0]))
    return dataset_candidates


# Ветка 11.1
# Добавляем столбец даты специально для clickhouse
def add_main_data_col(data, source_name, new_name):
    data[new_name] = list(map(lambda s: datetime.date(s), data[source_name]))
    return data


# Ветка 11.2
# Добавляем столбец даты специально для clickhouse
def data_preprocess(params, dataset_candidates):
    dataset_candidates = add_main_data_col(
        dataset_candidates,
        params['status_set_at_field'],
        params['date_field']
    )
    return dataset_candidates


# Ветка 12.1
# Блок функций для работы с разными типами данных
def any_to_int8(a):
    if a == True:
        r = '1'
    else:
        r = '0'
    return r


def any_to_date(a):
    r = str(a)[:10]
    if r == 'NaT' or r == 'null':
        r = '0000-00-00'
    return r


def any_to_datetime(a):
    r = str(a)[:19]
    if r == 'NaT' or r == 'null':
        r = '0000-00-00 00:00:00'
    return r


def any_to_int64(a):
    if str(a) != 'null':
        r = str(a)
    else:
        r = str(-1)
    return r


def any_to_float64(a):
    if str(a) != 'null':
        r = str(a)
    else:
        r = 'nan'
    return r


def any_to_string(a):
    r = str(a)
    r = r.replace('\'', '')
    r = r.replace('\"', '')
    r = r.replace('\\', '')
    return r


# Ветка 12.1
# Подготовка запроса insert из dateframe
def df_to_strs(data, coldict):
    res = []
    my_cols = [x for x in coldict.values()]
    for i in range(data.shape[0]):
        res.append('(')
        for j in range(len(my_cols)):
            my_type = my_cols[j]['chtype']
            my_data = data[my_cols[j]['name']][i]
            if my_type == 'Int8':
                res[i] = res[i] + any_to_int8(my_data)
            elif my_type == 'Date':
                res[i] += '\'' + any_to_date(my_data) + '\''
            elif my_type == 'DateTime':
                res[i] += '\'' + any_to_datetime(my_data) + '\''
            elif my_type == 'Int64':
                res[i] = res[i] + any_to_int64(my_data)
            elif my_type == 'Float64':
                res[i] = res[i] + any_to_float64(my_data)
            elif my_type == 'Array(String)':
                res[i] = res[i] + str(list(map(any_to_string, my_data)))
            elif my_type.startswith('Nested'):
                res[i] = res[i] + str(my_data)[1: -1]
            elif my_type == 'Array(Int64)':
                res[i] = res[i] + str(my_data)
            else:
                res[i] = res[i] + '\'' + any_to_string(my_data) + '\''
            if j < len(my_cols)-1:
                res[i] = res[i] + ', '
            else:
                res[i] = res[i] + ')'
    return res


# Ветка 12.2
# Получаем на вход DataFrame, название таблицы и DataFrame по колонкам, готовим запрос для insert
def data_to_insert_query(data, coldict, table_name):
    res = [
        table_name,
        []
    ]
    res[1] = df_to_strs(data, coldict)
    return res


# Ветка 12.3
# Перегоняем данные в набор строк.
def df_to_str(params, dataset_candidates):
    i_query = data_to_insert_query(
        dataset_candidates[[x['name'] for x in params['coldict'].values()]],
        params['coldict'],
        params['full_tbl_name']
    )
    return i_query


# Ветка 13.1
# Insert в КХ
# query - массив из 2 элементов
# query[0] - название таблицы с указанием базы
# query[1] - массив строк, которые необходимо вставить
def insert(url, query, showquery=False):
    r = False
    s = ''
    sub_query = 'INSERT INTO ' + query[0] + ' VALUES'
    for i, q in enumerate(query[1]):
        sub_query = sub_query + ' ' + q
        if i < len(query[1])-1:
            sub_query += ','
        else:
            sub_query += ';'
    if showquery:
        print(sub_query)
    r, _ = _post(url, sub_query.encode('utf-8'))
    if r.status_code != 200:
        s = r.text
    return r, s


# Ветка 13.2
# Выгрузка данных в clickhouse
def data_to_clickhouse(params, i_query, start_time_total):
    print('- Выгружаем данные в clickhouse')
    state, text = insert(params['clickhouse_url'], i_query, params['show_queries'])
    if not state:
        print(' - Не удалось добавить данные в clickhouse')
        print(text)
    # Выводим результат по времени.
    estimation = str("%.4f" % ((time.time() - start_time_total)/60))
    limit = params['limit']
    print(f'{estimation} минут - весь процесс при лимите {limit} записей')
    return state


# Основная функция
# Наиболее отлаженный и полноценный датасет – кандитаты pikta. Остальные делаются по аналогии
# Принимает на вход объект params из config/utils.py, который содержит все данные о датасете
def process_candidates(params, min_date):
    print()
    # лимит -1 в файле значит - выводим все данные
    print('Лимит ' + str(params['limit']) + ' записей')
    print('Начали работу по кандидатам')
    # Проверяем соединение с КХ
    # Функция check_for_connection вызывается из файла mongo_clickhouse
    connection_check = check_for_connection(params)  # Ветка 1
    if connection_check:
        # Создаем базу в КХ
        create_db(params)  # Ветка 2
        # Засекаем время
        start_time_total = time.time()
        # Создание таблицы. Требует сгенерированного объекта в structure.py
        create_table(params)  # Ветка 3
        skip = 0
        while True:
            # Генерация запроса к Mongo
            mongo_query, query_text = create_mongodb_query(params, min_date)  # Ветка 4
            # Получение данных из Mongo
            candidates_to_cursor = select_mongodb(params, mongo_query, query_text).skip(skip)  # Ветка 5
            # Преобразование Mongo данных в DataFrame
            candidates_df = cursor_to_df(candidates_to_cursor)  # Ветка 6
            if candidates_df.shape[0] != 0:
                # Обработка данных из Mongo. Получение нужных полей
                candidates_prep = cand_df_preparation(params, candidates_df)  # Ветка 7
                # Присоединеие заявок к датасету
                jobr_prep = cand_jobr_df_join(params, candidates_prep)  # Ветка 8
                # Исключение статусов присутвующих в КХ
                updated_df = update_df(params, jobr_prep, min_date)  # Ветка 9
                # Приведение к плоской структуре
                flat_dataset = df_to_flat(params, updated_df)  # Ветка 10
                print('flat_dataset', flat_dataset)
                if flat_dataset.shape[0] == 0:
                    skip += int(params['batch_size']/2)
                elif flat_dataset.shape[0] > 0:
                    skip = 0
                # Добавление столбца _date_for_clickhouse
                preprocessed_dataset = data_preprocess(params, flat_dataset)  # Ветка 11
                # Подготовка к отправке данных в КХ в качестве строки
                data_for_query = df_to_str(params, preprocessed_dataset)  # Ветка 12
                # Отправка данных в КХ
                candidates_to_clickhouse = data_to_clickhouse(params, data_for_query, start_time_total)  # Ветка 13

                if candidates_to_clickhouse:
                    print('Кандидаты выгружены в Clickhouse')
                else:
                    print('Ошибка - Кандидаты не выгружены')
            else:
                print('Нет кандидатов за выбранный период')
                return True
            if candidates_df.shape[0] < params['batch_size']:
                return True
        print('Работа окончена')


# Ветка 14.1
# удаление таблицы с именем tbl_name
def drop_table_1(url, tbl_name):
    sub_query = 'DROP TABLE IF EXISTS ' + tbl_name
    _, s = _post(url, sub_query)
    return s


# Создание таблицы в clickhouse, с параметрами указанными в coldict, если ее там нет
# Удаление таблиц в ClickHouse
def drop_table(params):
    state = drop_table_1(
        params['clickhouse_url'],
        params['full_tbl_name']
        )  # Ветка 14
    print(params['full_tbl_name'])
    return state


drop_table(candidates_parameters_dict)
print('Удаление таблицы прошло успешно')


# Блок выгрузки данных
print('Сервис запущен')
try:
    try:
        min_date = datetime.strptime(config['date_loadout'], ('%Y, %m, %d, %H, %M'))  # start date

        counter = 0
        while True:
            counter += 1
            # Реализация выгрузки для prod
            # Выгрузка для остальных окружений
            process_candidates(candidates_parameters_dict, min_date)
            time.sleep(60 * config['time_to_sleep_success_min'])
    except KeyboardInterrupt:
        print()
        print('Сервис остановлен')
        print()
except KeyboardInterrupt:
    pass
