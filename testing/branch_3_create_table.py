import requests
from confs.utils import candidates_parameters_dict_OrgUnits


def _post(url, query):
    try:
        r = requests.post(url=url, data=query, stream=False)
        s = True
        if r.status_code != 200:
            s = r.text
    except:
        r = False
        s = False
    return r, s


# Ветка 3.1
# Блок преобразования словаря в запрос на создание таблицы
# Добавляет движок 'MergeTree'
def cols_to_create_query(coldict, table_name, id_field, date_field):
    res = [
        table_name,
        [],
        'MergeTree(' + date_field + ', (' + id_field + ', ' + date_field + '), 8192)'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    print(res)
    return res


# Добавляет движок 'ReplacingMergeTree'
def cols_to_create_replacing_tree_query(coldict, table_name, id_field, date_field, primary_keys):
    res = [
        table_name,
        [],
        'ReplacingMergeTree(' + date_field + ')' + ' ORDER BY ' + '(' + primary_keys + ')'
    ]
    for irec in coldict:
        res[1].append(coldict[irec]['name'] + ' ' + coldict[irec]['chtype'])
    return res


# Создание таблицы
def create_table_1(url, query):
    sub_query = 'CREATE TABLE IF NOT EXISTS ' + query[0] + ' ('
    for i, q in enumerate(query[1]):
        sub_query = sub_query + ' ' + q
        if i < len(query[1])-1:
            sub_query += ','
        else:
            sub_query = sub_query + ') ENGINE = '
    sub_query = sub_query + query[2] + ';'
    _, s = _post(url, sub_query)
    return s


# Ветка 3.2
# Создание таблиц данных
def create_table(params):
    print('- Создаем таблицу в clickhouse, если её там нет')
    if params['scope'] == 'megafon' or params['scope'] == 'other':
        c_query = cols_to_create_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field']
        )
    elif params['scope'] == 'sberbank':
        c_query = cols_to_create_replacing_tree_query(
            params['coldict'],
            params['full_tbl_name'],
            params['id_field'],
            params['date_field'],
            params['primary_keys']
        )
    state = create_table_1(params['clickhouse_url'], c_query)
    if not state:
        print(' - Не удалось связаться с clickhouse')
        return state


create_table(candidates_parameters_dict_OrgUnits)
